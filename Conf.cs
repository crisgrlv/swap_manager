﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AereoManager
{
    public static class Conf
    {
        public static int getInt(string key)
        {
            try
            {
                int res;
                if (int.TryParse(ConfigurationManager.AppSettings.Get(key), out res))
                    return res;
                else
                {
                    Work.qLog.Enqueue(new messagetoLog("Error trying to read key " + key, 
                        "Conf", "getIntConf", messagetoLog.messageType.error));
                    return 0;
                }
            }
            catch (Exception ex)
            {
                Work.qLog.Enqueue(new messagetoLog("Error trying to read key " + key + " " + ex.Message, 
                    "Conf", "getIntConf", messagetoLog.messageType.error));
                return 0;
            }
        }

        public static string getStr(string key)
        {
            try
            {
                return ConfigurationManager.AppSettings.Get(key);
            }
            catch (Exception ex)
            {
                Work.qLog.Enqueue(new messagetoLog("Error trying to read key " + key + " " + ex.Message, 
                    "Conf", "getIntConf", messagetoLog.messageType.error));
                return "?";
            }
        }
    }
}
