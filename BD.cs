﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace AereoManager
{
    public static class BD
    {
        public static DataTable Select(string query)
        {
            DataTable resultado = new DataTable();
            try
            {
                using (SqlConnection conexion = new SqlConnection(Properties.Settings.Default.ConnectionString))
                {
                    conexion.Open();
                    SqlCommand comando = new SqlCommand(query, conexion);
                    SqlDataAdapter adapter = new SqlDataAdapter(comando);
                    adapter.Fill(resultado);
                    return resultado;
                }
            }
            catch (Exception ex)
            {
                Work.qLog.Enqueue(new messagetoLog("QUERY: " + query + " Exception: " + ex.Message, 
                    "BD", "Select", messagetoLog.messageType.error));
            }
            return resultado;
        }

        /// <summary>
        /// ExecuteReader function
        /// </summary>
        /// <param name="query">Text of query</param>
        /// <param name="parameters">Parameter names int query syntax always p0, p1, p2, etc.</param>
        /// <returns>Data Reader result</returns>
        public static DataTable ExecuteReader(string query, params object[] parameters)
        {
            DataTable res = new DataTable();
            try
            {
                using (SqlConnection conexion = new SqlConnection(Properties.Settings.Default.ConnectionString))
                {
                    conexion.Open();
                    SqlCommand comm = new SqlCommand(query, conexion);
                    int i = 0;
                    foreach (object o in parameters)
                    {
                        SqlParameter p = new SqlParameter("p" + i, o);
                        comm.Parameters.Add(p);
                        i++;
                    }

                    SqlDataReader dr = comm.ExecuteReader();
                    res.Load(dr);
                }
            }
            catch (Exception ex)
            {
                Work.qLog.Enqueue(new messagetoLog("QUERY: " + query + " Exception: " + ex.Message, 
                    "BD", "ExecuteReader", messagetoLog.messageType.error));
            }
            return res;
        }

        /// <summary>
        /// ExecuteNonQuery function
        /// </summary>
        /// <param name="query">Text of query</param>
        /// <param name="parameters">Parameter names int query syntax always p0, p1, p2, etc.</param>
        /// <returns>Rows affected by query</returns>
        public static int ExecuteNonQuery(string query, params object[] parameters)
        {
            int affectedrows = 0;
            try { 
                using (SqlConnection conexion = new SqlConnection(Properties.Settings.Default.ConnectionString))
                {
                    conexion.Open();
                    
                    SqlCommand comm = new SqlCommand(query, conexion);
                    int i = 0;
                    foreach(object o in parameters)
                    {
                        SqlParameter p = new SqlParameter("p" + i, o);
                        comm.Parameters.Add(p);
                        i++;
                    }
                    affectedrows = comm.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Work.qLog.Enqueue(new messagetoLog("QUERY: " + query + " Exception: " + ex.Message,
                    "BD", "ExecuteNonQuery", messagetoLog.messageType.error));

            }
            return affectedrows;
        }
    }
}
