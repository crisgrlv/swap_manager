﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AereoManager
{
    public partial class frmLaneData : Form
    {
        private bool Iscarrier;


        public frmLaneData()
        {
            InitializeComponent();
            dgv.AutoGenerateColumns = false;
        }

        /// <summary>
        /// Busca datos de las perchas por número de calle o referencia
        /// </summary>
        /// <param name="LaneOrRef">Número de calle o referencia</param>
        /// <param name="byLane">True si es por calle, false si es por referencia</param>
        public frmLaneData(string LaneOrRef, bool byLane=true):this()
        {
            if (byLane)
            {
                Iscarrier = false;
                this.Text = "Datos calle " + LaneOrRef;
                string query = "Select TS, CarrierId, Referencia, Lote, Orden from dbo.DG_InvAereo where Calle = @p0 and Estado = @p1 order by TS";
                DataTable dt = BD.ExecuteReader(query, LaneOrRef, Carrier.getSt(Carrier.enumEstadosPercha.Colocada));
                dgv.DataSource = dt;
                colCalle.Visible = false;
                colEstado.Visible = false;
            }
            else
            {
                Iscarrier = false;
                this.Text = "Ubicaciones de la referencia " + LaneOrRef;
                string query = "Select TS, CarrierId, Referencia, calle, Lote, Orden from dbo.DG_InvAereo where Referencia = @p0 and Estado = @p1 order by calle, TS";
                DataTable dt = BD.ExecuteReader(query, LaneOrRef, Carrier.getSt(Carrier.enumEstadosPercha.Colocada));
                dgv.DataSource = dt;
                colEstado.Visible = false;
            }
        }

        public frmLaneData(int id): this()
        {
            Iscarrier = true;
            this.Text = "Datos percha " + id;
            string query = "Select TS, CarrierId, Referencia, Lote, Orden, Calle, Estado from dbo.DG_InvAereo where CarrierId = @p0 order by TS";
            DataTable dt = BD.ExecuteReader(query, id);
            dgv.DataSource = dt;

        }

        private void dgv_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            try {
                if (Iscarrier && e.RowIndex > -1 && e.ColumnIndex == colEstado.Index)
                {
                    if (e.Value != null)
                    {
                        if (e.Value.ToString() == Carrier.getSt(Carrier.enumEstadosPercha.Colocada))
                        {
                            e.Value = "Colocada";
                        }
                        else if (e.Value.ToString() == Carrier.getSt(Carrier.enumEstadosPercha.Vacia))
                        {
                            e.Value = "Vacía";
                        }
                        else if (e.Value.ToString() == Carrier.getSt(Carrier.enumEstadosPercha.Bajando))
                        {
                            e.Value = "Bajando";
                        }
                        else if (e.Value.ToString() == Carrier.getSt(Carrier.enumEstadosPercha.Subiendo))
                        {
                            e.Value = "Subiendo";
                        }
                        else if (e.Value.ToString() == Carrier.getSt(Carrier.enumEstadosPercha.Eliminada))
                        {
                            e.Value = "Eliminada";
                        }
                        else if (e.Value.ToString() == Carrier.getSt(Carrier.enumEstadosPercha.Retornando))
                        {
                            e.Value = "Retornando";
                        }
                    }
                }
            }
            catch { }
        }
    }
}
