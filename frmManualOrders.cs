﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AereoManager
{
    public partial class frmManualOrders : Form
    {
        public frmManualOrders()
        {
            InitializeComponent();

            string query = "select distinct Referencia from dbo.ConfRef where referencia != ''";
            DataTable dt = BD.ExecuteReader(query);

            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    cboRef1.Items.Add(dr["Referencia"]);
                    cboRef2.Items.Add(dr["Referencia"]);
                }
            }

            cboWS.Items.Add("006 - Línea 1000");
            cboWS.Items.Add("007 - Línea 2000");
            cboWS.Items.Add("008 - Línea 3000");
            cboWS.Items.Add("010 - Reparaciones 2");

            cboRef1.SelectedIndex = 0;
            cboRef2.SelectedIndex = 0;
            cboWS.SelectedIndex = 0;

            cboOrigen.Items.Add(800);

            cboOrigen.Items.Add(901);

            for (int i = 1001; i < 1033; i++)
            {
                cboOrigen.Items.Add(i);
            }
            for (int i = 2001; i < 2020; i++)
            {
                cboOrigen.Items.Add(i);
            }
            for (int i = 3001; i < 3029; i++)
            {
                cboOrigen.Items.Add(i);
            }
            for (int i = 4001; i < 4017; i++)
            {
                cboOrigen.Items.Add(i);

            }

        }

        //Orden de carga manual
        private void btnSend_Click(object sender, EventArgs e)
        {
            comJIT jit = new comJIT();
            jit.seq = 0;
            jit.orden = txtOrder1.Text;
            jit.puesto = (string)cbows1.SelectedItem;
            jit.param.Add((string)cboRef1.SelectedItem);
            jit.param.Add(txtLot1.Text);
            jit.param.Add("1001");
            jit.procesado = false;
            jit.isReq = true;
            jit.param.Add(nudNumCarr.Value.ToString());
            jit.typeComm = comJIT.enumTipoComJIT.MLD_TRM;
            comJIT.Save(jit);
        }

        //Petición de percha
        private void btnSend2_Click(object sender, EventArgs e)
        {
            if (radioButton2.Checked)
            {
                comPLC pc = new comPLC();
                pc.idPeticionJIT = 0;
                //     pc.seq = PLCComm.calcSeq();
                pc.seq = 0;
                pc.tipoComando = comPLC.enumTipoComPLC.PCDescargaAPuesto;
                Carrier c = new Carrier();

                // c.reference = (string)cboRef2.SelectedItem;
                c.referencia = textBox1.Text.Trim();
                c.orden = txtOrder2.Text;
                enumPuestos ws = (enumPuestos)Enum.Parse(typeof(enumPuestos),
                    Enum.GetName(typeof(enumPuestos), int.Parse(((string)cboWS.SelectedItem).Substring(0, 3))));
                c.BuscaCalle(ws);
                //Si no encontró la calle, pongo la del comando JIT
                if (c.calle == 0)
                {
                    lblError.Text = "ERROR: NO SE ENCUENTRA LA REFERENCIA EN EL ALMACÉN";
                }
                else
                {
                    pc.param.Add(c.calle); //calle
                    pc.CarrierId = c.CarrierId;
                }
                pc.param.Add((short)ws); //puesto
                pc.PLCEnvia = false;
                pc.referencia = "0";
                pc.lote = "0";
                pc.orden = "manual";
                comPLC.Save(pc);
            }
            else if (radioButton1.Checked)
            {
                try
                {
                    comPLC pc = new comPLC();
                    pc.idPeticionJIT = 0;
                    pc.seq = 0;
                    pc.enviado = false;
                    pc.tipoComando = comPLC.enumTipoComPLC.PCDescargaAPuesto;
                    Carrier c = new Carrier();


                    c.orden = txtOrder2.Text;
                    enumPuestos ws = (enumPuestos)Enum.Parse(typeof(enumPuestos),
                        Enum.GetName(typeof(enumPuestos), int.Parse(((string)cboWS.SelectedItem).Substring(0, 3))));
                    c.calle = short.Parse(cboOrigen.SelectedItem.ToString());

                    c.referencia = Carrier.findRef(c.calle);
                   
                    pc.CarrierId = c.CarrierId;
                    pc.param.Add(c.calle);
                    pc.param.Add((short)ws); //puesto
                    pc.PLCEnvia = false;
                    pc.referencia = "0";
                    pc.lote = "0";
                    pc.orden = "manual";
                    comPLC.Save(pc);
                }
                catch { }
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            cboOrigen.Enabled = radioButton1.Checked;
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            cboRef2.Enabled = radioButton2.Checked;
            textBox1.Enabled = radioButton2.Checked;
        }
    }
}
