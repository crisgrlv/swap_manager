﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.ComponentModel;
using System.Data;

namespace AereoManager
{
    /// <summary>
    /// Clase que modela un mensaje que se guarda como log
    /// </summary>
    public class messagetoLog
    {
        /// <summary>
        /// Texto del mensaje
        /// </summary>
        public string message;
        /// <summary>
        /// Nombre de la clase donde se ha producido el mensaje que se quiere guardar
        /// </summary>
        public string classname;
        /// <summary>
        /// Nombre de la función donde se ha producido el mensaje que se quiere guardar
        /// </summary>
        public string functionname;
        /// <summary>
        /// Fecha hora en que se ha producido el mensaje
        /// </summary>
        public DateTime ts;
        /// <summary>
        /// Tipo de mensaje
        /// </summary>
        public messageType type;
        

        public messagetoLog(string message, string classname, string functionname, messageType type)
        {
            ts = DateTime.Now;
            this.message = message;
            this.classname = classname;
            this.functionname = functionname;
            this.type = type;
        }

        /// <summary>
        /// Tipo de mensaje que se loggea
        /// </summary>
        public enum messageType
        {
            /// <summary>
            /// Comunicación con JIT o con PLC activa
            /// </summary>
            state_on,
            /// <summary>
            ///  Comunicación con JIT o con PLC caída
            /// </summary>
            state_off,
            /// <summary>
            /// Se ha producido un error
            /// </summary>
            error,
            /// <summary>
            /// Información para guardar en el log Trace
            /// </summary>
            trace
        }
    }

    /// <summary>
    /// Hilo de acceso a datos, independiente de JITDaemon y PLCDaemon 
    /// </summary>
    public static class Work
    {
        /// <summary>
        /// Pila de mensajes para loggear
        /// </summary>
        public static Queue<messagetoLog> qLog;
        /// <summary>
        /// Pila de ordenes recibidas de JIT
        /// </summary>
        public static Queue<comJIT> qJITReq;
        /// <summary>
        /// Pila de respuestas del PLC
        /// </summary>
        public static Queue<comPLC> qPLCResp;
        /// <summary>
        /// Evento que se envía cada vez que hay algún mensaje que notificar a la interfaz
        /// </summary>
        /// <param name="m">Mensaje a notificar</param>
        public delegate void NotificationEvent(messagetoLog m);
        /// <summary>
        /// Evento que se envía cada vez que hay algún mensaje que notificar a la interfaz
        /// </summary>
        public static event NotificationEvent evNotification;
        private static System.Timers.Timer tmr;
        private static bool tmrFin;
        private static DateTime timetmr;
        private static int contador;

        /// <summary>
        /// Constructor sin parámetros
        /// </summary>
        static Work()
        {
            qLog = new Queue<messagetoLog>();
            qPLCResp = new Queue<comPLC>();
            qJITReq = new Queue<comJIT>();
            tmr = new System.Timers.Timer();
            tmr.Interval = Conf.getInt("work_interval");
            tmr.Elapsed += Tmr_Elapsed;
            tmrFin = true;
        }

        public static void Start()
        {
            tmr.Start();
        }

        private static void Tmr_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (tmrFin)
            {
                tmrFin = false;
                timetmr = DateTime.Now;

                doWork();
               // Trace.TraceError("WORK: " + (DateTime.Now - timetmr).ToString());
                tmrFin = true;
            }
            else
            {
                if (timetmr.AddSeconds(10) < DateTime.Now)
                {
                    Work.qLog.Enqueue(new messagetoLog("TIMER OVERFLOW", "Work", "Tmr_Elapsed", messagetoLog.messageType.error));
                    tmr.Stop();
                    tmrFin = true;
                    tmr.Start();
                }
            }
        }

        private static void JITReqReceived(comJIT com)
        {
            comJIT.Save(com);

            Work.qLog.Enqueue(new messagetoLog("ORDEN JIT: " + com.typeComm.ToString(),
                "Work", "JITReqReceived", messagetoLog.messageType.trace));

            comJIT resp = new comJIT();
          //  resp.idReq = com.id;
            resp.seq = com.seq + 1;

            JITDaemon.qJITResponses.Enqueue(resp);
        }

        /// <summary>
        /// Lógica de funcionamiento de la clase
        /// </summary>
        public static void doWork()
        {
            ActualizarEstadosCalles();

            ActualizaInfoPuestos("006");
            ActualizaInfoPuestos("007");
            ActualizaInfoPuestos("008");
            ActualizaInfoPuestos("010");
            ActualizaInfoPuestos("011");
            consultaConfiguracion();

        //     ActualizaInfoPuestos("001");
        //     ActualizaInfoPuestos("002");

            OrdenesJIT();

            RespuestasPLC();

            contador++;
        }

        private static void consultaConfiguracion()
        {
            try
            {
                string query = "select * from dbo.parametrosconfig order by codigo";
                DataTable dt = BD.ExecuteReader(query);
                if (dt != null && dt.Rows.Count > 2)
                {
                    foreach(DataRow dr in dt.Rows)
                    {
                        if (dr["codigo"].ToString().Trim() == "1")
                        {
                            PLCDaemon.maxbufferbajada = (int)dr["valor"];
                        }
                        else if ((string)dr["codigo"].ToString().Trim() == "2")
                        {
                            PLCDaemon.calle_overflow = (int)dr["valor"];
                        }
                        else if ((string)dr["codigo"].ToString().Trim() == "3")
                        {
                            PLCDaemon.calle_noreadnodata = (int)dr["valor"];
                        }
                    }
                }
                else
                {
                    PLCDaemon.maxbufferbajada = 10;
                    PLCDaemon.calle_overflow = 800;
                    PLCDaemon.calle_noreadnodata = 901;
                }
            }
            catch(Exception ex)
            {
                Work.qLog.Enqueue(new messagetoLog(ex.Message, "Work", "consultaConfiguracion", messagetoLog.messageType.error));
            }
        }

        private static void RespuestasPLC()
        {
            #region RESPUESTAS DEL PLC --------------------------------------------------------------------------
            //1. Respuesta leida en el PLC, se guarda en la BD
            if (qPLCResp.Count > 0)
                SavePLCResp();

            //2. Respuesta del PLC en la BD, se trata
            checkPLCResp();

            //3. Respuesta para el JIT en la BD, se envía al JIT
            List<comJIT> lstJ = comJIT.findPendResp();
            foreach (comJIT jc in lstJ)
            {
                comJIT jiC = JITDaemon.qJITResponses.ToList().Find(jcc => jcc.id == jc.id);
                JITDaemon.qJITResponses.Enqueue(jc);
                jc.ChangeProcAttr(true);
            }
            //Loggea en la BD y/o en archivos
            if (qLog.Count > 0)
                LogRequest();

            #endregion RESPUESTAS DEL PLC --------------------------------------------------------------------------
        }

        private static void OrdenesJIT()
        {
            #region ORDENES DEL JIT -------------------------------------------------------------------------
            //1. Orden recibida de JIT, se guarda en la BD
            List<comJIT> lst = qJITReq.ToList();
            foreach (comJIT j in lst)
            {
                JITReqReceived(qJITReq.Dequeue());
            }

            //2. Orden JIT en la BD, se genera la orden para el PLC y se guarda en la BD
            FindJITReqInBD();

            //3. Orden para el PLC en la BD, se envía al PLC
            List<comPLC> lstPLC = comPLC.Find(true);
            foreach (comPLC pc in lstPLC)
            {
                if (pc.tipoComando == comPLC.enumTipoComPLC.PCDescargaAPuesto)
                {
                    if (pc.param[1] == 6)
                    {
                        comPLC p = PLCDaemon.qCommL1.ToList().Find(pco => pco.id == pc.id);
                        if (p == null)
                            PLCDaemon.qCommL1.Enqueue(pc);
                    }
                    else if (pc.param[1] == 7)
                    {
                        comPLC p = PLCDaemon.qCommL2.ToList().Find(pco => pco.id == pc.id);
                        if (p == null)
                            PLCDaemon.qCommL2.Enqueue(pc);
                    }
                    else if (pc.param[1] == 10)
                    {
                        comPLC p = PLCDaemon.qCommL4.ToList().Find(pco => pco.id == pc.id);
                        if (p == null)
                            PLCDaemon.qCommL4.Enqueue(pc);
                    }
                    else if (pc.param[1] == 11)
                    {
                        comPLC p = PLCDaemon.qCommL5.ToList().Find(pco => pco.id == pc.id);
                        if (p == null)
                            PLCDaemon.qCommL5.Enqueue(pc);
                    }
                }
                else
                {
                    comPLC p = PLCDaemon.qCommToWrite.ToList().Find(pco => pco.id == pc.id);
                    if (p == null)
                    {
                        PLCDaemon.qCommToWrite.Enqueue(pc);
                        
                    }
                }
                pc.CambiaProcesado(true);
                Work.qLog.Enqueue(new messagetoLog("ORDEN PLC: " + pc.tipoComando.ToString(), "Work", "doWork", messagetoLog.messageType.trace));
            }
            #endregion ORDENES DEL JIT --------------------------------------------------------------------------
        }

        /// <summary>
        /// Guarda en la BD las respuestas del PLC
        /// </summary>
        private static void SavePLCResp()
        {
            List<comPLC> list = qPLCResp.ToList();
            foreach (comPLC p in list)
            {
                comPLC pc = qPLCResp.Dequeue();
                Carrier c = Carrier.Find(pc.CarrierId);

                //Si la percha ya está en estado retornando, no hago nada porque ya lo había hecho
                if (pc.tipoComando == comPLC.enumTipoComPLC.PLCPerchaLlegaAPuesto && c != null
                    && c.Estado == Carrier.enumEstadosPercha.Retornando)
                {
                    return;
                }

                comPLC origComm = comPLC.BuscaComandoPLC(pc.seq);
                if (origComm != null)
                {
                    pc.idPeticionPLC = origComm.id;
                    pc.idPeticionJIT = origComm.idPeticionJIT;
                }

                    pc.enviado = true;
                    comPLC.Save(pc);

                    //   if (pc.tipoComando == comPLC.enumTipoComPLC.PLCTagLeido)
                    //    {

                    //SI NO ASIGNÓ EL QUE YO ESPERABA, ERROR
                    //         if (origComm.CarrierId > 0 && origComm.CarrierId != pc.CarrierId)
                    //         {
                    //Marco la que yo creía como dudosa (Estado '001')
                    //      Carrier origC = Carrier.Find(origComm.CarrierId);
                    //      origC.currentState = Carrier.CStates.Doubt;
                    //      origC.Save(false);

                    //      Work.qLog.Enqueue(new messagetoLog("PERCHA ASIGNADA (" + pc.CarrierId + " DIFERENTE A LA ESPERADA " + origComm.CarrierId,
                    //          "Work", "SavePLCResp", messagetoLog.messageType.error));
                    //       }
                    //    }
                    // }

                

                /*      if (pc.typeComm == PLCComm.enumTypePLCComm.PLCPerchaSituadaEnCalle)
                      {
                          if (c != null && c.currentState == Carrier.CStates.Located)
                              return;
                      }
                      else if (pc.typeComm == PLCComm.enumTypePLCComm.PLCTagLeido)
                      {
                          if (c != null && c.currentState == Carrier.CStates.MovingDown || c.currentState == Carrier.CStates.MovingUP)
                              return;
                      }
                      else if (pc.typeComm == PLCComm.enumTypePLCComm.PLCPerchaLlegaAPuesto)
                      {
                          if (c != null && c.currentState == Carrier.CStates.Empty)
                              return;
                      }*/
            }
        }

        private static void ActualizarEstadosCalles()
        {
            int s1, s2, s3, s4;

            string query = "select calle, isnull(count(carrierid), 0) as suma from dbo.DG_InvAereo " +
                " where estado = '050' and calle > 1000 group by calle " +
                "order by Calle ";

            DataTable dt = BD.ExecuteReader(query);
            Dictionary<string, int> dict = new Dictionary<string, int>();
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    dict[(string)dr["calle"]] = (int)dr["suma"];
                }
            }

            s1 = 0;
            for (int i = 0; i <= 31; i++)
            {
                if (dict.Keys.Contains((i + 1001).ToString()))
                {
                    if (dict[(i + 1001).ToString()] > 0)
                    {
                        s1 = s1 | 1 << i;
                    }
                }
            }
            s2 = 0;
            for (int i = 0; i <= 20; i++)
            {
                if (dict.Keys.Contains((i + 2001).ToString()))
                {
                    if (dict[(i + 2001).ToString()] > 0)
                    {
                        s2 = s2 | 1 << i;
                    }
                }
            }
            s3 = 0;
            for (int i = 0; i <= 28; i++)
            {
                if (dict.Keys.Contains((i + 3001).ToString()))
                {
                    if (dict[(i + 3001).ToString()] > 0)
                    {
                        s3 = s3 | 1 << i;
                    }
                }
            }
            s4 = 0;
            for (int i = 0; i <= 16; i++)
            {
                if (dict.Keys.Contains((i + 4001).ToString()))
                {
                    if (dict[(i + 4001).ToString()] > 0)
                    {
                        s4 = s4 | 1 << i;
                    }
                }
            }

            PLCDaemon.StockTeorico1 = s1;
            PLCDaemon.StockTeorico2 = s2;
            PLCDaemon.StockTeorico3 = s3;
            PLCDaemon.StockTeorico4 = s4;
    }

        /// <summary>
        /// Consulta en la bd si hay órdenes del JIT pendientes de procesar,
        /// si las hay, genera las equivalentes para el PLC y las guarda en bd 
        /// </summary>
        private static void FindJITReqInBD()
        {
            try
            {
                List<comJIT> lst = comJIT.FindPendReq();
                if (lst.Count > 0)
                {
                    //Cada orden procedente del JIT se manda al PLC
                    foreach (comJIT jc in lst)
                    {
                        //Orden de carga. ENVIAR TANTAS ÓRDENES AL PLC COMO PERCHAS HAYA QUE MOVER.
                        if (jc.typeComm == comJIT.enumTipoComJIT.MLD_TRM)
                        {
                            comCarga(jc);
                        }
                        //Peticion de percha a puesto
                        else if (jc.typeComm == comJIT.enumTipoComJIT.MREQTRM)
                        {
                            comDescarga(jc);
                        }

                        jc.ChangeProcAttr(true);
                    }
                }
            }
            catch (Exception ex)
            {
                qLog.Enqueue(new messagetoLog("Error processing response from JIT " + ex.Message,
                    "Work", "checkJITReq", messagetoLog.messageType.error));
            }
        }

        private static Dictionary<int, int> buscaHuecos()
        {
            //Huecos que hay en las calles
            string query = " select (50 - count (CarrierId)) as huecos, dbo.ConfRef.Calle as calle " +
                               "  from dbo.DG_InvAereo " +
                                  "  right join dbo.ConfRef " +
                                  "  on dbo.DG_InvAereo.Calle = dbo.ConfRef.Calle " +
                                   " and (dbo.DG_InvAereo.Estado = '050' or dbo.DG_InvAereo.Estado = '010') " +
                                    "  group by dbo.ConfRef.Calle";

            Dictionary<int, int> Huecos = new Dictionary<int, int>();
            DataTable dt = BD.ExecuteReader(query);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    Huecos.Add((int)dr["calle"], (int)dr["huecos"]);
                }
            }
            return Huecos;
        }

        private static List<int> buscaCallesPorRef(string referencia)
        {
            //Calles en las que debería haber esa referencia
            string queryr = "select calle from dbo.confref where referencia = @p0";

            List<int> calles = new List<int>();
            DataTable dtr = BD.ExecuteReader(queryr, referencia);
            if (dtr.Rows.Count > 0)
            {
                foreach (DataRow drr in dtr.Rows)
                {
                    calles.Add((int)drr["calle"]);
                }
            }

            return calles;
        }

        private static void comDescarga(comJIT ordenJIT)
        {
            comPLC pc = new comPLC();
            pc.idPeticionJIT = ordenJIT.id;
            pc.seq = 0;
            pc.tipoComando = comPLC.enumTipoComPLC.PCDescargaAPuesto;
            enumPuestos puesto = (enumPuestos)Enum.Parse(typeof(enumPuestos),
                Enum.GetName(typeof(enumPuestos), int.Parse(ordenJIT.puesto)));

            short calle = Carrier.PeticionPerchaCalle(puesto, ordenJIT.param[1]);
            //-1 error, 0 error. Me salto la orden.
            if (calle < 1000)
            {
                qLog.Enqueue(new messagetoLog("REF NO ENCONTRADA EN ALMACÉN", "Work", 
                    "FindJITReqInBD", messagetoLog.messageType.error));
                ordenJIT.ChangeProcAttr(true);
                return;
            }

            pc.param.Add(calle); //origen
            pc.CarrierId = 0;
            pc.param.Add(short.Parse(ordenJIT.puesto)); //destino
            pc.PLCEnvia = false;
            pc.enviado = false;
            pc.orden = ordenJIT.orden;
            pc.referencia = ordenJIT.param[1];
            pc.lote = "0";
            comPLC.Save(pc);
        }

        private static void comCarga(comJIT ordenJIT) 
        {
            try
            {
                string referencia = ordenJIT.param[0];
                int numPerchas = short.Parse(ordenJIT.param[3]);
                long idPeticionJIT = ordenJIT.id;
                string puesto = ordenJIT.puesto;
                string orden = ordenJIT.orden;
                string lote = ordenJIT.param[1];

                short secuencia = 0;

                for(int i = 0; i < numPerchas; i++)
                {
                    comPLC pc = new comPLC();
                    pc.idPeticionJIT = idPeticionJIT;
                    pc.seq = secuencia;
                    pc.tipoComando = comPLC.enumTipoComPLC.PCCargaACalle;
                    pc.orden = orden;
                    pc.referencia = referencia;
                    pc.lote = lote;
                   //Destino = 0, se lo doy en el Fence
                    pc.param.Add(0);
                    //Puesto de carga, puede ser 1 o 2
                    pc.param.Add(short.Parse(puesto));
                    pc.enviado = false;
                    //No sé qué percha va a ser, la asignará después
                    pc.CarrierId = 0;
                    comPLC.Save(pc);
                    secuencia++;
                }

               /* List<int> calles = buscaCallesPorRef(referencia);
                Dictionary<int, int> Huecos = buscaHuecos();

                int situadas = 0;
                for (int i = 0; i < numPerchas; i++)
                {
                    foreach (int calle in calles)
                    {
                        if (situadas < numPerchas)
                        {
                            if (Huecos[calle] > 0)
                            {
                                creacomando(idPeticionJIT, (short)sec, (short)calle, puesto, orden, referencia, lote);

                                Huecos[calle] = Huecos[calle] - 1;

                                situadas++;
                            }
                        }
                    }
                }

                if (situadas < numPerchas)
                {
                    for (int i = situadas; i <= numPerchas; i++)
                    {
                        creacomando(idPeticionJIT, (short)sec, (short)enumCalles.Overflow, puesto, orden, referencia, lote);
                    }
                }*/
            }
            catch(Exception ex)
            {
                Work.qLog.Enqueue(new messagetoLog(ex.Message, "Work", "comCarga", messagetoLog.messageType.error));
            }
        }

        private static int BuscaHuecoUnaPercha(string referencia)
        {
            int resp = 0;
            try
            {
                List<int> calles = buscaCallesPorRef(referencia);
                Dictionary<int, int> Huecos = buscaHuecos();

                if (calles == null || calles.Count == 0)
                    return -1;

                int huecomax = 0;
                int calleElegida = 0;
                foreach (int calle in calles)
                {

                    if (Huecos[calle] > 0)
                    {
                        if (Huecos[calle] > huecomax)
                        {
                            huecomax = Huecos[calle];
                            calleElegida = calle;
                        }
                    }  
                }

                resp = calleElegida;

                return resp;
            }
            catch (Exception ex)
            {
                Work.qLog.Enqueue(new messagetoLog(ex.Message, "Work", "comCarga", messagetoLog.messageType.error));
                return -1;
            }
        }

        /// <summary>
        /// ANÁLISIS DE LAS RESPUESTAS DEL PLC
        /// </summary>
        /// <param name="comm"></param>
        private static void RespPLC(comPLC comm)
        {
            try
            {
                //1. UNA PERCHA ENTRA EN SU CALLE ************************************************************************************************************
                if (comm.tipoComando == comPLC.enumTipoComPLC.PLCPerchaSituadaEnCalle)
                {
                    comPLC PLCPet = comPLC.BuscaComandoPLC(comm.seq);
                    string referencia = "";
                    string lote = "";

                    if (PLCPet != null)
                    {
                        if (PLCPet.tipoComando == comPLC.enumTipoComPLC.PCCargaACalle)
                        {
                            //Petición recibida del JIT
                            comJIT jitReq = comJIT.Find(PLCPet.idPeticionJIT);

                            if (jitReq != null)
                            {
                                //Creo la respuesta para el JIT
                                comJIT jitResp = new comJIT();
                                jitResp.seq = jitReq.seq + 1;
                              //  jitResp.idReq = jitReq.id;
                                jitResp.typeComm = comJIT.enumTipoComJIT.PLD_RDY;
                                jitResp.orden = jitReq.orden;
                                jitResp.puesto = jitReq.puesto;                             
                                jitResp.param.Add(jitReq.param[0]);
                                jitResp.param.Add(PLCPet.param[0].ToString());
                                jitResp.param.Add(comm.CarrierId.ToString());
                                jitResp.param.Add("0");
                                comJIT.Save(jitResp);
                                referencia = jitReq.param[0];
                                lote = jitReq.param[1];
                            }
                        }
                    }

                    Carrier c = Carrier.Find(comm.CarrierId);
                    if (c == null)
                    {
                        // c = new Carrier();
                        //  c.CarrierId = comm.CarrierId;
                    }
                    else
                    {
                        c.Estado = Carrier.enumEstadosPercha.Colocada;

                        if (referencia != "")
                            c.referencia = referencia;

                        if (lote != "")
                            c.lote = lote;

                        c.calle = comm.param[0];
                        c.Save(false);
                    }
                }
                //2. LEÍDO UN TAG EN UN LECTOR ****************************************************************************************************************
                else if (comm.tipoComando == comPLC.enumTipoComPLC.PLCTagLeido)
                { //comm.param[0] = num lector que lo asigna !!!
                    comPLC PLCPet = comPLC.BuscaComandoPLC(comm.seq);
                    if (PLCPet != null)
                    {
                        //ASIGNACIÓN EN LA CARGA
                        if (PLCPet.tipoComando == comPLC.enumTipoComPLC.PCCargaACalle)
                        {
                            //Petición recibida del JIT
                            comJIT jitReq = comJIT.Find(PLCPet.idPeticionJIT);

                            if (jitReq != null)
                            {
                                //Creo la respuesta para el JIT
                                comJIT jitResp = new comJIT();
                                jitResp.seq = 0;
                              //  jitResp.seq = jitReq.seq + 1;
                              //  jitResp.idReq = jitReq.id;
                                jitResp.typeComm = comJIT.enumTipoComJIT.PLD_RDY;
                                jitResp.orden = jitReq.orden;
                                jitResp.puesto = jitReq.puesto;
                                jitResp.param.Add("");
                                jitResp.param.Add("");
                                // jitResp.param.Add(jitReq.param[0]);
                                // jitResp.param.Add(PLCPet.param[0].ToString());
                                jitResp.param.Add(comm.CarrierId.ToString());
                                jitResp.param.Add("0");
                                comJIT.Save(jitResp);
                                //Escribo la traza en el comando del PLC
                                comm.orden = jitReq.orden;
                                comm.referencia = jitReq.param[0];
                                comm.lote = jitReq.param[1];
                                comm.updateTraza();
                            }

                            //Actualizo el estado de la percha
                            Carrier c = Carrier.Find(comm.CarrierId);
                            if (c == null)
                            {
                               // c = new Carrier();
                              //  c.CarrierId = comm.CarrierId;
                              //  c.referencia = jitReq.param[0];
                              //  c.lote = jitReq.param[1];
                              //  c.orden = jitReq.orden;
                             //   c.calle = PLCPet.param[0];
                              //  c.Save(true);
                            }
                            else
                            {
                                //Percha subiendo (010)
                                c.Estado = Carrier.enumEstadosPercha.Subiendo;
                                c.referencia = jitReq.param[0];
                                c.lote = jitReq.param[1];
                                c.orden = jitReq.orden;
                                int destino = BuscaHuecoUnaPercha(c.referencia);
                                if (destino == -1)
                                {
                                    c.calle = (short)PLCDaemon.calle_noreadnodata;
                                }
                                else if (destino == 0)
                                {
                                    c.calle = (short)PLCDaemon.calle_overflow;
                                }
                                else
                                {
                                    c.calle = (short)destino;
                                }
                                c.Save(false);
                            }
                        }//ASIGNACIÓN EN LA RECIRCULACIÓN Y EN LA ENTREGA A PUESTO
                        else if (PLCPet.tipoComando == comPLC.enumTipoComPLC.PCDescargaAPuesto)
                        {
                            //Petición recibida del JIT
                            comJIT jitReq = comJIT.Find(PLCPet.idPeticionJIT);
                            string orden = "";

                            if (jitReq != null)
                            {
                                orden = jitReq.orden;
                            }

                            Carrier c = Carrier.Find(comm.CarrierId);
                            //  bool isnew = false;
                            if (c == null)
                            {
                                // c = new Carrier();
                                // c.CarrierId = comm.CarrierId;
                                //  isnew = true;
                            }
                            else
                            {
                                //ASIGNACIÓN EN LA RECIRCULACIÓN
                                if (PLCPet.param[1] > 1000)
                                {
                                    //Percha subiendo (010)
                                    c.Estado = Carrier.enumEstadosPercha.Subiendo;
                                    c.calle = PLCPet.param[1];
                                }
                                //ASIGNACIÓN EN LA ENTREGA A PUESTO
                                else
                                {
                                    //Percha bajando (020)
                                    c.Estado = Carrier.enumEstadosPercha.Bajando;
                                    c.orden = orden;
                                    //FECHA DE ULTIMA BAJANDO, ETC...
                                    comm.referencia = PLCPet.referencia;
                                    comm.lote = c.lote;
                                    comm.orden = PLCPet.orden;
                                    comm.updateTraza();
                                }

                                /*   if (PLCPet.CarrierId > 0 &&
                                       PLCPet.CarrierId != comm.CarrierId && PLCPet.CarrierId != 0)
                                   {
                                       //Marco la que yo creía como dudosa (Estado '001')
                                       //      Carrier origC = Carrier.Find(PLCPet.CarrierId);
                                       //      origC.currentState = Carrier.CStates.Doubt;
                                       //      origC.Save(false);

                                       Work.qLog.Enqueue(new messagetoLog("PERCHA ASIGNADA (" + comm.CarrierId + " DIFERENTE A LA ESPERADA " + PLCPet.CarrierId,
                                           "Work", "SavePLCResp", messagetoLog.messageType.error));
                                   }*/

                                c.Save(false);
                            }
                        } 
                    }
                }
                //3. PERCHA LLEGA A PUESTO, ENTREGADA. Número de lector en param[0] 
                else if (comm.tipoComando == comPLC.enumTipoComPLC.PLCPerchaLlegaAPuesto)
                {
                  //  bool isnew = false;
                    if (comm.CarrierId > 0)
                    {
                        Carrier c = Carrier.Find(comm.CarrierId);

                        if (c == null)
                        {
                          //  c = new Carrier();
                          //  c.CarrierId = comm.CarrierId;
                          //  isnew = true;
                        }
                        else
                        {
                            //Por si ya lo había hecho, para no repetir
                            if (c.Estado != Carrier.enumEstadosPercha.Retornando)
                            {
                                comPLC cp = comPLC.BuscaByCarrier(comm.CarrierId);
                                // if (cp != null && cp.orden.Trim() !="0" && cp.orden.Contains("-"))
                                if (cp != null)
                                {
                                    //Creo la respuesta para el JIT
                                    comJIT jitResp = new comJIT();
                                    jitResp.typeComm = comJIT.enumTipoComJIT.PTRMRDY;
                                    jitResp.isReq = false;
                                    jitResp.orden = cp.orden;
                                    if (comm.param[0] == 1)
                                        jitResp.puesto = "006";
                                    else if (comm.param[0] == 2)
                                        jitResp.puesto = "007";
                                    else if (comm.param[0] == 5)
                                        jitResp.puesto = "010";
                                    else if (comm.param[0] == 6)
                                        jitResp.puesto = "011";
                                    jitResp.param.Add(c.referencia); //referencia
                                   // if (isnew)
                                   // {
                                   //     jitResp.param.Add("0");
                                   // }
                                 //   else
                                  //  {
                                        jitResp.param.Add(c.lote); //lote
                                  //  }
                                    jitResp.param.Add(c.CarrierId.ToString()); //num de percha
                                    jitResp.param.Add("0");
                                    jitResp.procesado = false;
                                    // jitResp.idReq = 0;

                                    //Guardo el comando al JIT para enviarlo
                                    comJIT.Save(jitResp);

                                    comm.orden = cp.orden;
                                    comm.referencia = c.referencia;
                                    comm.lote = c.lote;
                                    comm.updateTraza();
                                }
                            }

                            //Cambio el estado de la percha a retornando (030)
                            c.Estado = Carrier.enumEstadosPercha.Retornando;
                            c.Save(false);
                        }
                    }

                }
                //4. PERCHA ENTRA EN VACIAS
                else if (comm.tipoComando == comPLC.enumTipoComPLC.PLCPerchaVacia)
                {
                    Carrier c = Carrier.Find(comm.CarrierId);
                //    bool isnew = false;
                    if (c == null)
                    {
                      //  c = new Carrier();
                     //   c.CarrierId = comm.CarrierId;
                      //  isnew = true;
                    }
                    c.orden = "";
                    c.referencia = "";
                    c.lote = "";
                    c.Estado = Carrier.enumEstadosPercha.Vacia;
                    c.Save(false);
                }
                //5. PREGUNTA DESTINO DE PERCHA NUEVA. comm.param[0] = numero de lector
                if (comm.tipoComando == comPLC.enumTipoComPLC.PLCPerchaNueva)
                {
                    //Si es el lector de vacías, la pongo vacía
                    if (comm.param[0] == 7)
                    {
                        Carrier c = Carrier.Find(comm.CarrierId);
                       // bool isnew = false;
                        if (c == null)
                        {
                           // c = new Carrier();
                           // c.CarrierId = comm.CarrierId;
                           // isnew = true;
                        }
                        else
                        {
                            c.orden = "";
                            c.lote = "";
                            c.referencia = "";
                            c.Estado = Carrier.enumEstadosPercha.Vacia;
                            c.Save(false);
                        }
                    }  
                    else
                    {
                        Carrier c = Carrier.Find(comm.CarrierId);

                        comPLC newcomm = new comPLC();
                        newcomm.seq = 0;
                        newcomm.idPeticionJIT = 0;
                        newcomm.idPeticionPLC = comm.id;
                        newcomm.PLCEnvia = false;
                        newcomm.procesado = false;
                        newcomm.tipoComando = comPLC.enumTipoComPLC.PCRespDestinoPerchaPerdida;
                        newcomm.enviado = false;
                        newcomm.CarrierId = (short)comm.CarrierId;
                        newcomm.param.Add((short)comm.CarrierId);
                        //Si no tengo datos de esa percha
                        if (c == null)
                        {
                          //  c = new Carrier();
                          //  c.CarrierId = comm.CarrierId;
                         //   c.calle = (short)PLCDaemon.calle_noreadnodata;
                         //   c.Estado = Carrier.enumEstadosPercha.Subiendo;
                         //   c.Save(true);
                        }
                        else
                        {
                         //   if (c.Estado == Carrier.enumEstadosPercha.Vacia ||
                         //       c.Estado == Carrier.enumEstadosPercha.Eliminada)
                        //    {
                         //       c.calle = (short)PLCDaemon.calle_noreadnodata;
                         //       c.Estado = Carrier.enumEstadosPercha.Subiendo;
                          //      c.Save(false);
                          //  }
                         //   else
                          //  {
                                //si es el lector del fence
                                // if (comm.param[0] == 9 || comm.param[0] == 10)
                                //  {
                                if (string.IsNullOrEmpty(c.referencia))
                                {
                                    c.calle = (short)PLCDaemon.calle_noreadnodata;
                                }
                                else
                                {
                                    int destino = BuscaHuecoUnaPercha(c.referencia);
                                    if (destino == -1)
                                    {
                                        c.calle = (short)PLCDaemon.calle_noreadnodata;
                                    }
                                    else if (destino == 0)
                                    {
                                        c.calle = (short)PLCDaemon.calle_overflow;
                                    }
                                    else
                                    {
                                        c.calle = (short)destino;
                                    }
                                }

                                c.Estado = Carrier.enumEstadosPercha.Subiendo;
                                c.Save(false);

                                //    }
                                /*    else
                                    {
                                        //Si está colocada, no hago nada, la mando a su calle de vuelta
                                        if (c.Estado == Carrier.enumEstadosPercha.Colocada ||
                                            c.Estado == Carrier.enumEstadosPercha.Bajando
                                            || c.Estado == Carrier.enumEstadosPercha.Retornando)
                                        {
                                            c.Estado = Carrier.enumEstadosPercha.Subiendo;
                                            c.Save(false);
                                        }
                                    }*/
                            //}
                        }
                        //Envío el destino al PLC
                        newcomm.param.Add(c.calle);
                        newcomm.referencia = c.referencia;
                        newcomm.lote = c.lote;
                        newcomm.orden = c.orden;
                        comPLC.Save(newcomm);
                    }
                }
            }
            catch (Exception ex)
            {
                qLog.Enqueue(new messagetoLog(ex.Message, "Work", "RespPLC", messagetoLog.messageType.error));
            }

        }

    private static void ActualizaInfoPuestos(string puesto)
    {
        try {
                string query = "";
                DataTable dt = null;
                string texto = "";
                if (puesto == "001" || puesto == "002")
                {
                    query = " select top 13 carrierid, orden, lote, referencia from dbo.msgplc where tipo = 5 and par1 = 0 order by ts desc";

                    dt = BD.ExecuteReader(query, puesto);

                    if (puesto == "001")
                    {
                        PLCDaemon.cargando1.Clear();
                        if (dt.Rows.Count > 0)
                        {
                            foreach (DataRow dr in dt.Rows)
                            {
                                texto = dr["carrierid"].ToString().PadLeft(5, '0') + " - " + dr["orden"].ToString();
                                PLCDaemon.cargando1.Add(texto);
                            }
                        }
                    }
                    else
                    {
                        PLCDaemon.cargando2.Clear();
                        if (dt.Rows.Count > 0)
                        {
                            foreach (DataRow dr in dt.Rows)
                            {
                                texto = dr["carrierid"].ToString().PadLeft(5, '0') + " - " + dr["orden"].ToString();
                                PLCDaemon.cargando2.Add(texto);
                            }
                        }
                    }
                }
                else
                {
                    List<string> lstServidas = new List<string>();
                    List<string> lstPedidas = new List<string>();
                    List<string> cplstbajando1 = new List<string>();
                    List<string> cplstbajando2 = new List<string>();
                    List<string> cplstbajando3 = new List<string>();
                    List<string> cplstbajando4 = new List<string>();
                    List<string> cplstbajando5 = new List<string>();

                    string orden = "";

                    short lector = 0; 
                    switch (puesto)
                    {
                        case "006": lector = 1; break;
                        case "007": lector = 2; break;
                        case "008": lector = 3; break;
                        case "010": lector = 5; break;
                        case "011": lector = 6; break;
                    }

                    string queryServidas = "select top 20 b.orden from dbo.dg_invaereo a inner join msgjit b on a.orden = b.orden where a.estado = '020' " + 
                        " and b.puesto = @p0 order by b.ts desc";

                    DataTable dtServidas = BD.ExecuteReader(queryServidas, puesto);
                    if (dtServidas != null && dtServidas.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dtServidas.Rows)
                        {
                            lstServidas.Add(dr["orden"].ToString());
                        }
                    }

                    string actual = "";
                    string queryActual = "select top 1 orden from dbo.msgplc where tipo = 6 and par1 = @p0 order by ts desc";
                    DataTable dtActual = BD.ExecuteReader(queryActual, lector);
                    if (dtActual != null && dtActual.Rows.Count > 0)
                    {
                        actual = dtActual.Rows[0]["orden"].ToString();
                    }

                    if (!actual.Contains("-"))
                        return;

                    string queryPedidas = "select top 12 orden from dbo.msgjit where espeticion = 1 and operacion = 'MREQTRM' and puesto = @p0 " +
                        " and procesado = 1 and ts > (select ts from dbo.msgjit where operacion = 'MREQTRM' and orden = " +
                        " @p1  order by ts";

                    DataTable dtPedidas = BD.ExecuteReader(queryPedidas, puesto, actual);
                    if (dtPedidas != null && dtPedidas.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dtPedidas.Rows)
                        {
                            lstPedidas.Add(dr["orden"].ToString());
                        }
                    }
                    else
                    {
                        return;
                    }

                 //   lstPedidas.Reverse();
                    lstServidas.Reverse();

                   
                  
                    if (puesto == "006")
                    {
                        cplstbajando1.Add(actual);
                        foreach (string o in lstPedidas)
                        {
                            orden = (lstServidas.Contains(o) ? o : o + "..NO..");
                            cplstbajando1.Add(orden);
                        }
                        PLCDaemon.numbajando1 = lstServidas.Count;
                        PLCDaemon.bajando1 = cplstbajando1;
                    }
                    else if (puesto == "007")
                    {
                        cplstbajando2.Add(actual);
                        foreach (string o in lstPedidas)
                        {
                            orden = (lstServidas.Contains(o) ? o : o + "..NO..");
                            cplstbajando2.Add(orden);
                        }
                        PLCDaemon.numbajando2 = lstServidas.Count;
                        PLCDaemon.bajando2 = cplstbajando2;
                    }
                    else if (puesto == "008")
                    {
                        cplstbajando3.Add(actual);
                        foreach (string o in lstPedidas)
                        {
                            orden = (lstServidas.Contains(o) ? o : o + "..NO..");
                            cplstbajando3.Add(orden);
                        }
                        PLCDaemon.numbajando3 = lstServidas.Count;
                        PLCDaemon.bajando3 = cplstbajando3;
                    }
                    else if (puesto == "010")
                    {
                        cplstbajando4.Add(actual);
                        foreach (string o in lstPedidas)
                        {
                            orden = (lstServidas.Contains(o) ? o : o + "..NO..");
                            cplstbajando4.Add(orden);
                        }
                        PLCDaemon.numbajando4 = lstServidas.Count;
                        PLCDaemon.bajando4 = cplstbajando3;
                    }
                    else if (puesto == "011")
                    {
                        cplstbajando5.Add(actual);
                        foreach (string o in lstPedidas)
                        {
                            orden = (lstServidas.Contains(o) ? o : o + "..NO..");
                            cplstbajando5.Add(orden);
                        }
                        PLCDaemon.numbajando5 = lstServidas.Count;
                        PLCDaemon.bajando5 = cplstbajando3;
                    }
                }
        }
        catch(Exception ex)
        {
            Work.qLog.Enqueue(new messagetoLog(ex.Message, "Work", "ActualizaInfoPuestos", messagetoLog.messageType.error));
        }
    }

        /// <summary>
        /// Guarda el estado de cada percha segun la respuesta del PLC
        /// y genera las respuestas para el JIT
        /// </summary>
        private static void checkPLCResp()
        {
            try
            {
                List<comPLC> lst = comPLC.Find(false);
                foreach (comPLC comm in lst)
                {
                    Work.qLog.Enqueue(new messagetoLog("RESP PLC: " + comm.tipoComando.ToString(), "Work",
                        "checkPLCResp", messagetoLog.messageType.trace));

                    //Verifico que me envía bien el número de orden (que la dirección es del PLC al PC)
                    if (comm.tipoComando != comPLC.enumTipoComPLC.PCDescargaAPuesto &&
                        comm.tipoComando != comPLC.enumTipoComPLC.PCCargaACalle &&
                        comm.tipoComando != comPLC.enumTipoComPLC.PCRespDestinoPerchaPerdida)
                    {
                        RespPLC(comm);
                    }

                    comm.CambiaProcesado(true);
                }
            }
            catch (Exception ex)
            {
                qLog.Enqueue(new messagetoLog(ex.Message, "Work", "chekPLCResp", messagetoLog.messageType.error));
            }
        }

        /// <summary>
        /// Guarda en la base de datos y archivos los mensajes pendientes de loggear 
        /// </summary>
        private static void LogRequest()
        {
            messagetoLog m = qLog.Dequeue();
            try
            {

                if (evNotification != null)
                    evNotification(m);

                if (m.type == messagetoLog.messageType.error)
                {
                    if (m.classname != "BD" && m.classname != "BDDaemon" && m.classname != "BDQuery")
                    {
                        BD.ExecuteNonQuery("INSERT INTO dbo.Log VALUES (@p0, @p1, @p2, @p3, @p4)",
                            m.ts, 0, m.classname, m.functionname, m.message);
                    }
                    else
                    {
                        Trace.TraceError("{0}     {1}", m.ts, m.message);
                    }

                }
                else if (m.type == messagetoLog.messageType.trace)
                {
                    /*
                    if (m.classname != "BD" && m.classname != "BDDaemon" && m.classname != "BDQuery")
                    {
                        BD.ExecuteNonQuery("INSERT INTO dbo.Log VALUES (@p0, @p1, @p2, @p3, @p4)",
                           m.ts, 1, m.classname, m.functionname, m.message);
                    }
                    else
                    {
                        Trace.TraceInformation("{0}     {1}", m.ts, m.message);
                    }*/
                }
                else if (m.type == messagetoLog.messageType.state_on && evNotification != null)
                {
                    evNotification(m);
                }
            }
            catch 
            {

            }
        }
    }
}
