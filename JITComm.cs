﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AereoManager
{
    /// <summary>
    /// Clase que modela un comando de comunicación con el JIT
    /// </summary>
    public class comJIT
    {
        /// <summary>
        /// Identificador del comando
        /// </summary>
        public long id;
        /// <summary>
        /// Fecha hora de envío o recepción del comando
        /// </summary>
        public DateTime ts;
        /// <summary>
        /// Número de secuencia, cada petición que envía el JIT tiene un número de secuencia y las 
        /// respuestas del PC llevan ese número incrementado en 1
        /// </summary>
        public int seq;
        /// <summary>
        /// Puesto de trabajo que envía o recibe el comando
        /// </summary>
        public string puesto;
        /// <summary>
        /// Orden de fabricación
        /// </summary>
        public string orden;
        /// <summary>
        /// Tipo de orden JIT
        /// </summary>
        public enumTipoComJIT typeComm;
        /// <summary>
        /// Parámetros de la orden JIT
        /// </summary>
        public List<string> param;
        /// <summary>
        /// True si se trató en el programa, false si está pendiente
        /// </summary>
        public bool procesado;
        /// <summary>
        /// Id de la petición correspondiente, en caso de que sea una respuesta
        /// </summary>
    //    public long idReq;
        public bool isReq;


        /// <summary>
        /// Constructor sin parámetros
        /// </summary>
        public comJIT()
        {
            param = new List<string>();
        }

        /// <summary>
        /// Tipos de comando JIT
        /// </summary>
        public enum enumTipoComJIT
        {
            /// <summary>
            /// Orden de carga de perchas solicitada por JIT
            /// </summary>
            MLD_TRM,
            /// <summary>
            /// Peticion de guarnecido a puesto de trabajo
            /// </summary>
            MREQTRM,
            /// <summary>
            /// Respuesta a MREQTRM. Presencia de percha en puesto
            /// </summary>
            PTRMRDY,
            /// <summary>
            /// Respuesta a MLD_TRM. Confirmacion de ubicacion por cada percha de la orden de carga
            /// </summary>
            PLD_RDY
        }

        /// <summary>
        /// Guarda en la bd una petición o respuesta del JIT
        /// </summary>
        /// <param name="comm">Petición o respuesta</param>
        internal static void Save(comJIT comm)
        {
            try
            {
                for(int i = comm.param.Count; i< 5; i++)
                {
                    comm.param.Add("0");
                }

                string query = "INSERT INTO dbo.MsgJIT (TS, Puesto, Operacion, Orden, Par1, Par2, Par3, Par4, Procesado, EsPeticion) " + 
                    " values (GETDATE(), @p0, @p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8)";

                int rowsAffected = BD.ExecuteNonQuery(query,
                   comm.puesto, comm.typeComm.ToString(), comm.orden, comm.param[0], comm.param[1],
                    comm.param[2], comm.param[3], comm.procesado, comm.isReq);
            }
            catch (Exception ex)
            {
                Work.qLog.Enqueue(new messagetoLog("Error guardando una petición de JIT en bd: " + ex.Message,
                    "JITComm", "Save", messagetoLog.messageType.error));
            }
        }

        /// <summary>
        /// Busca las ordenes JIT no procesadas 
        /// </summary>
        /// <returns>Ordenes JIT no procesadas</returns>
        internal static List<comJIT> FindPendReq()
        {
            List<comJIT> res = new List<comJIT>();

            try
            {
                DataTable dt = BD.ExecuteReader("select * from dbo.MsgJIT where Procesado = 0 and EsPeticion = 1 order by TS");

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        res.Add(instanciar(row));
                    }
                }
            }
            catch (Exception ex)
            {
                Work.qLog.Enqueue(new messagetoLog("Error buscando órdenes pendientes para el JIT: " + ex.Message,
                    "JITComm", "FindPendReq", messagetoLog.messageType.error));
            }

            return res;
        }

        /// <summary>
        /// Instancia un comando JIT
        /// </summary>
        /// <param name="row">Fila de la bd</param>
        /// <returns></returns>
        private static comJIT instanciar(DataRow row)
        {
            comJIT j = new comJIT();
            j.id = (long)row["Id"];
            j.ts = (DateTime)row["TS"];
            j.puesto = (string)row["Puesto"];
            j.typeComm = (enumTipoComJIT)Enum.Parse(typeof(enumTipoComJIT), (string)row["Operacion"]);
            j.orden = (string)row["Orden"];
            j.param.Add((string)row["Par1"]);
            j.param.Add((string)row["Par2"]);
            j.param.Add((string)row["Par3"]);
            j.param.Add((string)row["Par4"]);
            j.procesado = (bool)row["Procesado"];
         //   j.idReq = (long)row["IdPeticion"];
            j.isReq = (bool)row["EsPeticion"];
            return j;
        }

        /// <summary>
        /// Cambia el valor del atribuito Procesado en la bd
        /// </summary>
        /// <param name="newValue">Nuevo valor para el atributo processed</param>
        internal void ChangeProcAttr(bool newValue)
        {
            try
            {
                this.procesado = newValue;

                string query = "UPDATE dbo.MsgJIT set Procesado = @p0 WHERE Id = @p1";
                int rowsAffected = BD.ExecuteNonQuery(query, newValue, this.id);
            }
            catch (Exception ex)
            {
                Work.qLog.Enqueue(new messagetoLog("Error changing processed attribute: " + ex.Message,
                    "JITComm", "ChangeProcAttr", messagetoLog.messageType.error));
            }
        }

        /// <summary>
        /// Busca una orden JIT en la BD sabiendo el Id
        /// </summary>
        /// <param name="idJITReq">Id de la orden JIT</param>
        /// <returns></returns>
        internal static comJIT Find(long idJITReq)
        {
            comJIT res = null;

            try
            {
                DataTable dt = BD.ExecuteReader("select * from dbo.MsgJIT where Id = @p0", idJITReq);


                if (dt != null && dt.Rows.Count > 0)
                {
                    res = instanciar(dt.Rows[0]);
                }
            }
            catch (Exception ex)
            {
                Work.qLog.Enqueue(new messagetoLog("Error buscando la petición del JIT sabiendo el Id: " + ex.Message,
                    "JITComm", "Find", messagetoLog.messageType.error));
            }

            return res;
        }

        /// <summary>
        /// Busca si hay respuestas para enviar al JIT en la BD
        /// </summary>
        /// <returns></returns>
        internal static List<comJIT> findPendResp()
        {
            List<comJIT> lstResp = new List<comJIT>();

            try
            {
                string query = "SELECT * FROM dbo.MsgJIT WHERE Procesado = 0 AND EsPeticion = 0 ORDER BY TS";
                DataTable dt = BD.ExecuteReader(query);
                
                foreach (DataRow dr in dt.Rows)
                {
                    lstResp.Add(instanciar(dr));
                }
            }
            catch (Exception ex)
            {
                Work.qLog.Enqueue(new messagetoLog("Error recuperando de la bd las respuestas para el JIT: " + ex.Message,
                   "JITComm", "findPendResp", messagetoLog.messageType.error));
            }
            return lstResp;
        }

        /// <summary>
        /// Calcula el mensaje de petición de datos al JIT según la secuencia
        /// del último que haya en la base de datos
        /// </summary>
        /// <returns>Mensaje tal cual para enviar</returns>
        internal static string GetMsgPoll()
        {
            string seq = "0000";
           
            string query = "select IsNull(max(id), 0) + 1 as seq from dbo.MsgJIT";
            DataTable dt = BD.ExecuteReader(query);
            if (dt.Rows.Count > 0)
            {
                seq = dt.Rows[0]["seq"].ToString().PadLeft(4, '0');
            }

            return seq + "05000000<EF>"; 
        }
    }
}
