﻿namespace AereoManager
{
    partial class frmManualOrders
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel3 = new System.Windows.Forms.Panel();
            this.cbows1 = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.lblError = new System.Windows.Forms.Label();
            this.txtOrder2 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtOrder1 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cboWS = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cboRef2 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnSend2 = new System.Windows.Forms.Button();
            this.nudNumCarr = new System.Windows.Forms.NumericUpDown();
            this.txtLot1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cboRef1 = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnSend1 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.cboOrigen = new System.Windows.Forms.ComboBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudNumCarr)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.Control;
            this.panel3.Controls.Add(this.panel1);
            this.panel3.Controls.Add(this.cbows1);
            this.panel3.Controls.Add(this.label10);
            this.panel3.Controls.Add(this.lblError);
            this.panel3.Controls.Add(this.txtOrder2);
            this.panel3.Controls.Add(this.label9);
            this.panel3.Controls.Add(this.txtOrder1);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.cboWS);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.btnSend2);
            this.panel3.Controls.Add(this.nudNumCarr);
            this.panel3.Controls.Add(this.txtLot1);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.cboRef1);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.label7);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.btnSend1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(761, 313);
            this.panel3.TabIndex = 41;
            // 
            // cbows1
            // 
            this.cbows1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbows1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbows1.BackColor = System.Drawing.Color.White;
            this.cbows1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbows1.
                Enabled = true;
            this.cbows1.Items.AddRange(new object[] {
            "001",
            "002"});
            this.cbows1.Location = new System.Drawing.Point(111, 177);
            this.cbows1.Name = "cbows1";
            this.cbows1.Size = new System.Drawing.Size(116, 23);
            this.cbows1.TabIndex = 26;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(50, 185);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(49, 15);
            this.label10.TabIndex = 25;
            this.label10.Text = "Puesto";
            // 
            // lblError
            // 
            this.lblError.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblError.ForeColor = System.Drawing.Color.Red;
            this.lblError.Location = new System.Drawing.Point(0, 286);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(761, 27);
            this.lblError.TabIndex = 24;
            // 
            // txtOrder2
            // 
            this.txtOrder2.Location = new System.Drawing.Point(450, 58);
            this.txtOrder2.Name = "txtOrder2";
            this.txtOrder2.Size = new System.Drawing.Size(125, 23);
            this.txtOrder2.TabIndex = 23;
            this.txtOrder2.Text = "MANUAL";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(391, 61);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(42, 15);
            this.label9.TabIndex = 22;
            this.label9.Text = "Orden";
            // 
            // txtOrder1
            // 
            this.txtOrder1.Location = new System.Drawing.Point(111, 75);
            this.txtOrder1.Name = "txtOrder1";
            this.txtOrder1.Size = new System.Drawing.Size(116, 23);
            this.txtOrder1.TabIndex = 21;
            this.txtOrder1.Text = "MANUAL";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(57, 78);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 15);
            this.label4.TabIndex = 20;
            this.label4.Text = "Orden";
            // 
            // cboWS
            // 
            this.cboWS.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboWS.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboWS.BackColor = System.Drawing.Color.White;
            this.cboWS.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboWS.FormattingEnabled = true;
            this.cboWS.Location = new System.Drawing.Point(450, 208);
            this.cboWS.Name = "cboWS";
            this.cboWS.Size = new System.Drawing.Size(168, 23);
            this.cboWS.TabIndex = 19;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(367, 211);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 15);
            this.label2.TabIndex = 18;
            this.label2.Text = "Puesto";
            // 
            // cboRef2
            // 
            this.cboRef2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboRef2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboRef2.BackColor = System.Drawing.Color.White;
            this.cboRef2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboRef2.Enabled = false;
            this.cboRef2.FormattingEnabled = true;
            this.cboRef2.Location = new System.Drawing.Point(198, 43);
            this.cboRef2.Name = "cboRef2";
            this.cboRef2.Size = new System.Drawing.Size(147, 23);
            this.cboRef2.TabIndex = 17;
            this.cboRef2.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(369, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(208, 18);
            this.label3.TabIndex = 16;
            this.label3.Text = "PETICIÓN DE PERCHA MANUAL";
            // 
            // btnSend2
            // 
            this.btnSend2.Location = new System.Drawing.Point(438, 250);
            this.btnSend2.Name = "btnSend2";
            this.btnSend2.Size = new System.Drawing.Size(87, 27);
            this.btnSend2.TabIndex = 13;
            this.btnSend2.Text = "Enviar";
            this.btnSend2.UseVisualStyleBackColor = true;
            this.btnSend2.Click += new System.EventHandler(this.btnSend2_Click);
            // 
            // nudNumCarr
            // 
            this.nudNumCarr.Location = new System.Drawing.Point(121, 220);
            this.nudNumCarr.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudNumCarr.Name = "nudNumCarr";
            this.nudNumCarr.Size = new System.Drawing.Size(115, 23);
            this.nudNumCarr.TabIndex = 12;
            this.nudNumCarr.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nudNumCarr.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // txtLot1
            // 
            this.txtLot1.Location = new System.Drawing.Point(111, 137);
            this.txtLot1.Name = "txtLot1";
            this.txtLot1.Size = new System.Drawing.Size(116, 23);
            this.txtLot1.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(57, 140);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 15);
            this.label1.TabIndex = 10;
            this.label1.Text = "Lote";
            // 
            // cboRef1
            // 
            this.cboRef1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboRef1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboRef1.BackColor = System.Drawing.Color.White;
            this.cboRef1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboRef1.FormattingEnabled = true;
            this.cboRef1.Location = new System.Drawing.Point(111, 105);
            this.cboRef1.Name = "cboRef1";
            this.cboRef1.Size = new System.Drawing.Size(147, 23);
            this.cboRef1.TabIndex = 9;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(31, 28);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(176, 18);
            this.label8.TabIndex = 7;
            this.label8.Text = "ORDEN DE CARGA MANUAL";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(37, 220);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 15);
            this.label7.TabIndex = 6;
            this.label7.Text = "Nº perchas";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(28, 108);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 15);
            this.label6.TabIndex = 4;
            this.label6.Text = "Referencia";
            // 
            // btnSend1
            // 
            this.btnSend1.Location = new System.Drawing.Point(111, 256);
            this.btnSend1.Name = "btnSend1";
            this.btnSend1.Size = new System.Drawing.Size(87, 27);
            this.btnSend1.TabIndex = 0;
            this.btnSend1.Text = "Enviar";
            this.btnSend1.UseVisualStyleBackColor = true;
            this.btnSend1.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.textBox1);
            this.panel1.Controls.Add(this.radioButton2);
            this.panel1.Controls.Add(this.cboOrigen);
            this.panel1.Controls.Add(this.radioButton1);
            this.panel1.Controls.Add(this.cboRef2);
            this.panel1.Location = new System.Drawing.Point(370, 100);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(360, 100);
            this.panel1.TabIndex = 27;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(13, 3);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(144, 19);
            this.radioButton1.TabIndex = 0;
            this.radioButton1.Text = "Seleccionar calle";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Checked = true;
            this.radioButton2.Location = new System.Drawing.Point(13, 44);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(179, 19);
            this.radioButton2.TabIndex = 18;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Seleccionar referencia";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // cboOrigen
            // 
            this.cboOrigen.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboOrigen.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboOrigen.BackColor = System.Drawing.Color.White;
            this.cboOrigen.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboOrigen.Enabled = false;
            this.cboOrigen.FormattingEnabled = true;
            this.cboOrigen.Location = new System.Drawing.Point(198, 5);
            this.cboOrigen.Name = "cboOrigen";
            this.cboOrigen.Size = new System.Drawing.Size(147, 23);
            this.cboOrigen.TabIndex = 19;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(149, 69);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(153, 23);
            this.textBox1.TabIndex = 20;
            this.textBox1.Text = "193040";
            // 
            // frmManualOrders
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(761, 313);
            this.Controls.Add(this.panel3);
            this.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "frmManualOrders";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Carga y descarga manual";
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudNumCarr)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnSend1;
        private System.Windows.Forms.ComboBox cboRef1;
        private System.Windows.Forms.NumericUpDown nudNumCarr;
        private System.Windows.Forms.TextBox txtLot1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboWS;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cboRef2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnSend2;
        private System.Windows.Forms.TextBox txtOrder2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtOrder1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblError;
        private System.Windows.Forms.ComboBox cbows1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.ComboBox cboOrigen;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.TextBox textBox1;
    }
}