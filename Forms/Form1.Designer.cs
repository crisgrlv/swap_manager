﻿namespace AereoManager
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.sCMonitPerchas = new System.Windows.Forms.SplitContainer();
            this.tlpTransit = new System.Windows.Forms.TableLayoutPanel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtCarrierId = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.txtNuevaRef = new System.Windows.Forms.TextBox();
            this.lblError = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.lblmaxbuffer = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.lblbuffer3 = new System.Windows.Forms.Label();
            this.lblbuffer1 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lblbuffer2 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.lbBajando3 = new System.Windows.Forms.ListBox();
            this.label7 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lbBajando2 = new System.Windows.Forms.ListBox();
            this.label6 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.txtCalle = new System.Windows.Forms.TextBox();
            this.button6 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbBajando1 = new System.Windows.Forms.ListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tlpCol = new System.Windows.Forms.TableLayoutPanel();
            this.dgvColocadas4 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColocadas3 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColocadas2 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColocadas1 = new System.Windows.Forms.DataGridView();
            this.colCalle1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPrimPercha1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colTotal1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label5 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.tsmiMC = new System.Windows.Forms.ToolStripMenuItem();
            this.cargaDescargaManualToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.recircularPerchasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.motoresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eVEntradaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pLCToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiSeeDBcarrierstate = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiDBOrdenesCarga = new System.Windows.Forms.ToolStripMenuItem();
            this.dBPerchasEnPuestoDeCarga1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dBPerchasCargadasEnPuesto002ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.comunicaciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gestiónDePerchasDudosasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiConfRef = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiIdiomas = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiEspañol = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiPortugues = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiAcercaDe = new System.Windows.Forms.ToolStripMenuItem();
            this.lblStatePLC = new System.Windows.Forms.ToolStripStatusLabel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.sCMonitPerchas)).BeginInit();
            this.sCMonitPerchas.Panel1.SuspendLayout();
            this.sCMonitPerchas.Panel2.SuspendLayout();
            this.sCMonitPerchas.SuspendLayout();
            this.tlpTransit.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tlpCol.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvColocadas4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvColocadas3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvColocadas2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvColocadas1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // sCMonitPerchas
            // 
            this.sCMonitPerchas.BackColor = System.Drawing.SystemColors.ControlDark;
            this.sCMonitPerchas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sCMonitPerchas.Location = new System.Drawing.Point(0, 25);
            this.sCMonitPerchas.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.sCMonitPerchas.Name = "sCMonitPerchas";
            // 
            // sCMonitPerchas.Panel1
            // 
            this.sCMonitPerchas.Panel1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.sCMonitPerchas.Panel1.Controls.Add(this.tlpTransit);
            this.sCMonitPerchas.Panel1.Padding = new System.Windows.Forms.Padding(5, 3, 5, 5);
            // 
            // sCMonitPerchas.Panel2
            // 
            this.sCMonitPerchas.Panel2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.sCMonitPerchas.Panel2.Controls.Add(this.tlpCol);
            this.sCMonitPerchas.Panel2.Controls.Add(this.label5);
            this.sCMonitPerchas.Panel2.Padding = new System.Windows.Forms.Padding(5);
            this.sCMonitPerchas.Size = new System.Drawing.Size(1290, 578);
            this.sCMonitPerchas.SplitterDistance = 593;
            this.sCMonitPerchas.SplitterWidth = 3;
            this.sCMonitPerchas.TabIndex = 39;
            // 
            // tlpTransit
            // 
            this.tlpTransit.BackColor = System.Drawing.SystemColors.Control;
            this.tlpTransit.ColumnCount = 3;
            this.tlpTransit.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpTransit.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tlpTransit.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tlpTransit.Controls.Add(this.panel6, 1, 1);
            this.tlpTransit.Controls.Add(this.panel5, 0, 1);
            this.tlpTransit.Controls.Add(this.panel4, 2, 0);
            this.tlpTransit.Controls.Add(this.panel2, 1, 0);
            this.tlpTransit.Controls.Add(this.panel3, 2, 1);
            this.tlpTransit.Controls.Add(this.panel1, 0, 0);
            this.tlpTransit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpTransit.Location = new System.Drawing.Point(5, 3);
            this.tlpTransit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tlpTransit.Name = "tlpTransit";
            this.tlpTransit.RowCount = 2;
            this.tlpTransit.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tlpTransit.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tlpTransit.Size = new System.Drawing.Size(583, 570);
            this.tlpTransit.TabIndex = 46;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.panel8);
            this.panel6.Controls.Add(this.lblError);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(197, 288);
            this.panel6.Name = "panel6";
            this.panel6.Padding = new System.Windows.Forms.Padding(2);
            this.panel6.Size = new System.Drawing.Size(188, 279);
            this.panel6.TabIndex = 48;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.Gainsboro;
            this.panel8.Controls.Add(this.label2);
            this.panel8.Controls.Add(this.button2);
            this.panel8.Controls.Add(this.button4);
            this.panel8.Controls.Add(this.label1);
            this.panel8.Controls.Add(this.txtCarrierId);
            this.panel8.Controls.Add(this.button1);
            this.panel8.Controls.Add(this.txtNuevaRef);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(2, 2);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(184, 275);
            this.panel8.TabIndex = 19;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(3, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 14);
            this.label2.TabIndex = 14;
            this.label2.Text = "Tag:";
            // 
            // button2
            // 
            this.button2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button2.Location = new System.Drawing.Point(54, 55);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(115, 38);
            this.button2.TabIndex = 13;
            this.button2.Text = "Dar de baja la percha";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button4
            // 
            this.button4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button4.Location = new System.Drawing.Point(52, 171);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(115, 36);
            this.button4.TabIndex = 15;
            this.button4.Text = "Cambiar referencia";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click_1);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(6, 140);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 14);
            this.label1.TabIndex = 17;
            this.label1.Text = "Nueva ref:";
            // 
            // txtCarrierId
            // 
            this.txtCarrierId.Location = new System.Drawing.Point(43, 16);
            this.txtCarrierId.Name = "txtCarrierId";
            this.txtCarrierId.Size = new System.Drawing.Size(55, 22);
            this.txtCarrierId.TabIndex = 11;
            this.txtCarrierId.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // button1
            // 
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.Location = new System.Drawing.Point(101, 16);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(78, 23);
            this.button1.TabIndex = 12;
            this.button1.Text = "Ver datos";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // txtNuevaRef
            // 
            this.txtNuevaRef.Location = new System.Drawing.Point(83, 137);
            this.txtNuevaRef.Name = "txtNuevaRef";
            this.txtNuevaRef.Size = new System.Drawing.Size(93, 22);
            this.txtNuevaRef.TabIndex = 16;
            this.txtNuevaRef.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblError
            // 
            this.lblError.ForeColor = System.Drawing.Color.Red;
            this.lblError.Location = new System.Drawing.Point(11, 243);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(170, 20);
            this.lblError.TabIndex = 18;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.panel9);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(3, 288);
            this.panel5.Name = "panel5";
            this.panel5.Padding = new System.Windows.Forms.Padding(2);
            this.panel5.Size = new System.Drawing.Size(188, 279);
            this.panel5.TabIndex = 47;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.Gainsboro;
            this.panel9.Controls.Add(this.lblmaxbuffer);
            this.panel9.Controls.Add(this.label9);
            this.panel9.Controls.Add(this.button5);
            this.panel9.Controls.Add(this.label8);
            this.panel9.Controls.Add(this.lblbuffer3);
            this.panel9.Controls.Add(this.lblbuffer1);
            this.panel9.Controls.Add(this.label13);
            this.panel9.Controls.Add(this.label11);
            this.panel9.Controls.Add(this.lblbuffer2);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel9.Location = new System.Drawing.Point(2, 2);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(184, 275);
            this.panel9.TabIndex = 6;
            // 
            // lblmaxbuffer
            // 
            this.lblmaxbuffer.AutoSize = true;
            this.lblmaxbuffer.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblmaxbuffer.Location = new System.Drawing.Point(105, 186);
            this.lblmaxbuffer.Name = "lblmaxbuffer";
            this.lblmaxbuffer.Size = new System.Drawing.Size(21, 15);
            this.lblmaxbuffer.TabIndex = 16;
            this.lblmaxbuffer.Text = "xx";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(22, 186);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(77, 15);
            this.label9.TabIndex = 15;
            this.label9.Text = "Conf. max:";
            // 
            // button5
            // 
            this.button5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button5.Location = new System.Drawing.Point(13, 216);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(151, 52);
            this.button5.TabIndex = 14;
            this.button5.Text = "Vaciar lista de perchas bajando";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(14, 18);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(154, 14);
            this.label8.TabIndex = 0;
            this.label8.Text = "Bajando a línea 1000:";
            // 
            // lblbuffer3
            // 
            this.lblbuffer3.AutoSize = true;
            this.lblbuffer3.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblbuffer3.Location = new System.Drawing.Point(60, 146);
            this.lblbuffer3.Name = "lblbuffer3";
            this.lblbuffer3.Size = new System.Drawing.Size(21, 15);
            this.lblbuffer3.TabIndex = 5;
            this.lblbuffer3.Text = "xx";
            // 
            // lblbuffer1
            // 
            this.lblbuffer1.AutoSize = true;
            this.lblbuffer1.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblbuffer1.Location = new System.Drawing.Point(60, 44);
            this.lblbuffer1.Name = "lblbuffer1";
            this.lblbuffer1.Size = new System.Drawing.Size(21, 15);
            this.lblbuffer1.TabIndex = 1;
            this.lblbuffer1.Text = "xx";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(14, 120);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(154, 14);
            this.label13.TabIndex = 4;
            this.label13.Text = "Bajando a línea 3000:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(14, 68);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(154, 14);
            this.label11.TabIndex = 2;
            this.label11.Text = "Bajando a línea 2000:";
            // 
            // lblbuffer2
            // 
            this.lblbuffer2.AutoSize = true;
            this.lblbuffer2.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblbuffer2.Location = new System.Drawing.Point(60, 94);
            this.lblbuffer2.Name = "lblbuffer2";
            this.lblbuffer2.Size = new System.Drawing.Size(21, 15);
            this.lblbuffer2.TabIndex = 3;
            this.lblbuffer2.Text = "xx";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.lbBajando3);
            this.panel4.Controls.Add(this.label7);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(391, 3);
            this.panel4.Name = "panel4";
            this.panel4.Padding = new System.Windows.Forms.Padding(2);
            this.panel4.Size = new System.Drawing.Size(189, 279);
            this.panel4.TabIndex = 46;
            // 
            // lbBajando3
            // 
            this.lbBajando3.BackColor = System.Drawing.Color.Gainsboro;
            this.lbBajando3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lbBajando3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbBajando3.ForeColor = System.Drawing.Color.Black;
            this.lbBajando3.FormattingEnabled = true;
            this.lbBajando3.ItemHeight = 14;
            this.lbBajando3.Location = new System.Drawing.Point(2, 32);
            this.lbBajando3.Margin = new System.Windows.Forms.Padding(5);
            this.lbBajando3.Name = "lbBajando3";
            this.lbBajando3.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.lbBajando3.Size = new System.Drawing.Size(185, 245);
            this.lbBajando3.TabIndex = 40;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.Gainsboro;
            this.label7.Dock = System.Windows.Forms.DockStyle.Top;
            this.label7.Font = new System.Drawing.Font("Consolas", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(2, 2);
            this.label7.Margin = new System.Windows.Forms.Padding(5);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(185, 30);
            this.label7.TabIndex = 41;
            this.label7.Text = "BAJANDO A LÍNEA 3000:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.lbBajando2);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(197, 3);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(2);
            this.panel2.Size = new System.Drawing.Size(188, 279);
            this.panel2.TabIndex = 45;
            // 
            // lbBajando2
            // 
            this.lbBajando2.BackColor = System.Drawing.Color.Gainsboro;
            this.lbBajando2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lbBajando2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbBajando2.ForeColor = System.Drawing.Color.Black;
            this.lbBajando2.FormattingEnabled = true;
            this.lbBajando2.ItemHeight = 14;
            this.lbBajando2.Location = new System.Drawing.Point(2, 32);
            this.lbBajando2.Margin = new System.Windows.Forms.Padding(5);
            this.lbBajando2.Name = "lbBajando2";
            this.lbBajando2.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.lbBajando2.Size = new System.Drawing.Size(184, 245);
            this.lbBajando2.TabIndex = 40;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.Gainsboro;
            this.label6.Dock = System.Windows.Forms.DockStyle.Top;
            this.label6.Font = new System.Drawing.Font("Consolas", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(2, 2);
            this.label6.Margin = new System.Windows.Forms.Padding(5);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(184, 30);
            this.label6.TabIndex = 41;
            this.label6.Text = "BAJANDO A LÍNEA 2000:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.Control;
            this.panel3.Controls.Add(this.panel7);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(391, 288);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(189, 279);
            this.panel3.TabIndex = 6;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.Gainsboro;
            this.panel7.Controls.Add(this.label10);
            this.panel7.Controls.Add(this.txtCalle);
            this.panel7.Controls.Add(this.button6);
            this.panel7.Controls.Add(this.label3);
            this.panel7.Controls.Add(this.textBox1);
            this.panel7.Controls.Add(this.button3);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(0, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(189, 279);
            this.panel7.TabIndex = 7;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(26, 195);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(49, 14);
            this.label10.TabIndex = 9;
            this.label10.Text = "Calle:";
            // 
            // txtCalle
            // 
            this.txtCalle.Location = new System.Drawing.Point(81, 192);
            this.txtCalle.Name = "txtCalle";
            this.txtCalle.Size = new System.Drawing.Size(78, 22);
            this.txtCalle.TabIndex = 8;
            this.txtCalle.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // button6
            // 
            this.button6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button6.Location = new System.Drawing.Point(29, 230);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(130, 40);
            this.button6.TabIndex = 7;
            this.button6.Text = "Marcar calle vacía";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(26, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 14);
            this.label3.TabIndex = 6;
            this.label3.Text = "Ref:";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(66, 15);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(93, 22);
            this.textBox1.TabIndex = 2;
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // button3
            // 
            this.button3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button3.Location = new System.Drawing.Point(29, 51);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(130, 68);
            this.button3.TabIndex = 3;
            this.button3.Text = "Buscar referencia en almacén";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.lbBajando1);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(2);
            this.panel1.Size = new System.Drawing.Size(188, 279);
            this.panel1.TabIndex = 44;
            // 
            // lbBajando1
            // 
            this.lbBajando1.BackColor = System.Drawing.Color.Gainsboro;
            this.lbBajando1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lbBajando1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbBajando1.ForeColor = System.Drawing.Color.Black;
            this.lbBajando1.FormattingEnabled = true;
            this.lbBajando1.ItemHeight = 14;
            this.lbBajando1.Location = new System.Drawing.Point(2, 32);
            this.lbBajando1.Margin = new System.Windows.Forms.Padding(5);
            this.lbBajando1.Name = "lbBajando1";
            this.lbBajando1.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.lbBajando1.Size = new System.Drawing.Size(184, 245);
            this.lbBajando1.TabIndex = 40;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Gainsboro;
            this.label4.Dock = System.Windows.Forms.DockStyle.Top;
            this.label4.Font = new System.Drawing.Font("Consolas", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(2, 2);
            this.label4.Margin = new System.Windows.Forms.Padding(5);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(184, 30);
            this.label4.TabIndex = 41;
            this.label4.Text = "BAJANDO A LÍNEA 1000:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tlpCol
            // 
            this.tlpCol.BackColor = System.Drawing.SystemColors.Control;
            this.tlpCol.ColumnCount = 4;
            this.tlpCol.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlpCol.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlpCol.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlpCol.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlpCol.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpCol.Controls.Add(this.dgvColocadas4, 3, 0);
            this.tlpCol.Controls.Add(this.dgvColocadas3, 2, 0);
            this.tlpCol.Controls.Add(this.dgvColocadas2, 1, 0);
            this.tlpCol.Controls.Add(this.dgvColocadas1, 0, 0);
            this.tlpCol.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpCol.Location = new System.Drawing.Point(5, 30);
            this.tlpCol.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tlpCol.Name = "tlpCol";
            this.tlpCol.RowCount = 1;
            this.tlpCol.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpCol.Size = new System.Drawing.Size(684, 543);
            this.tlpCol.TabIndex = 39;
            // 
            // dgvColocadas4
            // 
            this.dgvColocadas4.AllowUserToAddRows = false;
            this.dgvColocadas4.AllowUserToDeleteRows = false;
            this.dgvColocadas4.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvColocadas4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvColocadas4.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dgvColocadas4.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvColocadas4.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvColocadas4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvColocadas4.ColumnHeadersVisible = false;
            this.dgvColocadas4.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9});
            this.dgvColocadas4.Cursor = System.Windows.Forms.Cursors.Hand;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.NullValue = "0";
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvColocadas4.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgvColocadas4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvColocadas4.EnableHeadersVisualStyles = false;
            this.dgvColocadas4.GridColor = System.Drawing.SystemColors.Control;
            this.dgvColocadas4.Location = new System.Drawing.Point(523, 2);
            this.dgvColocadas4.Margin = new System.Windows.Forms.Padding(10, 2, 10, 2);
            this.dgvColocadas4.MultiSelect = false;
            this.dgvColocadas4.Name = "dgvColocadas4";
            this.dgvColocadas4.ReadOnly = true;
            this.dgvColocadas4.RowHeadersVisible = false;
            this.dgvColocadas4.RowTemplate.DividerHeight = 1;
            this.dgvColocadas4.RowTemplate.Height = 20;
            this.dgvColocadas4.RowTemplate.ReadOnly = true;
            this.dgvColocadas4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvColocadas4.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvColocadas4.Size = new System.Drawing.Size(151, 539);
            this.dgvColocadas4.TabIndex = 36;
            this.dgvColocadas4.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvColocadas_CellFormatting);
            this.dgvColocadas4.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvColocadas1_CellMouseClick);
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.dataGridViewTextBoxColumn7.DataPropertyName = "Calle";
            this.dataGridViewTextBoxColumn7.HeaderText = "Calle";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Width = 5;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn8.DataPropertyName = "CarrierId";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn8.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewTextBoxColumn8.HeaderText = "1ª";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.dataGridViewTextBoxColumn9.DataPropertyName = "suma";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn9.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewTextBoxColumn9.HeaderText = "Total";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.Width = 5;
            // 
            // dgvColocadas3
            // 
            this.dgvColocadas3.AllowUserToAddRows = false;
            this.dgvColocadas3.AllowUserToDeleteRows = false;
            this.dgvColocadas3.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvColocadas3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvColocadas3.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dgvColocadas3.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvColocadas3.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvColocadas3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvColocadas3.ColumnHeadersVisible = false;
            this.dgvColocadas3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6});
            this.dgvColocadas3.Cursor = System.Windows.Forms.Cursors.Hand;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.NullValue = "0";
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvColocadas3.DefaultCellStyle = dataGridViewCellStyle8;
            this.dgvColocadas3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvColocadas3.EnableHeadersVisualStyles = false;
            this.dgvColocadas3.GridColor = System.Drawing.SystemColors.Control;
            this.dgvColocadas3.Location = new System.Drawing.Point(347, 2);
            this.dgvColocadas3.Margin = new System.Windows.Forms.Padding(5, 2, 5, 2);
            this.dgvColocadas3.MultiSelect = false;
            this.dgvColocadas3.Name = "dgvColocadas3";
            this.dgvColocadas3.ReadOnly = true;
            this.dgvColocadas3.RowHeadersVisible = false;
            this.dgvColocadas3.RowTemplate.DividerHeight = 1;
            this.dgvColocadas3.RowTemplate.Height = 20;
            this.dgvColocadas3.RowTemplate.ReadOnly = true;
            this.dgvColocadas3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvColocadas3.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvColocadas3.Size = new System.Drawing.Size(161, 539);
            this.dgvColocadas3.TabIndex = 35;
            this.dgvColocadas3.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvColocadas_CellFormatting);
            this.dgvColocadas3.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvColocadas1_CellMouseClick);
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.dataGridViewTextBoxColumn4.DataPropertyName = "Calle";
            this.dataGridViewTextBoxColumn4.HeaderText = "Calle";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 5;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn5.DataPropertyName = "CarrierId";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn5.DefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridViewTextBoxColumn5.HeaderText = "1ª";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.dataGridViewTextBoxColumn6.DataPropertyName = "suma";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridViewTextBoxColumn6.HeaderText = "Total";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Width = 5;
            // 
            // dgvColocadas2
            // 
            this.dgvColocadas2.AllowUserToAddRows = false;
            this.dgvColocadas2.AllowUserToDeleteRows = false;
            this.dgvColocadas2.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvColocadas2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvColocadas2.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dgvColocadas2.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvColocadas2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dgvColocadas2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvColocadas2.ColumnHeadersVisible = false;
            this.dgvColocadas2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3});
            this.dgvColocadas2.Cursor = System.Windows.Forms.Cursors.Hand;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle12.NullValue = "0";
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvColocadas2.DefaultCellStyle = dataGridViewCellStyle12;
            this.dgvColocadas2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvColocadas2.EnableHeadersVisualStyles = false;
            this.dgvColocadas2.GridColor = System.Drawing.SystemColors.Control;
            this.dgvColocadas2.Location = new System.Drawing.Point(176, 2);
            this.dgvColocadas2.Margin = new System.Windows.Forms.Padding(5, 2, 5, 2);
            this.dgvColocadas2.MultiSelect = false;
            this.dgvColocadas2.Name = "dgvColocadas2";
            this.dgvColocadas2.ReadOnly = true;
            this.dgvColocadas2.RowHeadersVisible = false;
            this.dgvColocadas2.RowTemplate.DividerHeight = 1;
            this.dgvColocadas2.RowTemplate.Height = 20;
            this.dgvColocadas2.RowTemplate.ReadOnly = true;
            this.dgvColocadas2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvColocadas2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvColocadas2.Size = new System.Drawing.Size(161, 539);
            this.dgvColocadas2.TabIndex = 34;
            this.dgvColocadas2.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvColocadas_CellFormatting);
            this.dgvColocadas2.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvColocadas1_CellMouseClick);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Calle";
            this.dataGridViewTextBoxColumn1.HeaderText = "Calle";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 5;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.DataPropertyName = "CarrierId";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridViewTextBoxColumn2.HeaderText = "1ª";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.dataGridViewTextBoxColumn3.DataPropertyName = "suma";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridViewTextBoxColumn3.HeaderText = "Total";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 5;
            // 
            // dgvColocadas1
            // 
            this.dgvColocadas1.AllowUserToAddRows = false;
            this.dgvColocadas1.AllowUserToDeleteRows = false;
            this.dgvColocadas1.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvColocadas1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvColocadas1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dgvColocadas1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvColocadas1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this.dgvColocadas1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvColocadas1.ColumnHeadersVisible = false;
            this.dgvColocadas1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colCalle1,
            this.colPrimPercha1,
            this.colTotal1});
            this.dgvColocadas1.Cursor = System.Windows.Forms.Cursors.Hand;
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle16.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle16.NullValue = "0";
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvColocadas1.DefaultCellStyle = dataGridViewCellStyle16;
            this.dgvColocadas1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvColocadas1.EnableHeadersVisualStyles = false;
            this.dgvColocadas1.GridColor = System.Drawing.SystemColors.Control;
            this.dgvColocadas1.Location = new System.Drawing.Point(10, 2);
            this.dgvColocadas1.Margin = new System.Windows.Forms.Padding(10, 2, 10, 2);
            this.dgvColocadas1.MultiSelect = false;
            this.dgvColocadas1.Name = "dgvColocadas1";
            this.dgvColocadas1.ReadOnly = true;
            this.dgvColocadas1.RowHeadersVisible = false;
            this.dgvColocadas1.RowTemplate.DividerHeight = 1;
            this.dgvColocadas1.RowTemplate.Height = 20;
            this.dgvColocadas1.RowTemplate.ReadOnly = true;
            this.dgvColocadas1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvColocadas1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvColocadas1.Size = new System.Drawing.Size(151, 539);
            this.dgvColocadas1.TabIndex = 33;
            this.dgvColocadas1.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvColocadas_CellFormatting);
            this.dgvColocadas1.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvColocadas1_CellMouseClick);
            // 
            // colCalle1
            // 
            this.colCalle1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.colCalle1.DataPropertyName = "Calle";
            this.colCalle1.HeaderText = "Calle";
            this.colCalle1.Name = "colCalle1";
            this.colCalle1.ReadOnly = true;
            this.colCalle1.Width = 5;
            // 
            // colPrimPercha1
            // 
            this.colPrimPercha1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colPrimPercha1.DataPropertyName = "CarrierId";
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.colPrimPercha1.DefaultCellStyle = dataGridViewCellStyle14;
            this.colPrimPercha1.HeaderText = "1ª";
            this.colPrimPercha1.Name = "colPrimPercha1";
            this.colPrimPercha1.ReadOnly = true;
            // 
            // colTotal1
            // 
            this.colTotal1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.colTotal1.DataPropertyName = "suma";
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.colTotal1.DefaultCellStyle = dataGridViewCellStyle15;
            this.colTotal1.HeaderText = "Total";
            this.colTotal1.Name = "colTotal1";
            this.colTotal1.ReadOnly = true;
            this.colTotal1.Width = 5;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.SystemColors.Control;
            this.label5.Dock = System.Windows.Forms.DockStyle.Top;
            this.label5.Font = new System.Drawing.Font("Consolas", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(5, 5);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(684, 25);
            this.label5.TabIndex = 38;
            this.label5.Text = "CONTENIDO DEL ALMACÉN (Calle / 1ª percha para salir / Total perchas en calle)";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // menuStrip1
            // 
            this.menuStrip1.AutoSize = false;
            this.menuStrip1.BackColor = System.Drawing.Color.White;
            this.menuStrip1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiMC,
            this.pLCToolStripMenuItem,
            this.gestiónDePerchasDudosasToolStripMenuItem,
            this.tsmiConfRef,
            this.tsmiIdiomas,
            this.tsmiAcercaDe,
            this.lblStatePLC});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(0);
            this.menuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.menuStrip1.Size = new System.Drawing.Size(1290, 25);
            this.menuStrip1.TabIndex = 0;
            // 
            // tsmiMC
            // 
            this.tsmiMC.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cargaDescargaManualToolStripMenuItem,
            this.recircularPerchasToolStripMenuItem,
            this.motoresToolStripMenuItem,
            this.eVEntradaToolStripMenuItem});
            this.tsmiMC.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsmiMC.Name = "tsmiMC";
            this.tsmiMC.Size = new System.Drawing.Size(117, 25);
            this.tsmiMC.Text = "Control Manual";
            // 
            // cargaDescargaManualToolStripMenuItem
            // 
            this.cargaDescargaManualToolStripMenuItem.Name = "cargaDescargaManualToolStripMenuItem";
            this.cargaDescargaManualToolStripMenuItem.Size = new System.Drawing.Size(235, 22);
            this.cargaDescargaManualToolStripMenuItem.Text = "Carga / descarga manual";
            this.cargaDescargaManualToolStripMenuItem.Click += new System.EventHandler(this.cargaDescargaManualToolStripMenuItem_Click);
            // 
            // recircularPerchasToolStripMenuItem
            // 
            this.recircularPerchasToolStripMenuItem.Name = "recircularPerchasToolStripMenuItem";
            this.recircularPerchasToolStripMenuItem.Size = new System.Drawing.Size(235, 22);
            this.recircularPerchasToolStripMenuItem.Text = "Recircular perchas";
            this.recircularPerchasToolStripMenuItem.Click += new System.EventHandler(this.recircularPerchasToolStripMenuItem_Click);
            // 
            // motoresToolStripMenuItem
            // 
            this.motoresToolStripMenuItem.Name = "motoresToolStripMenuItem";
            this.motoresToolStripMenuItem.Size = new System.Drawing.Size(235, 22);
            this.motoresToolStripMenuItem.Text = "Motores";
            this.motoresToolStripMenuItem.Click += new System.EventHandler(this.motoresToolStripMenuItem_Click);
            // 
            // eVEntradaToolStripMenuItem
            // 
            this.eVEntradaToolStripMenuItem.Name = "eVEntradaToolStripMenuItem";
            this.eVEntradaToolStripMenuItem.Size = new System.Drawing.Size(235, 22);
            this.eVEntradaToolStripMenuItem.Text = "Electroválvulas";
            this.eVEntradaToolStripMenuItem.Click += new System.EventHandler(this.eVEntradaToolStripMenuItem_Click);
            // 
            // pLCToolStripMenuItem
            // 
            this.pLCToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiSeeDBcarrierstate,
            this.tsmiDBOrdenesCarga,
            this.dBPerchasEnPuestoDeCarga1ToolStripMenuItem,
            this.dBPerchasCargadasEnPuesto002ToolStripMenuItem,
            this.comunicaciónToolStripMenuItem});
            this.pLCToolStripMenuItem.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pLCToolStripMenuItem.Name = "pLCToolStripMenuItem";
            this.pLCToolStripMenuItem.Size = new System.Drawing.Size(82, 25);
            this.pLCToolStripMenuItem.Text = "Datos PLC";
            // 
            // tsmiSeeDBcarrierstate
            // 
            this.tsmiSeeDBcarrierstate.Name = "tsmiSeeDBcarrierstate";
            this.tsmiSeeDBcarrierstate.Size = new System.Drawing.Size(354, 22);
            this.tsmiSeeDBcarrierstate.Text = "DB 10. Perchas en Fence entrada a calles";
            this.tsmiSeeDBcarrierstate.Click += new System.EventHandler(this.tsmiSeeDBcarrierstate_Click);
            // 
            // tsmiDBOrdenesCarga
            // 
            this.tsmiDBOrdenesCarga.Name = "tsmiDBOrdenesCarga";
            this.tsmiDBOrdenesCarga.Size = new System.Drawing.Size(354, 22);
            this.tsmiDBOrdenesCarga.Text = "DB 11. Perchas en Pin salida de calles";
            this.tsmiDBOrdenesCarga.Click += new System.EventHandler(this.verDBToolStripMenuItem_Click);
            // 
            // dBPerchasEnPuestoDeCarga1ToolStripMenuItem
            // 
            this.dBPerchasEnPuestoDeCarga1ToolStripMenuItem.Name = "dBPerchasEnPuestoDeCarga1ToolStripMenuItem";
            this.dBPerchasEnPuestoDeCarga1ToolStripMenuItem.Size = new System.Drawing.Size(354, 22);
            this.dBPerchasEnPuestoDeCarga1ToolStripMenuItem.Text = "DB 71. Perchas cargadas en puesto 001";
            this.dBPerchasEnPuestoDeCarga1ToolStripMenuItem.Click += new System.EventHandler(this.dBPerchasEnPuestoDeCarga1ToolStripMenuItem_Click);
            // 
            // dBPerchasCargadasEnPuesto002ToolStripMenuItem
            // 
            this.dBPerchasCargadasEnPuesto002ToolStripMenuItem.Name = "dBPerchasCargadasEnPuesto002ToolStripMenuItem";
            this.dBPerchasCargadasEnPuesto002ToolStripMenuItem.Size = new System.Drawing.Size(354, 22);
            this.dBPerchasCargadasEnPuesto002ToolStripMenuItem.Text = "DB 72. Perchas cargadas en puesto 002";
            this.dBPerchasCargadasEnPuesto002ToolStripMenuItem.Click += new System.EventHandler(this.dBPerchasCargadasEnPuesto002ToolStripMenuItem_Click);
            // 
            // comunicaciónToolStripMenuItem
            // 
            this.comunicaciónToolStripMenuItem.Name = "comunicaciónToolStripMenuItem";
            this.comunicaciónToolStripMenuItem.Size = new System.Drawing.Size(354, 22);
            this.comunicaciónToolStripMenuItem.Text = "Estado detectores salida calles";
            this.comunicaciónToolStripMenuItem.Click += new System.EventHandler(this.comandosPLCYJITToolStripMenuItem_Click);
            // 
            // gestiónDePerchasDudosasToolStripMenuItem
            // 
            this.gestiónDePerchasDudosasToolStripMenuItem.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gestiónDePerchasDudosasToolStripMenuItem.Name = "gestiónDePerchasDudosasToolStripMenuItem";
            this.gestiónDePerchasDudosasToolStripMenuItem.Size = new System.Drawing.Size(201, 25);
            this.gestiónDePerchasDudosasToolStripMenuItem.Text = "Gestión de perchas dudosas";
            this.gestiónDePerchasDudosasToolStripMenuItem.Click += new System.EventHandler(this.gestiónDePerchasDudosasToolStripMenuItem_Click);
            // 
            // tsmiConfRef
            // 
            this.tsmiConfRef.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsmiConfRef.Name = "tsmiConfRef";
            this.tsmiConfRef.Size = new System.Drawing.Size(152, 25);
            this.tsmiConfRef.Text = "Gestión referencias";
            this.tsmiConfRef.Click += new System.EventHandler(this.configuraciónReferenciasToolStripMenuItem_Click);
            this.tsmiConfRef.Paint += new System.Windows.Forms.PaintEventHandler(this.ControlPaint);
            // 
            // tsmiIdiomas
            // 
            this.tsmiIdiomas.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiEspañol,
            this.tsmiPortugues});
            this.tsmiIdiomas.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsmiIdiomas.Name = "tsmiIdiomas";
            this.tsmiIdiomas.Size = new System.Drawing.Size(68, 25);
            this.tsmiIdiomas.Text = "Idiomas";
            this.tsmiIdiomas.Visible = false;
            // 
            // tsmiEspañol
            // 
            this.tsmiEspañol.Name = "tsmiEspañol";
            this.tsmiEspañol.Size = new System.Drawing.Size(152, 22);
            this.tsmiEspañol.Text = "Español";
            this.tsmiEspañol.Click += new System.EventHandler(this.tsmiEspañol_Click);
            // 
            // tsmiPortugues
            // 
            this.tsmiPortugues.Name = "tsmiPortugues";
            this.tsmiPortugues.Size = new System.Drawing.Size(152, 22);
            this.tsmiPortugues.Text = "Portugués";
            this.tsmiPortugues.Click += new System.EventHandler(this.tsmiPortugues_Click);
            // 
            // tsmiAcercaDe
            // 
            this.tsmiAcercaDe.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsmiAcercaDe.Name = "tsmiAcercaDe";
            this.tsmiAcercaDe.Size = new System.Drawing.Size(82, 25);
            this.tsmiAcercaDe.Text = "Acerca de";
            this.tsmiAcercaDe.Click += new System.EventHandler(this.tsmiAcercaDe_Click);
            // 
            // lblStatePLC
            // 
            this.lblStatePLC.ActiveLinkColor = System.Drawing.Color.Green;
            this.lblStatePLC.BackColor = System.Drawing.Color.Gainsboro;
            this.lblStatePLC.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.lblStatePLC.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatePLC.ForeColor = System.Drawing.Color.White;
            this.lblStatePLC.LinkColor = System.Drawing.Color.Gray;
            this.lblStatePLC.Margin = new System.Windows.Forms.Padding(5);
            this.lblStatePLC.Name = "lblStatePLC";
            this.lblStatePLC.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblStatePLC.Size = new System.Drawing.Size(91, 15);
            this.lblStatePLC.Text = "CONEXIÓN PLC";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 5000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1290, 603);
            this.Controls.Add(this.sCMonitPerchas);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "c1";
            this.Shown += new System.EventHandler(this.Form1_Shown);
            this.sCMonitPerchas.Panel1.ResumeLayout(false);
            this.sCMonitPerchas.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.sCMonitPerchas)).EndInit();
            this.sCMonitPerchas.ResumeLayout(false);
            this.tlpTransit.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.tlpCol.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvColocadas4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvColocadas3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvColocadas2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvColocadas1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tsmiIdiomas;
        private System.Windows.Forms.ToolStripMenuItem tsmiPortugues;
        private System.Windows.Forms.ToolStripMenuItem tsmiEspañol;
        private System.Windows.Forms.ToolStripMenuItem tsmiAcercaDe;
        private System.Windows.Forms.ToolStripMenuItem pLCToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmiDBOrdenesCarga;
        private System.Windows.Forms.ToolStripMenuItem tsmiMC;
        private System.Windows.Forms.ToolStripMenuItem tsmiConfRef;
        private System.Windows.Forms.ToolStripMenuItem tsmiSeeDBcarrierstate;
        private System.Windows.Forms.ToolStripMenuItem motoresToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eVEntradaToolStripMenuItem;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.SplitContainer sCMonitPerchas;
        private System.Windows.Forms.TableLayoutPanel tlpCol;
        private System.Windows.Forms.DataGridView dgvColocadas1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TableLayoutPanel tlpTransit;
        private System.Windows.Forms.ToolStripMenuItem recircularPerchasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem comunicaciónToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel lblStatePLC;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCalle1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPrimPercha1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTotal1;
        private System.Windows.Forms.DataGridView dgvColocadas4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridView dgvColocadas3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridView dgvColocadas2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.ToolStripMenuItem cargaDescargaManualToolStripMenuItem;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.ToolStripMenuItem gestiónDePerchasDudosasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dBPerchasEnPuestoDeCarga1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dBPerchasCargadasEnPuesto002ToolStripMenuItem;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ListBox lbBajando1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.ListBox lbBajando3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ListBox lbBajando2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label lblError;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtNuevaRef;
        private System.Windows.Forms.TextBox txtCarrierId;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label lblbuffer3;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label lblbuffer2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lblbuffer1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label lblmaxbuffer;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtCalle;
        private System.Windows.Forms.Button button6;
    }
}

