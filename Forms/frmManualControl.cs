﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AereoManager
{
    internal partial class frmManualControl : Form
    {
        MCElement.typeElement typeElement;
        int numDB = 23;
       // int num1byte = 10;
        private Color color1 = Color.FromArgb(224, 224, 224);
        private Color color2 = Color.FromArgb(255, 255, 192);
        private Color color3 = Color.FromArgb(192, 255, 192);
        private Color color4 = Color.FromArgb(192, 192, 255);
        private Color lastcolor;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="type">Tipo de elemento a mostrar</param>
        internal frmManualControl(MCElement.typeElement type)
        {
            InitializeComponent();
            ManualControl.Actualizar();
          
            typeElement = type;

            if (type == MCElement.typeElement.EV)
            {
                this.Text = "Electroválvulas de entrada y salida de cada calle";
                drawEV();
            }
            else
            {
                DrawElements();
            }
        }

        private void DrawElements()
        {
            foreach (MCElement m in ManualControl.obj.ELEMENTO)
            {
                if (m.type == typeElement)
                {
                    if (m.type == MCElement.typeElement.motor)
                    {
                        this.Text = "Motores";
                        drawMotor(m);
                    }
                }
            }
        }

        private void drawEV()
        {
            int numbit = 0;
            int numbyte = 10;
            for (int i = 1001; i < 1033; i ++)
            {
                drawEVControls(i, numbyte, numbit, Color.FromArgb(224, 224, 224));
                numbit= numbit + 2;
                if (numbit > 7)
                {
                    numbyte++;
                    numbit = 0;
                }
            }

            
            numbit = 0;
            for (int i = 2001; i < 2021; i ++)
            {
                drawEVControls(i, numbyte, numbit, Color.FromArgb(255, 255, 192));
                numbit = numbit + 2;
                if (numbit > 7)
                {
                    numbyte++;
                    numbit = 0;
                }
            }
        
            numbit = 0;
            for (int i = 3001; i < 3029; i++)
            {
                drawEVControls(i, numbyte, numbit, Color.FromArgb(192, 255, 192));
                numbit = numbit + 2;
                if (numbit > 7)
                {
                    numbyte++;
                    numbit = 0;
                }
            }
          
            numbit = 0;
            for (int i = 4001; i < 4016; i++)
            {
                drawEVControls(i, numbyte, numbit, Color.FromArgb(192, 192, 255));
                numbit = numbit + 2;
                if (numbit > 7)
                {
                    numbyte++;
                    numbit = 0;
                }
            }
        }

        private void drawEVControls(int i,  int numbyte, int numbit, Color color)
        {
            Panel pn = new Panel();
            pn.BackColor = color; 
            pn.Size = new Size(182, 25);


            Label l = new Label();
            l.Text = i.ToString() + " " + numbyte + "." + numbit;
            l.ForeColor = Color.Black;
            l.Location = new Point(12, 15);
            l.AutoSize = false;
            
            l.TextAlign = ContentAlignment.MiddleLeft;
            l.Size = new Size(100, 25);
            l.Parent = pn;

            CheckBox chE = new CheckBox();
            chE.Location = new Point(64, 15);
            chE.Text = "E";
            chE.Checked = false;
            chE.CheckedChanged += Ch_CheckedChanged;
            MCElement mE = new MCElement();
            mE.db = numDB;
            mE.bytenum = numbyte;
            mE.bit1 = numbit;
            ManualControl.obj.ELEMENTO.Add(mE);
            chE.Tag = mE;
            chE.Parent = pn;
            chE.AutoSize = true;
            numbit++;

            CheckBox chS = new CheckBox();
            chS.Location = new Point(100, 15);
            chS.Text = "S";
            chS.Checked = false;
            chS.AutoSize = true;
            chS.CheckedChanged += Ch_CheckedChanged;
            MCElement mS = new MCElement();
            mS.db = numDB;
            mS.bytenum = numbyte;
            mS.bit1 = numbit;
            ManualControl.obj.ELEMENTO.Add(mS);
            chS.Tag = mS;
            chS.Parent = pn;

            l.Dock = DockStyle.Fill;
            chE.Dock = DockStyle.Right;
            chS.Dock = DockStyle.Right;

            flpEngines.Controls.Add(pn);
        }


        private void Ch_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox ch = (CheckBox)sender;
            Panel gb = (Panel)ch.Parent;
            gb.BackColor = ch.Checked ? Color.Red : Color.Gainsboro;
            MCElement m = (MCElement)ch.Tag;
            m.nextAction = ch.Checked ? MCElement.Action.on : MCElement.Action.off;
            PLCDaemon.qControlMan.Enqueue(m);
        }
        
        private void drawMotor(MCElement m)
        {
            Panel pn = new Panel();
            pn.Size = new Size(330, 22);

            pn.BackColor = (lastcolor == color1) ? color2 : color1;
            lastcolor = pn.BackColor;
            pn.Tag = m;

            Label l = new Label();
            l.Text = m.name;
            l.Parent = pn;
            l.Dock = DockStyle.Left;
         
            l.TextAlign = ContentAlignment.MiddleLeft;
            l.AutoSize = true;

            if (!m.name.Contains("200") && !m.name.Contains("600") && 
                !m.name.Contains("601") && !m.name.Contains("Rearme"))
            {
                RadioButton rbOff = new RadioButton();
                RadioButton rbLento = new RadioButton();
                RadioButton rbRapido = new RadioButton();

                rbLento.Parent = pn;
                rbLento.Location = new Point(5, 50);
                rbLento.Text = "Lento";
                rbLento.AutoSize = true;
                rbLento.CheckedChanged += Rb_CheckedChanged;
                rbLento.Tag = MCElement.Action.slow;

                rbOff.Parent = pn;
                rbOff.Location = new Point(55, 25);
                rbOff.Text = "Off";
                rbOff.AutoSize = true;
                rbOff.Tag = MCElement.Action.off;
                rbOff.Checked = true;
                rbOff.CheckedChanged += Rb_CheckedChanged;

                rbRapido.Parent = pn;
                rbRapido.Location = new Point(75, 50);
                rbRapido.Text = "Rap.";
                rbRapido.AutoSize = true;
                rbRapido.CheckedChanged += Rb_CheckedChanged;
                rbRapido.Tag = MCElement.Action.fast;

                rbOff.Dock = DockStyle.Right;
                rbRapido.Dock = DockStyle.Right;
                rbLento.Dock = DockStyle.Right;
            }
            else
            {
                RadioButton rbOff = new RadioButton();
                RadioButton rbon = new RadioButton();

                rbon.Parent = pn;
                rbon.Location = new Point(15, 30);
                rbon.Text = "On";
                rbon.AutoSize = true;
                rbon.CheckedChanged += Rb_CheckedChanged;
                rbon.Tag = MCElement.Action.on;

                rbOff.Parent = pn;
                rbOff.Location = new Point(85, 30);
                rbOff.Text = "Off";
                rbOff.AutoSize = true;
                rbOff.Tag = MCElement.Action.off;
                rbOff.Checked = true;
                rbOff.CheckedChanged += Rb_CheckedChanged;
                rbOff.Dock = DockStyle.Right;
                rbon.Dock = DockStyle.Right;
            }

            l.Dock = DockStyle.Fill;
        
            flpEngines.Controls.Add(pn);
        }

        private void Rb_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rb = sender as RadioButton;
            if (rb != null)
            {
                MCElement m = ((Panel)rb.Parent).Tag as MCElement;
                if (m != null)
                {
                    m.nextAction = (MCElement.Action)rb.Tag;
                    PLCDaemon.qControlMan.Enqueue(m);
                    if (m.nextAction != MCElement.Action.off)
                    {
                        ((Panel)rb.Parent).BackColor = Color.Red;
                    }
                    else
                        ((Panel)rb.Parent).BackColor = Color.Gainsboro;
                }
            }
        }
    }
}
