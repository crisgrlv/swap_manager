﻿namespace AereoManager
{
    partial class frmErrors
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbError = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // lbError
            // 
            this.lbError.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lbError.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbError.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbError.ForeColor = System.Drawing.Color.Red;
            this.lbError.FormattingEnabled = true;
            this.lbError.Location = new System.Drawing.Point(0, 0);
            this.lbError.Name = "lbError";
            this.lbError.Size = new System.Drawing.Size(712, 310);
            this.lbError.Sorted = true;
            this.lbError.TabIndex = 32;
            // 
            // frmErrors
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(712, 310);
            this.Controls.Add(this.lbError);
            this.Name = "frmErrors";
            this.Text = "ERROR LOG";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox lbError;
    }
}