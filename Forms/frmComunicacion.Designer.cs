﻿namespace AereoManager
{
    partial class frmComunicacion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblMaxBuffer = new System.Windows.Forms.Label();
            this.COverflow = new System.Windows.Forms.Label();
            this.CNoRNoD = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 5000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(20, 82);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1127, 527);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.CNoRNoD);
            this.panel1.Controls.Add(this.COverflow);
            this.panel1.Controls.Add(this.lblMaxBuffer);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(20, 20);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1127, 62);
            this.panel1.TabIndex = 1;
            // 
            // lblMaxBuffer
            // 
            this.lblMaxBuffer.AutoSize = true;
            this.lblMaxBuffer.Location = new System.Drawing.Point(20, 19);
            this.lblMaxBuffer.Name = "lblMaxBuffer";
            this.lblMaxBuffer.Size = new System.Drawing.Size(43, 17);
            this.lblMaxBuffer.TabIndex = 0;
            this.lblMaxBuffer.Text = "label1";
            // 
            // COverflow
            // 
            this.COverflow.AutoSize = true;
            this.COverflow.Location = new System.Drawing.Point(316, 19);
            this.COverflow.Name = "COverflow";
            this.COverflow.Size = new System.Drawing.Size(43, 17);
            this.COverflow.TabIndex = 1;
            this.COverflow.Text = "label1";
            // 
            // CNoRNoD
            // 
            this.CNoRNoD.AutoSize = true;
            this.CNoRNoD.Location = new System.Drawing.Point(646, 19);
            this.CNoRNoD.Name = "CNoRNoD";
            this.CNoRNoD.Size = new System.Drawing.Size(43, 17);
            this.CNoRNoD.TabIndex = 2;
            this.CNoRNoD.Text = "label1";
            // 
            // frmComunicacion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1167, 629);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmComunicacion";
            this.Padding = new System.Windows.Forms.Padding(20);
            this.Text = "Detectores salida calles";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label CNoRNoD;
        private System.Windows.Forms.Label COverflow;
        private System.Windows.Forms.Label lblMaxBuffer;
    }
}