﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AereoManager
{
    public partial class frmComunicacion : Form
    {
        public frmComunicacion()
        {
            InitializeComponent();
            actualizar();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            actualizar();
        }

        private void actualizar()
        {
            flowLayoutPanel1.Controls.Clear();
            Dictionary<int, bool> cp = PLCDaemon.dictStockReal;
            foreach (short calle in cp.Keys)
            {
                Label l = new Label();
                string texto = PLCDaemon.dictStockReal[calle] ? "OK" : "NO HAY PERCHA";
                l.Text = calle.ToString() + " : " + texto;
                flowLayoutPanel1.Controls.Add(l);
            }

            lblMaxBuffer.Text = "Max buffer: " + PLCDaemon.maxbufferbajada.ToString();
            COverflow.Text = "Calle overflow: " + PLCDaemon.calle_overflow.ToString();
            CNoRNoD.Text = "Calle no data: " + PLCDaemon.calle_noreadnodata.ToString();
        }
    }
}
