﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AereoManager
{
    public partial class frmErrors : Form
    {
        public frmErrors()
        {
            InitializeComponent();
            Work.evNotification += Work_evNotification;
        }

        private void Work_evNotification(messagetoLog m)
        {
            try
            {
                if (this.InvokeRequired)
                    this.Invoke(new Work.NotificationEvent(Work_evNotification), m);
                else
                {
                    if (m.type == messagetoLog.messageType.error)
                        lbError.Items.Add(m.ts + " - " + m.classname + ": " + m.message);
                }
            }
            catch { }
        }
    }
}
