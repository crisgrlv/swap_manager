﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AereoManager
{
    public partial class frmVerDB : Form
    {
        int numDB;
        CultureInfo cul;
        ResourceManager res_man;
        BindingSource bs;

        public frmVerDB(int numDB, string titulo, CultureInfo cul, ResourceManager res_man)
        {
            InitializeComponent();
            dgvDB.AutoGenerateColumns = false;
            bs = new BindingSource();
            dgvDB.DataSource = bs;
            this.cul = cul;
            this.res_man = res_man;
            this.numDB = numDB;
            PLCDaemon.numDBToAdd = numDB;
            PLCDaemon.addDB = true;
            this.Text = "DB" + numDB + ". " + titulo;
            colTimestamp.Visible = false;
            if (numDB >70)
            {
                colPosicion.Visible = false;
            }
            actualizar();
        }

        private void PintaTexttos()
        {
            foreach (Control c in this.Controls)
            {
                c.Text = res_man.GetString(c.Name, cul);
            }
        }

        private void frmVerDB_FormClosing(object sender, FormClosingEventArgs e)
        {
            PLCDaemon.numDBToRemove = numDB;
            PLCDaemon.removeDB = true;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {           
            int id = -1;
            try
            {
                if (dgvDB.SelectedRows.Count == 1)
                {
                    id = Convert.ToInt32(((DataRowView)bs.Current)["secuenciaPC"].ToString());

                }
            }
            catch { }

            actualizar();

            try {
                if (id != -1)
                {
                    for (int i = 0; i < dgvDB.Rows.Count; i++)
                    {
                        if (id == Convert.ToInt32(dgvDB.Rows[i].Cells[0].Value.ToString()))
                        {
                            dgvDB.Rows[i].Selected = true;
                            dgvDB.CurrentCell = dgvDB.Rows[i].Cells[1];
                            break;
                        }
                    }
                }
            }
            catch { }
        }

        private void actualizar()
        {
            if (PLCPercha.listPLCDBOrders != null && PLCPercha.listPLCDBOrders.Count > 0)
                bs.DataSource = PLCPercha.listPLCDBOrders.FindAll(p => p.db == numDB);
            else
                bs.DataSource = null;

            label1.Text = PLCPercha.listPLCDBOrders.Count.ToString();
        }

        private void dgvDB_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            try
            {
                if (e.RowIndex > -1)
                {
                    PLCPercha ord = dgvDB.Rows[e.RowIndex].DataBoundItem as PLCPercha;
                    if (e.ColumnIndex == ColSec.Index)
                        e.Value = ord.secuenciaPC;
                    else if (e.ColumnIndex == colOrigen.Index)
                        e.Value = ord.origen;
                    else if (e.ColumnIndex == colDestino.Index)
                        e.Value = ord.destino;
                    else if (e.ColumnIndex == colcodPercha.Index)
                        e.Value = ord.codigoPercha;
                    else if (e.ColumnIndex == colTransportador.Index)
                    {
                        switch(ord.transportador)
                        {
                            case 1: e.Value = "Pin 1"; break;
                            case 2: e.Value = "Pin 2"; break;
                            case 3: e.Value = "Subida de carga"; break;
                            case 4: e.Value = "De pin 1 a 2"; break;
                            case 5: e.Value = "De pin 2 a 1"; break;
                            case 6: e.Value = "Fence a calles"; break;
                            default: e.Value = ord.transportador;break;
                        }
                        
                    }
                    else if (e.ColumnIndex == colPosicion.Index)
                        e.Value = ord.posicion;
                    else if (e.ColumnIndex == colEstadoPercha.Index)
                    {
                        switch (ord.estadoPercha)
                        {
                            case 0: e.Value = "Circulando no asig."; break;
                            case 1: e.Value = "En desvío a 1000"; break;
                            case 2: e.Value = "En desvío a 2000"; break;
                            case 3: e.Value = "En desvío a 3000"; break;
                            case 4: e.Value = "En desvío a 4000"; break;
                            case 5: e.Value = "En desvío a retoques"; break;
                            case 6: e.Value = "En desvío a QC"; break;
                            case 7: e.Value = "En desvío a reent. 200.2 a 600"; break;
                            case 8: e.Value = "En trasv. de 200.1 a 200.2"; break;
                            case 9: e.Value = "En trasv. de 200.2 a 200.1"; break;
                            case 10: e.Value = "Circulando asignada"; break;
                            case 11: e.Value = "Recirculando no asign."; break;
                            case 12: e.Value = "Recirculando asign. "; break;
                            case 21: e.Value = "Cargando"; break;
                            case 22: e.Value = "En almacén para consumo"; break;
                            case 23: e.Value = "En almacén para recircular"; break;
                            case 25: e.Value = "Preparada para carga"; break;
                            case 30: e.Value = "Saliendo"; break;
                            default: e.Value = ord.estadoPercha; break;
                        }

                    }
                    else if (e.ColumnIndex == colTimestamp.Index)
                        e.Value = ord.timeStampOrden;
                }
            }
            catch { }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Se van a eliminar todas las perchas en el autómata, ¿desea continuar?", 
                "Eliminar perchas", MessageBoxButtons.OKCancel);
            if (dr == DialogResult.OK)
            {
                MCElement m = new MCElement();
                m.nextAction = MCElement.Action.delete;
                m.db = this.numDB;
                m.type = MCElement.typeElement.db;
                PLCDaemon.qControlMan.Enqueue(m);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            short tag = 0;
            short destino = 0;
            if (short.TryParse(txtTag.Text, out tag))
            {
                if (short.TryParse(txtTag.Text, out destino))
                {
                    comPLC c = new comPLC();
                    c.tipoComando = comPLC.enumTipoComPLC.PCNuevoDestino;
                    c.enviado = false;
                    c.procesado = false;
                    c.orden = "0";
                    c.lote = "0";
                    c.referencia = "0";
                    c.seq = 0;
                    c.PLCEnvia = false;
                    c.idPeticionJIT = 0;
                    c.idPeticionPLC = 0;
                    c.CarrierId = tag;
                    c.param[0] = tag;
                    c.param[1] = destino;
                    c.param[2] = 0;
                    comPLC.Save(c);
                }
            }
        }
    }
}
