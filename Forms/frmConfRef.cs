﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AereoManager
{
    public partial class frmConfRef : Form
    {
        BindingSource bs;
        DataTable dt;
        DataView dv;

        public frmConfRef()
        {
            InitializeComponent();
            dgv.AutoGenerateColumns = false;
            bs = new BindingSource();

            dgv.DataSource = bs;
            dt = BD.ExecuteReader("SELECT Referencia, Calle FROM dbo.ConfRef WHERE Calle > 1000 ORDER BY Calle");
            dv = new DataView(dt);
            bs.DataSource = dv;
        }

        private void dgv_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {

            try
            {
                if (e.RowIndex > -1 && dgv[colRef.Index, e.RowIndex].Value != null)
                {
                    BD.ExecuteNonQuery("update dbo.confref set referencia =@p0 where calle =@p1",
                        dgv[colRef.Index, e.RowIndex].Value, dgv[colCalle.Index, e.RowIndex].Value);
                }
            }
            catch (Exception ex)
            {
                Work.qLog.Enqueue(new messagetoLog(ex.Message, "frmConfRef", "dgv_CellEndEdit", messagetoLog.messageType.error));
            }
        }

        private void dgv_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            try
            {
                if (e.RowIndex>-1)
                {
                    if (e.ColumnIndex == colRef.Index)
                    {
                        e.Value = e.Value.ToString().Trim();
                    }
                }
            }
            catch { }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string query = "select * from dbo.DG_InvAereo where referencia =@p0";
            DataTable dt = BD.ExecuteReader(query, textBox2.Text.Trim());
            new frmLaneData(textBox2.Text.Trim(), false).Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            dv.RowFilter = "";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
            dv.RowFilter = "Referencia = '" + textBox2.Text.Trim() + "'";
        }
    }
}

