﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;
using System.Resources;
using System.Configuration;

namespace AereoManager
{
    /// <summary>
    /// Formulario principal de la aplicación
    /// </summary>
    public partial class Form1 : Form
    {
        ResourceManager res_man;    
        CultureInfo cul;
        bool translate;

        /// <summary>
        /// Constructor sin parámetros
        /// </summary>
        public Form1() 
        {
            translate = true;
            InitializeComponent();

            dgvColocadas1.AutoGenerateColumns = false;
            dgvColocadas2.AutoGenerateColumns = false;
            dgvColocadas3.AutoGenerateColumns = false;
            dgvColocadas4.AutoGenerateColumns = false;
           
            inicioIdiomas();

            Work.evNotification += new Work.NotificationEvent(Work_evNotification);

            JITDaemon.Start();
            PLCDaemon.Start();

          //  PLCDaemon.calle_noreadnodata = (short)Conf.getInt("calle_noreadnodata");
          //  PLCDaemon.calle_overflow = (short)Conf.getInt("calle_overflow");

            Work.Start();
        }

        private void Work_evNotification(messagetoLog m)
        {
            try
            {
                if (this.InvokeRequired)
                    this.Invoke(new Work.NotificationEvent(Work_evNotification), m);
                else
                {
                    if (m.type != messagetoLog.messageType.error && m.classname == "PLCDaemon")
                    {
                        if (m.type == messagetoLog.messageType.state_on)
                        {
                            lblStatePLC.Text = "PLC CONECTADO ";
                            lblStatePLC.BackColor = Color.LimeGreen;
                        }
                        else if (m.type == messagetoLog.messageType.state_off)
                        {
                            lblStatePLC.Text = "PLC DESCONECTADO ";
                            lblStatePLC.BackColor = Color.Red;
                        }
                    }
                }
            }
            catch { }
        }

        #region Idiomas
        private void inicioIdiomas()
        {
            string idioma = ConfigurationManager.AppSettings.Get("language");

            if (idioma != "es" && idioma != "pt")
                idioma = "es";

            cul = CultureInfo.CreateSpecificCulture(idioma);
            res_man = new ResourceManager("AereoManager.resources.Lang", typeof(Form1).Assembly);
            res_man.IgnoreCase = true;
        }

        private void tsmiEspañol_Click(object sender, EventArgs e)
        {
            cul = CultureInfo.CreateSpecificCulture("es");
            translate = true;
            this.Refresh();
        }

        private void tsmiPortugues_Click(object sender, EventArgs e)
        {
            cul = CultureInfo.CreateSpecificCulture("pt");
            translate = true;
            this.Text = res_man.GetString(this.Tag.ToString(), cul);
            this.Refresh();
        }

        #endregion Idiomas

        private void verDBToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmVerDB frm = new frmVerDB(11, "PERCHAS EN PIN SALIDA DE CALLES", cul, res_man);
            frm.Show();
        }

        private void configuraciónReferenciasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmConfRef().Show();
        }

        private void tsmiAcercaDe_Click(object sender, EventArgs e)
        {
            new frmAcercaDe().Show();
        }

        private void motoresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmManualControl(MCElement.typeElement.motor).Show();
        }

        private void desviostsmi_Click(object sender, EventArgs e)
        {
            new frmManualControl(MCElement.typeElement.desvio).Show();
        }

        private void eVEntradaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmManualControl(MCElement.typeElement.EV).Show();
        }

        private void dosificadoresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmManualControl(MCElement.typeElement.dosificador).Show();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                string queryColocadas = "select distinct c3.Calle as Calle, IsNull(c4.CarrierId, ' ') as CarrierId, " + 
                    " IsNull(c4.suma, 0) as suma from dbo.ConfRef c3 left join " +
                " ( " +
                   " SELECT c1.*, suma FROM dbo.DG_InvAereo c1 INNER JOIN " +
                   " ( " +
                       " SELECT count(CarrierId) as suma, Calle, MIN(ts) MinDate " +
                       " FROM dbo.DG_InvAereo " +
                       " WHERE  Estado = @p0 " +
                       " GROUP BY Calle " +
                    " ) c2 ON c1.Calle = c2.Calle AND c1.TS = c2.MinDate " +
                " ) c4 ON c4.Calle = c3.Calle " +
                " order by c3.Calle ";

                DataTable dtColocadas = BD.ExecuteReader(queryColocadas, Carrier.getSt(Carrier.enumEstadosPercha.Colocada));
                DataView dv = new DataView(dtColocadas);
                dv.RowFilter = "Calle < 2000";
                dgvColocadas1.DataSource = dv;
                DataView dv2 = new DataView(dtColocadas);
                dv2.RowFilter = "Calle > 2000 and Calle < 3000";
                dgvColocadas2.DataSource = dv2;
                DataView dv3 = new DataView(dtColocadas);
                dv3.RowFilter = "Calle > 3000 and Calle < 4000";
                dgvColocadas3.DataSource = dv3;
                DataView dv4 = new DataView(dtColocadas);
                dv4.RowFilter = "Calle > 4000";
                dgvColocadas4.DataSource = dv4;

                lbBajando1.Items.Clear();
                lbBajando1.Items.AddRange(PLCDaemon.bajando1.ToArray());

                lbBajando2.Items.Clear();
                lbBajando2.Items.AddRange(PLCDaemon.bajando2.ToArray());

                lbBajando3.Items.Clear();
                lbBajando3.Items.AddRange(PLCDaemon.bajando3.ToArray());

           /*     lbCarga1.Items.Clear();
                lbCarga1.Items.AddRange(PLCDaemon.cargando1.ToArray());

                lbCarga2.Items.Clear();
                lbCarga2.Items.AddRange(PLCDaemon.cargando2.ToArray());*/

                lblbuffer1.Text = PLCDaemon.numbajando1.ToString();
                lblbuffer2.Text = PLCDaemon.numbajando2.ToString();
                lblbuffer3.Text = PLCDaemon.numbajando3.ToString();

                lblmaxbuffer.Text = PLCDaemon.maxbufferbajada.ToString();
            }
            catch { }
        }

        private void dgvColocadas_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            try {
                DataGridView dgv = (DataGridView)sender;
                if (e.RowIndex > -1)
                {
                    DataRowView drv = dgv.Rows[e.RowIndex].DataBoundItem as DataRowView;

                    if (drv != null)
                    {
                        if (drv["Calle"].ToString() == "800")
                        {
                            e.CellStyle.BackColor = Color.Yellow;
                            e.CellStyle.SelectionBackColor = Color.Yellow;
                            e.CellStyle.ForeColor = Color.Black;
                            e.CellStyle.SelectionForeColor = Color.Black;
                        }
                        else
                        {
                            int suma = (int)drv["suma"];
                            if (suma <= 0)
                            {
                                e.CellStyle.BackColor = Color.Black;
                                e.CellStyle.SelectionBackColor = Color.Black;
                                e.CellStyle.ForeColor = Color.White;
                                e.CellStyle.SelectionForeColor = Color.White;
                            }
                            else if (suma < 50 && suma > 39)
                            {
                                e.CellStyle.BackColor = Color.DarkOrange;
                                e.CellStyle.SelectionBackColor = Color.DarkOrange;
                            }
                            else if (suma < 40)
                            {
                                e.CellStyle.BackColor = Color.LimeGreen;
                                e.CellStyle.SelectionBackColor = Color.LimeGreen;
                            }
                            else if (suma >= 50)
                            {
                                e.CellStyle.BackColor = Color.Red;
                                e.CellStyle.SelectionBackColor = Color.Red;
                            }
                        }
                    }
                }
            }
            catch { }
        }

        private void dgv1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            try {
                DataGridView dgv = (DataGridView)sender;
                if (e.RowIndex > -1)
                {
                    DataRowView drv = (DataRowView)dgv.Rows[e.RowIndex].DataBoundItem;
                    if ((string)drv["Estado"] == Carrier.getSt(Carrier.enumEstadosPercha.Subiendo))
                    {
                        e.CellStyle.BackColor = Color.Black;
                        e.CellStyle.SelectionBackColor = Color.Black;
                        e.CellStyle.ForeColor = Color.LightGray;
                        e.CellStyle.SelectionForeColor = Color.LightGray;
                    }
                    else if ((string)drv["Estado"] == Carrier.getSt(Carrier.enumEstadosPercha.Bajando))
                    {
                        e.CellStyle.BackColor = Color.White;
                        e.CellStyle.SelectionBackColor = Color.White;
                        e.CellStyle.ForeColor = Color.Gray;
                        e.CellStyle.SelectionForeColor = Color.Gray;
                    }
                    else if ((string)drv["Estado"] == Carrier.getSt(Carrier.enumEstadosPercha.Retornando))
                    {
                        e.CellStyle.BackColor = Color.Blue;
                        e.CellStyle.SelectionBackColor = Color.Blue;
                        e.CellStyle.ForeColor = Color.AliceBlue;
                        e.CellStyle.SelectionForeColor = Color.AliceBlue;
                    }
                }
            }
            catch { }
        }

        private void dgvColocadas1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try {
                DataGridView dgv = (DataGridView)sender;
                if (e.RowIndex > -1)
                {
                    DataRowView drv = dgv.Rows[e.RowIndex].DataBoundItem as DataRowView;
                    if (drv != null && (int)drv["suma"] > 0)
                    {
                        new frmLaneData(drv["Calle"].ToString()).ShowDialog();
                    }
                }
            }
            catch { }
        }

        private void comandosPLCYJITToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmComunicacion().Show();
        }

        private void logErroresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmErrors().Show();
        }

        private void recircularPerchasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmRecirc().Show();
        }

        private void tsmiSeeDBcarrierstate_Click(object sender, EventArgs e)
        {
            new frmVerDB(10, "PERCHAS EN FENCE HACIA CALLES", cul, res_man).Show();
        }

        private void ControlPaint(object sender, PaintEventArgs e)
        {
           
            if (translate)
            {
                Control c = sender as Control;
                if (c == null)
                {
                    ToolStripMenuItem d = sender as ToolStripMenuItem;
                    if (d != null)
                    {
                        try { d.Text = res_man.GetString(d.Tag.ToString(), cul); }
                        catch { }
                    }
                }
                
            }
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            this.Text = res_man.GetString(this.Tag.ToString(), cul);
        }

        private void cargaDescargaManualToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmManualOrders().Show();
        }

        private void CarrierData(string value)
        {
            int id = 0;
            if (int.TryParse(value, out id))
            {
                new frmLaneData(id).Show();
            }
        }

        private void dgv1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            if (e.RowIndex > -1)
            {
                DataRowView drv = dgv.Rows[e.RowIndex].DataBoundItem as DataRowView;
                if (drv != null)
                    CarrierData(drv["CarrierId"].ToString());
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string query = "select * from dbo.DG_InvAereo where referencia =@p0";
            DataTable dt = BD.ExecuteReader(query, textBox1.Text.Trim());
            new frmLaneData(textBox1.Text.Trim(), false).Show();
        }

        private void gestiónDePerchasDudosasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmdudosas().Show();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("La percha " + textBox1.Text.Trim() + " va a darse de baja del sistema, ¿desea continuar ?",
                "CONFIRMACIÓN", MessageBoxButtons.OKCancel);

            if (dr == DialogResult.OK)
            {
                short c = 0;
                if (short.TryParse(txtCarrierId.Text.Trim(), out c))
                {
                    string query = "update dbo.DG_InvAereo set Estado = @p0 where carrierid = @p1";
                    BD.ExecuteNonQuery(query, Carrier.getSt(Carrier.enumEstadosPercha.Eliminada), c);
                }
            }
        }

        private void dBPerchasEnPuestoDeCarga1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmVerDB(71, "PERCHAS EN CARGA DESDE PUESTO 1", cul, res_man).Show();
        }

        private void dBPerchasCargadasEnPuesto002ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmVerDB(72, "PERCHAS EN CARGA DESDE PUESTO 2", cul, res_man).Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            string query = "update dbo.dg_invaereo set estado = '000' where estado = '020'";
            BD.ExecuteNonQuery(query);
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            CarrierData(txtCarrierId.Text);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string query = "update dbo.dg_invaereo set estado = '045' where estado = '020'";
            BD.ExecuteNonQuery(query);
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            string nuevaRef = txtNuevaRef.Text.Trim();
            short carrierid = 0;
            if (!string.IsNullOrEmpty(nuevaRef))
            {
                if (short.TryParse(txtCarrierId.Text.Trim(), out carrierid))
                {
                    string query = "update dbo.DG_InvAereo set referencia = @p0 where carrierid = @p1";
                    BD.ExecuteNonQuery(query, nuevaRef, carrierid);
                }
                else
                {
                    lblError.Text = "Error en el nº de tag";
                }
            }
            else
            {
                lblError.Text = "Error en la nueva ref.";
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            int calle = 0;
            if (int.TryParse(txtCalle.Text, out calle) && calle > 799)
            {
                string query = "update dbo.dg_invaereo set estado = '000' where estado = '050' and calle = @p0";
                BD.ExecuteNonQuery(query, calle.ToString());
            }
        }
    }
}
