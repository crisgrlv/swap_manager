﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;


namespace AereoManager
{
    /// <summary>
    /// Elementos a controlar manualmente, se configuran en el 
    /// archivo ManualControl.xml
    /// </summary>
    [Serializable]
    public class ManualControl
    {
        
        public static ManualControl obj = new ManualControl();

        [XmlElement]
        public List<MCElement> ELEMENTO { get; set; }

        public static void Save()
        {
            using (StreamWriter sr = new StreamWriter(Application.StartupPath + "\\resources\\ManualControl.xml"))
                (new XmlSerializer(typeof(ManualControl))).Serialize(sr, obj);

            Actualizar();
        }

        public static void Actualizar()
        {
            using (StreamReader sr = new StreamReader(Application.StartupPath + "\\resources\\ManualControl.xml"))
                ManualControl.obj = (ManualControl)(new XmlSerializer(typeof(ManualControl))).Deserialize(sr);
        }
    }

    [Serializable]
    public class MCElement
    {
        [XmlAttribute]
        public int bytenum;
        [XmlAttribute]
        public int bit1;
        [XmlAttribute]
        public int bit2;
        [XmlAttribute]
        public int db;
        [XmlAttribute]
        public string name;
        [XmlAttribute]
        public typeElement type;

        public Action nextAction;


        public MCElement()
        {
            nextAction = Action.none;
        }

        public enum Action
        {
            none,
            slow,
            fast,
            on,
            off,
            delete
        }

        public enum typeElement
        {
            motor,
            EV,
            dosificador,
            desvio,
            db
        }

        public override string ToString()
        {
            return name;
        }
    }
}
