﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.IO;
using System.Configuration;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Threading;

namespace AereoManager
{
    /// <summary>
    /// Clase de comunicación por sockets con JIT
    /// </summary>
    public static class JITDaemon
    {
        private static System.Timers.Timer tmr;
        private static bool tmrFin;
        private static DateTime timetmr;
        private static string ip;
        private static int port;
        private static int portResp;
        private static Socket sReq;
        private static Socket sResp;
        private static bool SocketState;
        private static bool JitSocketEnabled;

        /// <summary>
        /// Cola de respuestas que hay que enviar al JIT
        /// </summary>
        public static Queue<comJIT> qJITResponses;

        static JITDaemon()
        {
            ip = Conf.getStr("jit_ip");          
            port = Conf.getInt("jit_port");
            portResp = 1300;

            qJITResponses = new Queue<comJIT>();

            JitSocketEnabled = Convert.ToBoolean(Conf.getInt("jit_socket_enable"));

            tmr = new System.Timers.Timer();
            tmr.Interval = Conf.getInt("jit_interval");
            tmr.Elapsed += Tmr_Elapsed;
            tmrFin = true;
            SocketState = true;
        }
        
        /// <summary>
        /// Arranca el hilo de comunicación con JIT
        /// </summary>
        public static void Start()
        {
            tmr.Start();
        }

        private static void Tmr_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (tmrFin)
            {
                tmrFin = false;
                timetmr = DateTime.Now;


                doWork();

          /*      if (timetmr.AddSeconds((int)(tmr.Interval / 1000)) < DateTime.Now)
                {
                    Work.qLog.Enqueue(new messagetoLog("INTERVAL TOO LITTLE", 
                        "JITDaemon", "Tmr_Elapsed", messagetoLog.messageType.error));
                }*/

                tmrFin = true;
            }
            else
            {
                if (timetmr.AddSeconds(50) < DateTime.Now)
                {
                 //   Work.qLog.Enqueue(new messagetoLog("TIMER OVERFLOW",
                 //      "JITDaemon", "Tmr_Elapsed", messagetoLog.messageType.error));
                   
                    tmr.Stop();
                    tmrFin = true;
                    tmr.Start();
                }
            }
        }

        private static void doWork()
        {
           // if (sResp == null)
            //    Connect(false);

            //Si está conectado, escucha por si hay algo del JIT
            if (JitSocketEnabled && checkConnection())
            {
                Listen();
            }

            //Si hay algo que enviar al JIT, lo envía
            List<comJIT> list = qJITResponses.ToList();
            foreach (comJIT j in list)
                Send(qJITResponses.Dequeue());
        }

        public static void Connect(bool isReq)
        {
            try
            {
                IPAddress ipAd = IPAddress.Parse(ip);
                

                if (isReq)
                {
                    IPEndPoint remoteEP = new IPEndPoint(ipAd, port);
                    sReq = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                    sReq.SendTimeout = 500;
                    sReq.ReceiveTimeout = 500;
                    sReq.Connect(ip, port);
                }
                else
                {
                    IPEndPoint remoteEP = new IPEndPoint(ipAd, portResp);
                    sResp = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                    sResp.SendTimeout = 500;
                    sResp.ReceiveTimeout = 500;
                    sResp.Connect(ip, portResp);
                }
            }

            catch (Exception e)
            {
                Work.qLog.Enqueue(new messagetoLog("Error trying to connect " + ip + ":" + port + " " + e.Message,
                    "JITDaemon", "Connect", messagetoLog.messageType.error));
            }
        }

        /// <summary>
        /// Conecta el socket de comunicación con el JIT
        /// </summary>
   /*     public static void Connect()
        {
            try
            {
                IPAddress ipAd = IPAddress.Parse(ip);
                IPEndPoint remoteEP = new IPEndPoint(ipAd, port);
                sReq = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                sReq.SendTimeout = 500;
                sReq.ReceiveTimeout = 500;
                //socket.Blocking = false;
                sReq.Connect(ip, port);

                sResp = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
              //  sResp.Connect(ip, portResp);
            }
          
            catch (Exception e)
            {
                Work.qLog.Enqueue(new messagetoLog("Error trying to connect " + ip + ":" + port + " " + e.Message,
                    "JITDaemon", "Connect", messagetoLog.messageType.error));
            }
        }*/

        /// <summary>
        /// Rellena cada campo hasta tener longitud fija
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private static byte[] fillArray(byte[] input)
        {
            List<byte> lstinput = input.ToList<byte>();

            for (int i = input.Length; i < 24; i++)
                lstinput.Add(3);

            return lstinput.ToArray();
        }
        

        private static void respReqOK(int numMessageOrig)
        {
            ASCIIEncoding asen = new ASCIIEncoding();
            List<byte> lstTelegram = new List<byte>();

            try
            {
                lstTelegram.AddRange(asen.GetBytes((numMessageOrig + 1).ToString().PadLeft(4, '0')));
                lstTelegram.AddRange(asen.GetBytes("0501"));
                lstTelegram.AddRange(asen.GetBytes((numMessageOrig).ToString().PadLeft(4, '0')));
                lstTelegram.AddRange(asen.GetBytes("<EF>")); //EOF
                sReq.Send(lstTelegram.ToArray());
            }
            catch (Exception ex)
            {
                Work.qLog.Enqueue(new messagetoLog("Error al responder al JIT:  " + ex.Message,
                    "JITDaemon", "resp", messagetoLog.messageType.error));
            }
        }

        private static byte[] buildTelegram(string message, string order, string param1, string param2, string param3, string param4)
        {
            ASCIIEncoding asen = new ASCIIEncoding();
            List<byte> lstTelegram = new List<byte>();

            try
            {
                byte[] byteMessage = asen.GetBytes(message);
                byte[] byteOrder = asen.GetBytes(order);
                byte[] byteParam1 = asen.GetBytes(param1);
                byte[] byteParam2 = asen.GetBytes(param2);
                byte[] byteParam3 = asen.GetBytes(param3);
                byte[] byteParam4 = asen.GetBytes(param4);
                byte[] byteFin = asen.GetBytes("<EF>");

                //lstTelegram.AddRange(asen.GetBytes("19290200003160607065819"));

                lstTelegram.AddRange(byteMessage);

                lstTelegram.Add(3);

                lstTelegram.AddRange(fillArray(byteOrder));

                lstTelegram.AddRange(fillArray(byteParam1));

                lstTelegram.AddRange(fillArray(byteParam2));

                lstTelegram.AddRange(fillArray(byteParam3));

                if (param4 != "0")
                    lstTelegram.AddRange(fillArray(byteParam4));
                else
                    lstTelegram.AddRange(fillArray(new byte[] { }));

                lstTelegram.AddRange(byteFin); //EOF

            }
            catch (Exception ex)
            {
                Work.qLog.Enqueue(new messagetoLog("Error al construir el mensaje para JIT:  " + ex.Message, 
                    "JITDaemon", "buildTelegram", messagetoLog.messageType.error));
            }
            return lstTelegram.ToArray();
        }

        /// <summary>
        /// Envía un mensaje al JIT por el puerto 1300
        /// </summary>
        /// <param name="com">Mensaje a enviar</param>
        public static void Send(comJIT com)
        {
            try
            {
                Connect(false);
                ASCIIEncoding ASCEN = new ASCIIEncoding();
                string message = "00000200" + com.puesto + DateTime.Now.ToString("yyMMddHHmmss");

                byte[] telegram = buildTelegram(message + com.typeComm.ToString(), com.orden.ToString(), com.param[0], com.param[1], com.param[2], com.param[3]);
                sResp.Send(telegram);
                   
                char[] charTelegram = ASCEN.GetChars(telegram);
                string telegramSended = new string(charTelegram);

                Work.qLog.Enqueue(new messagetoLog("Enviado: " + telegramSended, 
                    "JITDaemon", "Send", messagetoLog.messageType.trace));
                sResp.Disconnect(false);

            }
            catch (Exception ex)
            {
                Work.qLog.Enqueue(new messagetoLog("Error enviando mensaje " + com.id + " " + ex.Message, 
                    "JITDaemon", "Send", messagetoLog.messageType.error));

                Connect(false);
            }
        }

    

    /// <summary>
    /// Pregunta si JIT tiene alguna orden por el puerto 1010
    /// </summary>
    /// <returns>Mensaje que hay que enviar al JIT para que me diga si tiene órdenes para mí</returns>
    private static bool checkConnection()
    {
        bool connected = true;
            try
            {
                if (sReq != null)
                {
                    try
                    {
                        sReq.Blocking = false;
                        ASCIIEncoding asen = new ASCIIEncoding();
                        //Estructura: 00010500000
                        //Secuencia: 0001
                        sReq.Send(asen.GetBytes(comJIT.GetMsgPoll()));
                        connected = true;
                    }
                    catch (SocketException e)
                    {
                        // 10035 == WSAEWOULDBLOCK
                        connected = e.NativeErrorCode.Equals(10035);
                    }
                }
                else
                    connected = false;

                if (connected && !SocketState)
                {
                    SocketState = true;
                    Work.qLog.Enqueue(new messagetoLog("", "JITDaemon", "checkConnection", messagetoLog.messageType.state_on));
                }


                else if (!connected && SocketState)
                {
                    SocketState = false;
                    Work.qLog.Enqueue(new messagetoLog("", "JITDaemon", "checkConnection", messagetoLog.messageType.state_off));
                }

                if (!connected)
                {
                    Connect(true);
                }

                return connected;
            }
            catch (Exception ex)
            {
                Work.qLog.Enqueue(new messagetoLog("Error checking connection " + ex.Message, 
                    "JITDaemon", "checkConnection", messagetoLog.messageType.error));
                return false;
            }
    }

        /// <summary>
        /// Recibe datos del JIT
        /// </summary>
        public static void Listen()
        {
            //Estructua de una orden del JIT:
            //41440500009MREQTRM [ETX] 161745207-3L [ETX] 4004 [ETX] 196026800 [ETX] <EF>
            //secuencia=4144 espera_conf=0500 puesto=009 tipo=MREQTRM orden=161745207-3L calle=4004 referencia=196026800
            try
            {
                //Verifica si el socket está conectado
                
                    if (sReq.Available > 0)
                    {
                        ASCIIEncoding ascen = new ASCIIEncoding();

                        byte[] r = new byte[sReq.Available];

                        int res = sReq.Receive(r);

                            string strTelegram = new string(ascen.GetChars(r));

                            Work.qLog.Enqueue(new messagetoLog("Telegram received: " + strTelegram, 
                                "JITDaemon", "Listen", messagetoLog.messageType.trace));

                            strTelegram = new String(strTelegram.TakeWhile(c => c != '<').ToArray());

                            //string[] t = strTelegram.Split(new char[] { (char)3 }, StringSplitOptions.RemoveEmptyEntries);
                              string[] t = strTelegram.Split(new char[] { '*' }, StringSplitOptions.RemoveEmptyEntries);

                            if (t.Length > 3)
                            {
                                 comJIT commR = new comJIT();
                                 commR.seq = short.Parse(t[0].Trim().Substring(0, 4));
                                 commR.puesto = t[0].Trim().Substring(8, 3);
                                 commR.typeComm = (comJIT.enumTipoComJIT)Enum.Parse(typeof(comJIT.enumTipoComJIT), t[0].Trim().Substring(11,7));
                                 if ((commR.typeComm == comJIT.enumTipoComJIT.MLD_TRM) ||
                                     commR.typeComm == comJIT.enumTipoComJIT.MREQTRM)
                                 {
                                     commR.orden = t[1].Trim();
                                     commR.param.Add(t[2].Trim());
                                     commR.param.Add(t[3].Trim());

                                    if (t.Length > 4 && !string.IsNullOrEmpty(t[4].Trim()))
                                    {
                                        commR.param[2] = t[4].Trim();
                                    }
                                    if (t.Length > 5 && !string.IsNullOrEmpty(t[5].Trim()))
                                    {
                                        commR.param[3] = t[5].Trim();
                                    }

                                     Work.qJITReq.Enqueue(commR);

                                     //Respondo al JIT que ya me enteré
                                     respReqOK(commR.seq);
                                 }
                            }
                        }
                        
                  //  }
            }
            catch (Exception ex)
            {
                Work.qLog.Enqueue(new messagetoLog("Error listening " + ex.Message,
                    "JITDaemon", "Listen", messagetoLog.messageType.error));
            }
        }
    }
}