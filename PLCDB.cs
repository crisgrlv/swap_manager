﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AereoManager
{
    public class PLCPercha
    {
        public int db;
        public static List<PLCPercha> listPLCDBOrders = new List<PLCPercha>();
        public short secuenciaPC;
        public short origen;
        public short destino;
        public short codigoPercha;
        public short transportador;
        public short posicion;
        public short estadoPercha;
        public Int32 timeStampOrden;
        
        public PLCPercha()
        {
            
        }

        
        public PLCPercha(int db, short sec, short or, short dest, short codPercha, short transp, short pos, short estPercha, Int32 ts)
        {
            this.db = db;
            secuenciaPC = sec;
            origen = or;
            dest = destino;
            codigoPercha = codPercha;
            transportador = transp;
            posicion = pos;
            estadoPercha = estPercha;
            timeStampOrden = ts;
            
        }


    }
}
