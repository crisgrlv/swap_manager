﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AereoManager
{
    public static class PLCDaemon
    {
        private static System.Timers.Timer tmr;
        private static bool tmrFin;
        private static DateTime timetmr;
        private static S7Client Client;
        /// <summary>
        /// Numero de DB de lectura (recepcion de respuestas del PLC)
        /// </summary>
        public static int DBRead;
        /// <summary>
        /// Numero de DB de escritura (envio de ordenes al PLC)
        /// </summary>
        private static int NumDBWrite;
        /// <summary>
        /// IP del PLC
        /// </summary>
        private static string ip;
        /// <summary>
        /// Rack del PLC
        /// </summary>
        private static int rack;
        /// <summary>
        /// Slot del PLC
        /// </summary>
        private static int slot;
        /// <summary>
        /// Pila de ordenes para enviar al PLC
        /// </summary>
        public static Queue<comPLC> qCommToWrite;
        public static Queue<comPLC> qCommL1;
        public static Queue<comPLC> qCommL2;
        public static Queue<comPLC> qCommL3;
        public static Queue<comPLC> qCommL4;
        public static Queue<comPLC> qCommL5;
        internal static Queue<MCElement> qControlMan;
        private static int dbVivo, byteVivo, bitVivo;
        private static bool boolVivo;
        private static List<int> dbPerchas;
        public static bool addDB;
        public static int numDBToAdd;
        internal static bool removeDB;
        internal static int numDBToRemove;
        static int contDescarga1, contRestoComandos, contDescarga2, contDescarga3;
        static int contDescarga4, contDescarga5;
        public static Int32 StockTeorico1;
        public static Int32 StockTeorico2;
        public static Int32 StockTeorico3;
        public static Int32 StockTeorico4;
        public static Dictionary<int, bool> dictStockReal;
        public static List<string> bajando1;
        public static List<string> bajando2;
        public static List<string> bajando3;
        public static List<string> bajando4;
        public static List<string> bajando5;
        public static List<string> cargando1;
        public static List<string> cargando2;
        public static int numbajando1;
        public static int numbajando2;
        public static int numbajando3;
        public static int numbajando4;
        public static int numbajando5;
        private static int contEsperaCarga;
        private static short ultimaSec;
        public static int calle_overflow;
        public static int calle_noreadnodata;
        public static int maxbufferbajada;

        /// <summary>
        /// Tipo de notificación para la interfaz y log
        /// </summary>
        public enum NotificationType
        {
            MessageSended,
            ReadData,
            Error
        }

        static PLCDaemon()
        {
            DBRead = Conf.getInt("plc_db_read");
            NumDBWrite = Conf.getInt("plc_db_write");
            ip = Conf.getStr("plc_ip");
            rack = Conf.getInt("plc_rack");
            slot = Conf.getInt("plc_slot");

            string[] live = Conf.getStr("plc_add_live").Split('.');
            dbVivo = int.Parse(live[0]);
            byteVivo = int.Parse(live[1]);
            bitVivo = int.Parse(live[2]);

            Client = new S7Client();

            contDescarga1 = 0;
            contDescarga2 = 0;
            contDescarga3 = 0;
            contDescarga4 = 0;
            contDescarga5 = 0;
            contRestoComandos = 0;
            contEsperaCarga = 0;

            bajando1 = new List<string>();
            bajando2 = new List<string>();
            bajando3 = new List<string>();
            bajando4 = new List<string>();
            bajando5 = new List<string>();
            
            cargando1 = new List<string>();
            cargando2 = new List<string>();

            dictStockReal = new Dictionary<int, bool>();

            for(int i = 1001; i<1033; i++)
            {
                dictStockReal.Add(i, true);
            }
            for(int i=2001; i<2021;i++)
            {
                dictStockReal.Add(i, true);
            }
            for(int i=3001; i<3029; i++)
            {
                dictStockReal.Add(i, true);
            }
            for(int i=4001; i<4017; i++)
            {
                dictStockReal.Add(i, true);
            }

            qCommToWrite = new Queue<comPLC>();
            qCommL1 = new Queue<comPLC>();
            qCommL2 = new Queue<comPLC>();
            qCommL3 = new Queue<comPLC>();
            qCommL4 = new Queue<comPLC>();
            qCommL5 = new Queue<comPLC>();
            qControlMan = new Queue<MCElement>();

            dbPerchas = new List<int>();

            tmr = new System.Timers.Timer();
            tmr.Interval = Conf.getInt("plc_interval");
            tmr.Elapsed += Tmr_Elapsed;
            tmrFin = true;


        }

        /// <summary>
        /// Comienza la ejecución del hilo
        /// </summary>
        public static void Start()
        {
            tmr.Start();
        }

        private static void Tmr_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (tmrFin)
            {
                tmrFin = false;
                
                timetmr = DateTime.Now;

                doWork();
             //   Trace.TraceError("PLC: " + (DateTime.Now - timetmr).ToString());
                tmrFin = true;
            }
            else
            {
                
                if (timetmr.AddSeconds(10) < DateTime.Now)
                {
                    tmr.Stop();
                    tmrFin = true;
                    tmr.Start();
                }
            }
        }

        private static void doWork()
        {
            //Sigo vivo para el automata
            boolVivo = !boolVivo;
            Write(dbVivo, byteVivo, bitVivo, Convert.ToByte(boolVivo));

            //Comando de descarga a linea 1000
            if (contDescarga1 >= 30)
            {
                contDescarga1 = 0;
                WriteStock();
                EscribeComDescarga(1);  
            }

            if (contDescarga4 >= 25)
            {
                contDescarga4 = 0;
                EscribeComDescarga(4);
            }

            if (contDescarga5 >= 20)
            {
                contDescarga5 = 0;
                EscribeComDescarga(5);
            }

            //Comando de descarga a linea 2000
            if (contDescarga2 >= 40)
            {
                contDescarga2 = 0;
                EscribeComDescarga(2);
                WriteInfo();
            }

            if (contDescarga3 >= 35)
            {
                contDescarga3 = 0;
                EscribeComDescarga(3);
            }

            //Resto de comandos
            if (contRestoComandos >= 5)
            {
                contRestoComandos = 0;
                EscribeComandos();
            }

            //Respuestas
            ConsultaRespuestasPLC();

            //Control Manual
            EjecutaOrdenesManuales();

            //Monitorizacion de DBs
            ConsultaPerchasEnDBs();

            contDescarga1++;
            contDescarga2++;
            contDescarga3++;
            contDescarga4++;
            contDescarga5++;
            contRestoComandos++;      
        }

        private static void EscribeComDescarga(int linea)
        {
            List<PLCPercha> lstPerchas = null;

            if (linea == 1)
            {
                List<comPLC> lst1 = qCommL1.ToList();
                if (lst1.Count > 0)
                {
                    lstPerchas = ReadDB(11, true);
                    oDescarga(lst1[0], lstPerchas, 1);
                }
            }
            else if (linea == 2)
            {
                List<comPLC> lst2 = qCommL2.ToList();
                if (lst2.Count > 0)
                {
                    lstPerchas = ReadDB(11, true);
                    oDescarga(lst2[0], lstPerchas, 2);
                }
            }
            else if (linea ==3)
            {
                List<comPLC> lst3 = qCommL3.ToList();
                if (lst3.Count > 0)
                {
                    lstPerchas = ReadDB(11, true);
                    oDescarga(lst3[0], lstPerchas, 3);
                }
            }
            else if (linea == 4)
            {
                List<comPLC> lst4 = qCommL4.ToList();
                if (lst4.Count > 0)
                {
                    lstPerchas = ReadDB(11, true);
                    oDescarga(lst4[0], lstPerchas, 4);
                }
            }
            else if (linea == 5)
            {
                List<comPLC> lst5 = qCommL5.ToList();
                if (lst5.Count > 0)
                {
                    lstPerchas = ReadDB(11, true);
                    oDescarga(lst5[0], lstPerchas, 5);
                }
            }
        }
        
        private static void EscribeComandos()
        {
            //Resto de comandos
            List<comPLC> lst = qCommToWrite.ToList();
            if (lst.Count > 0)
            {
                if (lst[0].tipoComando == comPLC.enumTipoComPLC.PCCargaACalle)
                {
                    if (contEsperaCarga >= 2)
                    {
                        List<comPLC> lstMismaOrden = lst.FindAll(o => o.orden == lst[0].orden);
                        oCarga(lstMismaOrden);
                        contEsperaCarga = 0;
                    }
                    else
                    {
                        contEsperaCarga++;
                    }

                }
                else
                {
                    comPLC p = qCommToWrite.Dequeue();
                    Write(p);
                }
            }
        }

        private static void EjecutaOrdenesManuales()
        {
            List<MCElement> lstE = qControlMan.ToList();
            if (lstE.Count > 0)
            {
                Write(qControlMan.Dequeue());
            }
        }

        private static void ConsultaPerchasEnDBs()
        {
            if (addDB && !dbPerchas.Contains(numDBToAdd))
            {
                dbPerchas.Add(numDBToAdd);
                addDB = false;
            }

            if (removeDB)
            {
                dbPerchas.Remove(numDBToRemove);
                removeDB = false;
            }

            foreach (int p in dbPerchas)
                ReadDB(p, false);
        }

        private static void oCarga(List<comPLC> lstCom)
        {
            try
            {
                short puesto = lstCom[0].param[1];
                int numDB = (puesto == 1) ? 71 : 72;
                int numPerchas = ConsultarNumPerchas(numDB);

                if (numPerchas > 0)
                {
                    return;
                }

                for (int i = 0; i < lstCom.Count; i++)
                {
                    Write(qCommToWrite.Dequeue());
                }

            }
            catch(Exception ex)
            {
                Work.qLog.Enqueue(new messagetoLog(ex.Message, "PLCDaemon", "ODescarga1", messagetoLog.messageType.error));
            }
        }
        
        private static bool esperarporPosicion(int origenNuevaOrden, PLCPercha perchaenDB, int destino)
        {
            bool respuesta = false;

            bool perchaEnZona1A = (perchaenDB.transportador == 1) && ((perchaenDB.posicion <= 370) && (perchaenDB.posicion >= 170));

            bool perchaEnZona1B = (perchaenDB.transportador == 5) || 
                                    ((perchaenDB.transportador == 1) && (perchaenDB.posicion < 170));

            bool perchaEnZona1 = perchaEnZona1A || perchaEnZona1B;

            bool perchaEnZona2 = (perchaenDB.transportador == 2) && (perchaenDB.posicion >= 200);

            bool perchaEnZona4 = (perchaenDB.transportador == 2) && (perchaenDB.posicion < 200);

            bool perchaEnZona3 = (perchaenDB.transportador == 4) ||
                                    ((perchaenDB.transportador == 1) && (perchaenDB.posicion >= 500));

            bool origenEnZona1A = origenNuevaOrden > 1000 && origenNuevaOrden <= 1019;
            bool origenEnZona1B = origenNuevaOrden >= 1020 && origenNuevaOrden < 2000;
            bool origenEnZona1 = origenEnZona1A || origenEnZona1B;
            bool origenEnZona2 = origenNuevaOrden > 2000 && origenNuevaOrden < 3000;
            bool origenEnZona3 = origenNuevaOrden > 3000 && origenNuevaOrden < 4000;
            bool origenEnZona4 = origenNuevaOrden > 4000 && origenNuevaOrden < 5000;


            //Bajada linea 1000
            if (destino == 6)
            { 
               if (origenEnZona1 && (perchaEnZona1 || perchaEnZona2 || perchaEnZona3 || perchaEnZona4))
                    respuesta = true;
                if (origenEnZona2 && (perchaEnZona2 || perchaEnZona3 || perchaEnZona4))
                    respuesta = true;
                if (origenEnZona4 && (perchaEnZona4 || perchaEnZona3))
                    respuesta = true;
                if (origenEnZona3 && perchaEnZona3)
                    respuesta = true;
            }
            else if (destino == 7)
            {
                if (origenEnZona2 && (perchaEnZona2 || perchaEnZona4 || perchaEnZona3 || perchaEnZona1))
                    respuesta = true;
                if (origenEnZona4 && (perchaEnZona4 || perchaEnZona3 || perchaEnZona1))
                    respuesta = true;
                if (origenEnZona3 && (perchaEnZona3 || perchaEnZona1))
                    respuesta = true;
                if (origenEnZona1 && perchaEnZona1)
                    respuesta = true;
            }
            else if (destino == 8)
            {
                if (origenEnZona3 && (perchaEnZona3 || perchaEnZona1 || perchaEnZona2 || perchaEnZona4))
                    respuesta = true;
                if (origenEnZona1 && (perchaEnZona1 || perchaEnZona2 || perchaEnZona4))
                    respuesta = true;
                if (origenEnZona2 && (perchaEnZona2 || perchaEnZona4))
                    respuesta = true;
                if (origenEnZona4 && (perchaEnZona4))
                    respuesta = true;
            }
            else if (destino == 10)
            {
                if (origenEnZona1B && (perchaEnZona1B || perchaEnZona2 || perchaEnZona4 || perchaEnZona3 || perchaEnZona1A))
                    respuesta = true;
                if (origenEnZona2 && (perchaEnZona2 || perchaEnZona4 || perchaEnZona3 || perchaEnZona1A))
                    respuesta = true;
                if (origenEnZona4 && (perchaEnZona4 || perchaEnZona3 || perchaEnZona1A))
                    respuesta = true;
                if (origenEnZona3 && (perchaEnZona3 || perchaEnZona1A))
                    respuesta = true;
                if (origenEnZona1A && perchaEnZona1A)
                    respuesta = true;
            }
            else if (destino == 11)
            {
                if (origenEnZona4 && (perchaEnZona4 || perchaEnZona3 || perchaEnZona1 || perchaEnZona2))
                    respuesta = true;
                if (origenEnZona3 && (perchaEnZona3 || perchaEnZona1 || perchaEnZona2))
                    respuesta = true;
                if (origenEnZona1 && (perchaEnZona1 || perchaEnZona2))
                    respuesta = true;
                if (origenEnZona2 && (perchaEnZona2))
                    respuesta = true;
            }

            return respuesta;
        }

    private static void oDescarga(comPLC com, List<PLCPercha> lstPerchasDB11, int zona)
    {
        try {
                short maxSecEnDB = 0;

                int num = 0;
                int destino = 0;

                switch (zona)
                {
                    case 1: num = numbajando1; destino = 6;  break;
                    case 2: num = numbajando2; destino = 7;  break;
                    case 3: num = numbajando3; destino = 8;  break;
                    case 4: num = numbajando4; destino = 10; break;
                    case 5: num = numbajando5; destino = 11; break;
                }

                if (num > maxbufferbajada)
                    return;

                if (lstPerchasDB11 != null)
                {
                    List<PLCPercha> lstPerchasPorPuesto = lstPerchasDB11.FindAll(pe => pe.destino == destino);
                    //Si hay alguna circulando por detrás, espero
                    foreach (PLCPercha p in lstPerchasPorPuesto)
                    {
                        bool esperaPorPosicion = false;
                        // esperaPorPosicion = esperarporPosicion(com.param[0], p, destino);

                        if (zona == 1)
                        {
                            esperaPorPosicion = p.origen > com.param[0] && p.origen < 1033 && p.posicion < 350;
                         
                        }
                        else if (zona == 2)
                        {
                            esperaPorPosicion = p.origen > com.param[0] && (p.origen < 2021 || (p.origen > 4000 && p.origen < 4017)) && p.posicion < 560;
                          
                        }
                        else if (zona == 3)
                        {
                            esperaPorPosicion = true;
                          
                        }
                        else if (zona == 4)
                        {
                            esperaPorPosicion = false;
                           
                        }
                        else if (zona == 5)
                        {
                            esperaPorPosicion = true;
                           
                        }

                        if (esperaPorPosicion)
                        {
                            return;
                        }
                    }

                    if (lstPerchasDB11.Count > 0)
                    {
                        maxSecEnDB = lstPerchasDB11.Max(p => p.secuenciaPC);
                    }
                }

                comPLC c = null;

                switch(zona)
                {
                    case 1: c = qCommL1.Dequeue(); break;
                    case 2: c = qCommL2.Dequeue(); break;
                    case 3: c = qCommL3.Dequeue(); break;
                    case 4: c = qCommL4.Dequeue(); break;
                    case 5: c = qCommL5.Dequeue(); break;
                }

                if (c != null)
                {
                    short proxsec = calculaProxSec();
                    if (proxsec > maxSecEnDB)
                    {
                        Write(c);
                    }
                }
            }
            
            catch(Exception ex)
            {
                Work.qLog.Enqueue(new messagetoLog(ex.Message, "PLCDaemon", "ODescarga1", messagetoLog.messageType.error));
            }
        }

        private static short ConsultarNumPerchas(int numDB)
        {
            try
            {
                byte[] val = new byte[2];
                short numPerchas = -1;
                int res = Client.DBRead(numDB, 0, 2, val);
                if (res == 0)
                {
                    numPerchas = BitConverter.ToInt16(new byte[] { val[1], val[0] }, 0);
                }
                return numPerchas;
            }
            catch(Exception ex)
            {
                Work.qLog.Enqueue(new messagetoLog(ex.Message, "PLCDaemon", "ConsultarNumPerchas", messagetoLog.messageType.error));
                return -1;
            }
        }

        private static List<PLCPercha> ReadDB(int numDB, bool isLocal) 
        {
            try
            {
                int res = -1;
                if (Client.Connected())
                {
                    List<PLCPercha> resp = new List<PLCPercha>();
                    //  byte[] val = new byte[2];
                    //    int res = Client.DBRead(numDB, 0, 2, val);
                    short numPerchas = ConsultarNumPerchas(numDB);
                    if (numPerchas >= 0)
                    {
                      //  short numPerchas = BitConverter.ToInt16(new byte[] { val[1], val[0] }, 0);
                        byte[] data = new byte[18 * numPerchas];
                        res = Client.DBRead(numDB, 4, 18 * numPerchas, data);
                        PLCPercha.listPLCDBOrders.Clear();
                   
                        if (res == 0 && numPerchas > 0)
                        {
                            for (int i = 0; i < 18 * numPerchas; i = i + 18)
                            {
                                PLCPercha or = new PLCPercha();
                                or.db = numDB;
                                or.secuenciaPC = BitConverter.ToInt16(new byte[] { data[i + 1], data[i] }, 0);
                                or.origen = BitConverter.ToInt16(new byte[] { data[i + 3], data[i + 2] }, 0);
                                or.destino = BitConverter.ToInt16(new byte[] { data[i + 5], data[i + 4] }, 0);
                                or.codigoPercha = BitConverter.ToInt16(new byte[] { data[i + 7], data[i + 6] }, 0);
                                or.transportador = BitConverter.ToInt16(new byte[] { data[i + 9], data[i + 8] }, 0);
                                or.posicion = BitConverter.ToInt16(new byte[] { data[i + 11], data[i + 10] }, 0);
                                or.estadoPercha = BitConverter.ToInt16(new byte[] { data[i + 13], data[i + 12] }, 0);
                                or.timeStampOrden = BitConverter.ToInt32(new byte[] { data[i + 15], data[i + 14], data[i + 17], data[i + 16] }, 0);
                                PLCPercha.listPLCDBOrders.Add(or);
                                resp.Add(or);
                            }
                        }
                    }
                    else
                    {
                        Work.qLog.Enqueue(new messagetoLog("Error trying to read DB " + numDB + ", error code: " + res,
                            "PLCDaemon", "ReadDB", messagetoLog.messageType.error));

                        Work.qLog.Enqueue(new messagetoLog("", "PLCDaemon", "Read", messagetoLog.messageType.state_off));

                        Client.Disconnect();

                        Connect();
                        return null;
           
                    }
                    return resp;
                }
                else
                {
                    Connect();
                    return null;
                } 
            }
            catch (Exception ex)
            {
                Work.qLog.Enqueue(new messagetoLog("Error trying to read DB " + DBRead + ", Exception: " + ex.Message,
                                "PLCDaemon", "Read", messagetoLog.messageType.error));

                Work.qLog.Enqueue(new messagetoLog("", "PLCDaemon", "Read", messagetoLog.messageType.state_off));

                Client.Disconnect();

                Connect();
                return null;
            }
        }

        /// <summary>
        /// Se conecta al PLC
        /// </summary>
        /// <returns>0 si todo va bien</returns>
        private static void Connect()
        {
            bool conectado;
            int res;

            try
            {
                res = Client.ConnectTo(ip, rack, slot);
                conectado = (res == 0);

                if (res != 0)
                {
                    Work.qLog.Enqueue(new messagetoLog("Error trying to connect " + ip + ", error code: " + res,
                        "PLCDaemon", "Connect", messagetoLog.messageType.error));
                }
            }
            catch (Exception e)
            {
                Work.qLog.Enqueue(new messagetoLog("Error trying to connect " + ip + " " + e.Message, 
                    "JITDaemon", "Connect", messagetoLog.messageType.error));

                conectado = false;
            }
            if (conectado)
            {
                Work.qLog.Enqueue(new messagetoLog("", "PLCDaemon", "Connect", messagetoLog.messageType.state_on));
            }
            else
            {
                Work.qLog.Enqueue(new messagetoLog("", "PLCDaemon", "Connect", messagetoLog.messageType.state_off));
            }
        }


        private static void ConstruyeComando(int valorInicio, List<Int16> valores)
        {
            if (valores != null && valores[valorInicio] != 0)
            {
                try
                {
                    if (valores[valorInicio + 2] != 0)
                    {
                        comPLC.enumTipoComPLC type = (comPLC.enumTipoComPLC)Enum.Parse(typeof(comPLC.enumTipoComPLC),
                            Enum.GetName(typeof(comPLC.enumTipoComPLC), valores[valorInicio]));

                        comPLC commReceived = new comPLC();
                        commReceived.seq = valores[valorInicio + 1];
                        commReceived.tipoComando = type;
                        commReceived.CarrierId = valores[valorInicio + 2];
                        commReceived.param.Add(valores[valorInicio + 3]);
                        commReceived.PLCEnvia = true;
                        commReceived.lote = "0";
                        commReceived.referencia = "0";
                        commReceived.orden = "0";
                        Work.qPLCResp.Enqueue(commReceived);
                        Write((comPLC)null, valorInicio * 2);
                    }
                }
                catch (Exception ex)
                {
                    Work.qLog.Enqueue(new messagetoLog("Construir comandos recibidos del PLC: " + ex.Message, 
                        "PLCDaemon", "ConstruyeComando", messagetoLog.messageType.error));
                }
            }
        }

        /// <summary>
        /// Consulta si hay respuestas en el DB del PLC
        /// </summary>
        private static void ConsultaRespuestasPLC()
        {
            try
            {
                if (Client.Connected())
                {
                    byte[] val = new byte[128];
                    List<short> values16Bit = new List<short>();
                    
                    int res = Client.DBRead(DBRead, 0, 128, val);
                
                    if (res == 0)
                    {
                        for (int i = 0; i < val.Length; i = i + 2)
                        {
                            values16Bit.Add(BitConverter.ToInt16(new byte[] { val[i + 1], val[i] }, 0));
                        }

                        for (int i = 0; i <= 60; i = i + 4)
                        {
                            if (i != 12)
                            {
                                ConstruyeComando(i, values16Bit);
                            }
                        }
                    }
                    else
                    {
                        Work.qLog.Enqueue(new messagetoLog("Intento lectura en el DB: " + DBRead + ", código error: " + res,
                            "PLCDaemon", "Read", messagetoLog.messageType.error));

                        Work.qLog.Enqueue(new messagetoLog("", "PLCDaemon", "Read", messagetoLog.messageType.state_off));

                        Client.Disconnect();

                        Connect();
                    }
                }
                else
                {
                    Connect();
                }
            }
            catch (Exception ex)
            {
                Work.qLog.Enqueue(new messagetoLog("Error trying to read DB " + DBRead + ", Exception: " + ex.Message,
                                "PLCDaemon", "Read", messagetoLog.messageType.error));

                Work.qLog.Enqueue(new messagetoLog("", "PLCDaemon", "Read", messagetoLog.messageType.state_off));

                Client.Disconnect();

                Connect();
            }
        }

        private static List<byte> rellenaInfo(List<string> cpbajando)
        {
            List<byte> lst = new List<byte>();

            int i = 0;
            string num = "";
            string orden = "";
            foreach (string o in cpbajando)
            {
                if (o.Contains("..NO.."))
                {
                    orden = o.Substring(0, 12);
                    num = "NO. ";
                }
                else
                {
                    orden = o;
                    num = (i + 1).ToString().PadLeft(2, ' ') + ". ";
                }

                lst.AddRange(ASCIIEncoding.ASCII.GetBytes(num));
                lst.AddRange(ASCIIEncoding.ASCII.GetBytes(orden));
                i++;
            }
            if (i < 11)
            {
                for (int j = i; j <= 11; j++)
                {
                    lst.AddRange(ASCIIEncoding.ASCII.GetBytes("                "));
                }
            }

            return lst;
        }

        private static void WriteInfo()
        {
            try
            {
                if (Client.Connected())
                {
                    
                    List<string> cpbajando1 = bajando1.ToList();
                    List<string> cpbajando2 = bajando2.ToList();
                    List<string> cpbajando3 = bajando3.ToList();

                    List<byte> lst = new List<byte>();
                    lst.AddRange(rellenaInfo(cpbajando1).ToArray());
                    lst.AddRange(rellenaInfo(cpbajando2).ToArray());
                    lst.AddRange(rellenaInfo(cpbajando3).ToArray());

                    int res = Client.DBWrite(504, 0, lst.Count, lst.ToArray());

                    if (res != 0)
                    {
                        Work.qLog.Enqueue(new messagetoLog("Error escribiendo en el DB504: " + res, "PLCDaemon", "WriteInfo", messagetoLog.messageType.error));
                    }
                }
            }
            catch(Exception ex)
            {
                Work.qLog.Enqueue(new messagetoLog(ex.Message, "PLCDaemon", "WriteInfo", messagetoLog.messageType.error));
            }
        }


        private static void WriteStock()
        {
            try
            {
                if (Client.Connected())
                {
                    //Escribo el stock teórico en el autómata
                    byte[] valor1 = BitConverter.GetBytes(StockTeorico1);
                    byte[] valor2 = BitConverter.GetBytes(StockTeorico2);
                    byte[] valor3 = BitConverter.GetBytes(StockTeorico3);
                    byte[] valor4 = BitConverter.GetBytes(StockTeorico4);

                    List<byte> l = new List<byte>();

                    l.AddRange(valor1);
                    l.AddRange(valor2);
                    l.AddRange(valor3);
                    l.AddRange(valor4);

                    Client.DBWrite(501, 20, 16, l.ToArray());

                    //Leo el stock real en el autómata
                    byte[] valores = new byte[16];
                    Client.DBRead(501, 0, 16, valores);

                    Int32 valZona1 = BitConverter.ToInt32(new byte[] { valores[0], valores[1], valores[2], valores[3] }, 0);
                    Int32 valZona2 = BitConverter.ToInt32(new byte[] { valores[4], valores[5], valores[6], valores[7] }, 0);
                    Int32 valZona3 = BitConverter.ToInt32(new byte[] { valores[8], valores[9], valores[10], valores[11] }, 0);
                    Int32 valZona4 = BitConverter.ToInt32(new byte[] { valores[12], valores[13], valores[14], valores[15] }, 0);

                    Int32 mascara = 0;
                    for(int i = 1001; i < 1033; i++)
                    {
                        mascara = 1 << (i-1001);
                        dictStockReal[i] = ((valZona1 & mascara) !=0);
                    }
                    for (int i = 2001; i < 2021; i++)
                    {
                        mascara = 1 << (i - 2001);
                        dictStockReal[i] = ((valZona2 & mascara) != 0);
                    }
                    for (int i = 3001; i < 3029; i++)
                    {
                        mascara = 1 << (i - 3001);
                        dictStockReal[i] = ((valZona3 & mascara) != 0);
                    }
                    for (int i = 4001; i < 4017; i++)
                    {
                        mascara = 1 << (i - 4001);
                        dictStockReal[i] = ((valZona4 & mascara) != 0);
                    }
                }
            }
            catch(Exception ex)
            {
                Work.qLog.Enqueue(new messagetoLog(ex.Message, "PLCDaemon", "WriteStock", messagetoLog.messageType.error));
            }
        }

        private static int Write(int db, int byten, int bit, byte value)
        {
            try
            {
                if (Client.Connected())
                    return Client.WriteArea(S7Client.S7AreaDB, db, byten * 8 + bit, 1, S7Client.S7WLBit, new byte[] { value });
                else
                    return -1;
            }
            catch(Exception ex)
            {
                Work.qLog.Enqueue(new messagetoLog(ex.Message, "PLCDaemon", "Write", messagetoLog.messageType.error));
                return -1;
            }
        }

        /// <summary>
        /// Control manual de un elemento
        /// </summary>
        /// <param name="el">Elemento</param>
        private static void Write(MCElement el)
        {
            if (Client.Connected())
            {
                bool error = false;
                int res = 0;

                if (el.nextAction == MCElement.Action.delete)
                {
                    res = Client.DBWrite(el.db, 0, 2, new byte[] { 0, 0 });
                }

                if (el.nextAction == MCElement.Action.slow)
                {
                    if (el.type == MCElement.typeElement.motor)
                    {
                        res = Write(el.db, el.bytenum, el.bit1, 1);
                        if (res == 0)
                            res = Write(el.db, el.bytenum, el.bit2, 1);
                        else
                            error = true;  
                    }
                }
                else if (el.nextAction == MCElement.Action.fast)
                {
                    res = Write(el.db, el.bytenum, el.bit1, 1);
                    if (res == 0)
                        res = Write(el.db, el.bytenum, el.bit2, 0);
                    else
                        error = true;
                }
                else if (el.nextAction == MCElement.Action.off)
                {
                    if (el.type == MCElement.typeElement.motor)
                    {
                        res = Write(el.db, el.bytenum, el.bit1, 0);
                        if (res != 0)
                            error = true;
                    }
                    else
                    {
                        res = Write(el.db, el.bytenum, el.bit1, 0);
                    }
                }
                else if (el.nextAction == MCElement.Action.on)
                {
                    res = Write(el.db, el.bytenum, el.bit1, 1);
                }

                el.nextAction = MCElement.Action.none;

                if (res!=0 || error)
                {
                    Work.qLog.Enqueue(new messagetoLog("Error control manual, motor: " + el.name + ", Error num: " + res,
                        "PLCDaemon", "Write", messagetoLog.messageType.error));

                    Work.qLog.Enqueue(new messagetoLog("", "PLCDaemon", "Write", messagetoLog.messageType.state_off));

                    Client.Disconnect();

                    Connect();
                }
            }
            else
            {
                Connect();
            }
        }

        private static short calculaProxSec()
        {
            short res = 1;

            if (ultimaSec <= 0)
            {
                res = comPLC.calcSeq();
            }
            else
            {
                if (ultimaSec >= 30000)
                {
                    res = 1;
                }
                else
                {
                    res = (short)(ultimaSec + 1);
                }
            }

            return res;
        }

        /// <summary>
        /// Envia una orden al PLC, la escribe en el DB de ordenes
        /// </summary>
        /// <param name="com">Orden a enviar</param>
        /// <param name="valorinicio">Valor donde tengo que resetear una vez que he leído</param>
        private static void Write(comPLC com, int valorinicio = 0)
        {
         if(Client.Connected())
            {
                bool error = false;
                int res = 0;
                short secActual = 0;
                if (com == null)
                {

                    res = Client.DBWrite(DBRead, valorinicio, 2, new byte[] { 0, 0 });
                }
                else
                {
                    secActual = calculaProxSec();

                    byte[] type = BitConverter.GetBytes((short)com.tipoComando);
                    byte[] seq = BitConverter.GetBytes(secActual);
                    byte[] p1 = BitConverter.GetBytes(com.param[0]);
                    byte[] p2 = BitConverter.GetBytes(com.param[1]);
                    
                    res = Client.DBWrite(NumDBWrite, 2, 6, new byte[] { seq[1], seq[0], p1[1], p1[0], p2[1], p2[0] });

                    if (res == 0)
                    {

                        res = Client.DBWrite(NumDBWrite, 0, 2, new byte[] { type[1], type[0] });

                        if (res != 0)
                        { 
                            error = true;
                        }
                    }
                    else
                    {
                        error = true;

                    }

                    com.CambiaEnviado(true, secActual);
                    ultimaSec = secActual;
                }

                if (error)
                {
                    Work.qLog.Enqueue(new messagetoLog("Error al escribir en el DB " + NumDBWrite + ", Error num: " + res,
                        "PLCDaemon", "Write", messagetoLog.messageType.error));

                    Work.qLog.Enqueue(new messagetoLog("", "PLCDaemon", "Write", messagetoLog.messageType.state_off));

                    Client.Disconnect();

                    Connect();
                }
            }
            else
            {
                Connect();
            }
        }
    }
}
