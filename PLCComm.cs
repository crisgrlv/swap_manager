﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AereoManager
{
    /// <summary>
    /// Clase que modela un comando de comunicación con el PLC
    /// </summary>
    public class comPLC
    {
        /// <summary>
        /// Identificador del comando
        /// </summary>
        public long id;
        /// <summary>
        /// Fecha hora de envío o recepción del comando
        /// </summary>
        public DateTime ts;
        /// <summary>
        /// Número de secuencia para saber qué respuestas y peticiones están relacionadas
        /// </summary>
        public short seq;
        /// <summary>
        /// Tipo de comando 
        /// </summary>
        public enumTipoComPLC tipoComando;
        /// <summary>
        /// Parámetros del comando
        /// </summary>
        public List<short> param;
        /// <summary>
        /// Cuando es una respuesta del PLC, identificador de la petición correspondiente
        /// </summary>
        public long idPeticionPLC;
        /// <summary>
        /// Es true si está procesado por Swap Manager, false si está pendiente de procesar
        /// </summary>
        public bool procesado;
        /// <summary>
        /// Cuando es una petición al PLC, identificador de la petición JIT correspondiente
        /// </summary>
        public long idPeticionJIT;
        /// <summary>
        /// Percha que espero que saque el PLC o percha que el PLC ha asignado
        /// </summary>
        public short CarrierId;
        /// <summary>
        /// True si lo envia el PLC, false si lo envia el PC
        /// </summary>
        public bool PLCEnvia;
        public bool enviado;
        public string orden;
        public string referencia;
        public string lote;
        

        /// <summary>
        /// Tipos de comandos con el PLC
        /// </summary>
        public enum enumTipoComPLC
        {
            /// <summary>
            /// Petición de percha hacia el almacén (subir)
            /// </summary>
            PCCargaACalle = 1,
            /// <summary>
            /// Petición de percha a un puesto de trabajo (bajar)
            /// </summary>
            PCDescargaAPuesto = 2,
            /// <summary>
            /// El PC le da destino a una percha que acaba de aparecer en el sistema
            /// </summary>
            PCRespDestinoPerchaPerdida = 11,
            /// <summary>
            /// Respuesta a la petición de subida, percha colocada. 
            /// El PLC lo envía cuando la percha llegó a su calle.
            /// </summary>
            PLCPerchaSituadaEnCalle = 4,
            /// <summary>
            /// Respuesta a la petición de bajada, percha asignada.
            /// El PLC lo envía cuando lee el tag de la percha que está bajando.
            /// </summary>
            PLCTagLeido = 5,
            /// <summary>
            /// Respuesta a la petición de bajada, percha colocada.
            /// El PLC lo envía cuando la percha ha llegado al puesto.
            /// </summary>
            PLCPerchaLlegaAPuesto = 6,
            /// <summary>
            /// El PLC avisa de que aparece una percha nueva en el sistema, pide destino
            /// </summary>
            PLCPerchaNueva = 10,
            /// <summary>
            /// El PLC avisa de que una percha está vacía
            /// </summary>
            PLCPerchaVacia = 12,

            PCNuevoDestino = 13
        }

        /// <summary>
        /// Constructor sin parámetros
        /// </summary>
        public comPLC ()
        {
            param = new List<short>();
        }

        /// <summary>
        /// Calcula cuál debe ser el próximo número de secuencia de envío de peticiones al PLC
        /// </summary>
        /// <returns>Próximo número de secuencia, min = 1, max = 32767 (0x7FFF)</returns>
        public static short calcSeq()
        {
            short res = 1;
            try {
                    string query = "SELECT isnull(sec, 1) as Sec FROM dbo.MsgPLC where enviado = 1 and plcenvia = 0 order by ts desc";
                    DataTable dt = BD.ExecuteReader(query);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        res = (short)dt.Rows[0]["Sec"];

                        if (res == 30000 || res <= 0)
                        {
                            res = 1;
                        }
                        else
                            res++;
                    }
            }
            catch (Exception ex)
            {
                Work.qLog.Enqueue(new messagetoLog("Error calculando la secuencia de una orden al PLC: " + ex.Message,
                   "PLCComm", "calcSeq", messagetoLog.messageType.error));
                res = 1;
            }

            return res;
        }

        /// <summary>
        /// Instancia una orden del PLC a partir de un resultado de base de datos
        /// </summary>
        /// <param name="r">Fila del DataTable</param>
        /// <returns>Objeto PLCComm instanciado</returns>
        private static comPLC instanciar (DataRow r)
        {
            comPLC c = new comPLC();
            c.id = (long)r["Id"];
            c.ts = (DateTime)r["TS"];
            c.seq = (short)r["Sec"];
            c.tipoComando = (enumTipoComPLC)Enum.Parse(typeof(enumTipoComPLC), Enum.GetName(typeof(enumTipoComPLC), (short)r["Tipo"]));
            c.param.Add((short)r["Par1"]);
            c.param.Add((short)r["Par2"]);
            c.param.Add((short)r["Par3"]);
            c.idPeticionPLC = (long)r["IdPeticionPLC"];
            c.procesado = (bool)r["Procesado"];
            c.idPeticionJIT = (long)r["IdPeticionJIT"];
            c.CarrierId = (short)r["CarrierId"];
            c.enviado = (bool)r["Enviado"];
            c.orden = (string)r["Orden"];
            c.referencia = (string)r["Referencia"];
            c.lote = (string)r["Lote"];
            return c;
        }

        /// <summary>
        /// Cambia el valor del atributo processed en la base de datos 
        /// </summary>
        /// <param name="newValue">Nuevo valor para el atributo processed</param>
        public void CambiaProcesado(bool newValue)
        {
            try
            {
                this.procesado = newValue;

                string query = "UPDATE dbo.MsgPLC set Procesado = @p0 WHERE Id = @p1";
                int rowsAffected = BD.ExecuteNonQuery(query, newValue, this.id);
            }
            catch (Exception ex)
            {
                Work.qLog.Enqueue(new messagetoLog("Error cambiando el atributo procesado: " + ex.Message,
                    "PLCComm", "ChangeProcAttr", messagetoLog.messageType.error));
            }
        }


        /// <summary>
        /// Cambia el valor del atributo processed en la base de datos 
        /// </summary>
        /// <param name="newValue">Nuevo valor para el atributo processed</param>
        /// <param name="sec">Nuevo valor de la secuencia segun lo enviado al plc</param>
        public void CambiaEnviado(bool newValue, short sec)
        {
            try
            {
                this.procesado = newValue;

                string query = "UPDATE dbo.MsgPLC set Enviado = @p0, sec = @p1, ts = getdate() WHERE Id = @p2";
                int rowsAffected = BD.ExecuteNonQuery(query, newValue, sec, this.id);
            }
            catch (Exception ex)
            {
                Work.qLog.Enqueue(new messagetoLog("Error cambiando el atributo enviado: " + ex.Message,
                    "PLCComm", "ChangeEnvAttr", messagetoLog.messageType.error));
            }
        }

        /// <summary>
        /// Busca en la base de datos la petición enviada al PLC que tiene el número de secuencia especificado
        /// </summary>
        /// <param name="seq">Número de secuenca para buscar en la base de datos</param>
        /// <returns>Comando encontrado en la base de datos</returns>
        public static comPLC BuscaComandoPLC(short seq)
        {
            comPLC c = null;
           
            try
            {
                if (seq != 0)
                {
                    DataTable dt = BD.ExecuteReader("select TOP 1 * from dbo.MsgPLC where Sec = @p0 and PLCEnvia = 0 and enviado = 1 order by ts desc", (short)seq);

                    if (dt != null && dt.Rows.Count > 0)
                    {
                        c = instanciar(dt.Rows[0]);
                    }
                }
            }
            catch(Exception ex)
            {
                Work.qLog.Enqueue(new messagetoLog("Error finding command PLC: " + ex.Message, 
                    "PLCComm", "FindComm", messagetoLog.messageType.error));
                c = null;
            }

            return c;
        }

        /// <summary>
        /// Busca los comandos pendientes de procesar
        /// </summary>
        /// <param name="isRequest">true si es una petición y false si es una respuesta</param>
        /// <returns></returns>
        public static List<comPLC> Find(bool isRequest)
        {
            List<comPLC> res = new List<comPLC>();
            string query = "";

            try
            {
                query = "select * from dbo.MsgPLC where Procesado = 0 and PLCEnvia = @p0 order by TS";
                DataTable dt = BD.ExecuteReader(query, !isRequest);

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        res.Add(instanciar(row));
                    }
                }
            }
            catch (Exception ex)
            {
                Work.qLog.Enqueue(new messagetoLog("Error finding list of command PLC: " + ex.Message,
                    "PLCComm", "FindPendResp", messagetoLog.messageType.error));
            }

            return res;
        }

        /// <summary>
        /// Guarda en bd un comando de comunicación con el PLC
        /// </summary>
        /// <param name="com">Comando del PLC para guardar</param>
        internal static void Save(comPLC com)
        {
            try
            {
                for(int i=com.param.Count; i<3; i++)
                {
                    com.param.Add(0);
                }

                string query = "INSERT INTO dbo.MsgPLC (TS, Sec, Tipo, Par1, Par2, Par3, IdPeticionPLC, Procesado, " +
                    " IdPeticionJIT, CarrierId, PLCEnvia, Enviado, Orden, Referencia, Lote) " + 
                    " values (GETDATE(), @p0, @p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8, @p9, @p10, @p11, @p12, @p13)";

                int rowsAffected = BD.ExecuteNonQuery(query,
                   com.seq, (short)com.tipoComando, com.param[0], com.param[1], com.param[2], com.idPeticionPLC, 
                   com.procesado, com.idPeticionJIT, com.CarrierId, com.PLCEnvia, com.enviado, com.orden, com.referencia, com.lote);
            }
            catch (Exception ex)
            {
                Work.qLog.Enqueue(new messagetoLog("Error al insertar en la tabla MsgPLC: " + ex.Message,
                    "PLCComm", "Save", messagetoLog.messageType.error));
            } 
        }

        internal void updateTraza()
        {
            try
            {
                string query = "update dbo.msgplc set referencia = @p0, orden = @p1, lote = @p2 where id = @p3";
                BD.ExecuteNonQuery(query, this.referencia, this.orden, this.lote, this.id);
            }
            catch (Exception ex)
            {
                Work.qLog.Enqueue(new messagetoLog(ex.Message, "PLCComm", "updateTraza", messagetoLog.messageType.error));
            }
        }

        internal static comPLC BuscaByCarrier(short carrierId)
        {
            try {
                comPLC resp = null;
                string query = "select top 1 * from dbo.msgplc where tipo = 5 and carrierid = @p0 order by ts desc ";
                DataTable dt = BD.ExecuteReader(query, carrierId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    resp = instanciar(dt.Rows[0]);
                }
                return resp;
            }
            catch(Exception ex)
            {
                Work.qLog.Enqueue(new messagetoLog(ex.Message, "PLCComm", "BuscaByCarrier", messagetoLog.messageType.error));
                return null;
            }
        }
    }
}
