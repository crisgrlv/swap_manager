﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AereoManager
{
    public partial class frmRecirc : Form
    {
        public frmRecirc()
        {
            InitializeComponent();

            cboOrigen.Items.Add(800);
            cboDest.Items.Add(800);

            cboOrigen.Items.Add(901);
            cboDest.Items.Add(901);

            for (int i = 1001; i < 1033; i++)
            {
                cboOrigen.Items.Add(i);
                cboDest.Items.Add(i);
            }
            for (int i = 2001; i < 2021; i++)
            {
                cboOrigen.Items.Add(i);
                cboDest.Items.Add(i);
            }
            for (int i = 3001; i < 3029; i++)
            {
                cboOrigen.Items.Add(i);
                cboDest.Items.Add(i);
            }
            for (int i = 4001; i < 4017; i++)
            {
                cboOrigen.Items.Add(i);
                cboDest.Items.Add(i);

            }

        //    cboNumCarr.Items.Add("Todas");
            for (int i = 1; i < 51; i++)
            {
                cboNumCarr.Items.Add(i);   
            }

            cboNumCarr.SelectedIndex = 0;
            cboOrigen.SelectedIndex = 0;
            cboDest.SelectedIndex = 0;
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            int numCarriersToMove = 0;
            short orig = short.Parse(cboOrigen.SelectedItem.ToString());
            short dest = short.Parse(cboDest.SelectedItem.ToString());

            if (cboNumCarr.SelectedItem.ToString() == "Todas")
            {
                numCarriersToMove = Carrier.BeginRecirc(orig);
            }
            else
                numCarriersToMove = (int)cboNumCarr.SelectedItem;

            int seq = 0;
            for(int i = 0; i < numCarriersToMove; i++)
            {
                comPLC p = new comPLC();
                p.tipoComando = comPLC.enumTipoComPLC.PCDescargaAPuesto;
                p.seq = 0;
                p.param.Add(orig);
                p.param.Add(dest);
                p.procesado = false;
                p.enviado = false;
                p.lote = "manual";
                p.orden = "manual";
                p.referencia = "manual";
                comPLC.Save(p);
            }
        }
    }
}
