﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AereoManager
{
    public partial class frmdudosas : Form
    {
        public frmdudosas()
        {
            InitializeComponent();
            dgv1.AutoGenerateColumns = false;
            actualizar();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Todas las perchas dudosas van a ser consideradas vacías, ¿desea continuar ?",
                "CONFIRMACIÓN", MessageBoxButtons.OKCancel);

            if (dr == DialogResult.OK)
            {
                string query = "Update dbo.DG_InvAereo set Estado = @p0 where Estado = @p1";
                BD.ExecuteNonQuery(query, Carrier.getSt(Carrier.enumEstadosPercha.Vacia), Carrier.getSt(Carrier.enumEstadosPercha.Duda));
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int lane = 0;
            DialogResult dr = MessageBox.Show("Todas las perchas de la calle " + textBox1.Text.Trim() + " van a considerarse dudosas, ¿desea continuar?",
                "CONFIRMACIÓN", MessageBoxButtons.OKCancel);

            if (dr == DialogResult.OK)
            {
                if (int.TryParse(textBox1.Text, out lane))
                {

                    string query = "Update dbo.DG_InvAereo set Estado = @p0 where Calle = @p1 and Estado = @p2";
                    BD.ExecuteNonQuery(query, Carrier.getSt(Carrier.enumEstadosPercha.Duda), lane.ToString(), Carrier.getSt(Carrier.enumEstadosPercha.Colocada));
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            short tag = 0;
            DialogResult dr = MessageBox.Show("La percha " + textBox2.Text.Trim() + " va a darse de baja del sistema, ¿desea continuar ?",
                "CONFIRMACIÓN", MessageBoxButtons.OKCancel);

            if (dr == DialogResult.OK)
            {
                if (short.TryParse(textBox2.Text, out tag))
                {
                    string query = "Update dbo.DG_InvAereo set Estado = @p0 where carrierid = @p1";
                    BD.ExecuteNonQuery(query, Carrier.getSt(Carrier.enumEstadosPercha.Eliminada), tag);
                }
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            actualizar();
        }

        private void actualizar()
        {
            string query = "select CarrierId, Calle, Referencia, Lote, Orden, TS from dbo.DG_InvAereo " +
                   " where estado = @p0 order by calle desc, TS";

            DataTable dt = BD.ExecuteReader(query, Carrier.getSt(Carrier.enumEstadosPercha.Duda));

            if (dt != null && dt.Rows.Count > 0)
                dgv1.DataSource = dt;
            else
                dgv1.DataSource = null;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            short tag = 0;
            DialogResult dr = MessageBox.Show("La percha " + textBox2.Text.Trim() + " va a darse de baja del sistema, ¿desea continuar ?",
                "CONFIRMACIÓN", MessageBoxButtons.OKCancel);

            if (dr == DialogResult.OK)
            {
                if (short.TryParse(textBox2.Text, out tag))
                {
                    string query = "Update dbo.DG_InvAereo set Estado = @p0 where CarrierId = @p1";
                    BD.ExecuteNonQuery(query, Carrier.getSt(Carrier.enumEstadosPercha.Eliminada), tag);
                }
            }
        }
    }
}
