﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AereoManager
{
    public enum enumPuestos
    {
        Carga1 = 1,
        Carga2 = 2,
        DescargaL1 = 6,
        DescargaL2 = 7,
        DescargaL3 = 8,
        DescargaL4 = 9,
        Rep2 = 10,
        Retoques = 11
    }
    public class Carrier
    {
        public short CarrierId;
        public enumEstadosPercha Estado;
        public string referencia;
        public string lote;
        public string orden;
        public short calle;
        public DateTime ts; 

        public enum enumEstadosPercha
        {
            Vacia = 0, 
            Duda = 1,
            Subiendo = 10,
            Eliminada = 45,
            Colocada = 50,
            Bajando = 20,
            Retornando = 30
        }

        private static Carrier Inst(DataRow r)
        {
            Carrier c = new Carrier();

            try { c.CarrierId = (short)r["CarrierId"]; } catch { c.CarrierId = 0; }

            try
            { short cs = 0;
                if (short.TryParse(r["Estado"].ToString(), out cs))
                    c.Estado = (enumEstadosPercha)Enum.Parse(typeof(enumEstadosPercha), Enum.GetName(typeof(enumEstadosPercha), cs));
                else
                    c.Estado = enumEstadosPercha.Vacia;
            }
            catch { c.Estado = enumEstadosPercha.Vacia; }

            try {
                if (string.IsNullOrEmpty((string)r["referencia"]))
                    c.referencia = "";
                else
                    c.referencia = (string)r["Referencia"];
            }
            catch{ c.referencia = ""; }

            try {
                if (string.IsNullOrEmpty((string)r["lote"]))
                    c.lote = "";
                else
                    c.lote = (string)r["Lote"];
            }
            catch { c.lote = ""; }

            try {
                short ca = 0;
                if (string.IsNullOrEmpty((string)r["Calle"]) || !short.TryParse((string)r["Calle"], out ca))
                    c.calle = (short)PLCDaemon.calle_noreadnodata;
                else
                    c.calle = ca;
            }
            catch { c.calle = 0; }
            try {
                if (string.IsNullOrEmpty((string)r["Orden"]))
                    c.orden = "";
                else
                    c.orden = (string)r["Orden"];
            }
            catch { c.orden = ""; }

            try {
                c.ts = (DateTime)r["TS"]; }
            catch { c.ts = DateTime.Now; }

            return c;
        }

        public static string findRef(short numlane)
        {
            string res = "";
            string query = "select referencia from dbo.DG_InvAereo where calle = @p0";
            DataTable dt = BD.ExecuteReader(query, numlane);
            if (dt != null && dt.Rows.Count > 0)
            {
                res = dt.Rows[0]["referencia"].ToString();
            }

            return res;
        }

        public static int BeginRecirc(short numLane)
        {
            int resp = 0;
          
            try
            {
                DataTable dt = BD.ExecuteReader("SELECT count(CarrierId) as num FROM dbo.DG_InvAereo WHERE Calle = @p0 and Estado =@p1", 
                    numLane.ToString(), getSt(enumEstadosPercha.Colocada));

                BD.ExecuteNonQuery("update dbo.DG_InvAereo set Estado =@p0 where Calle = @p1 and Estado = @p2", 
                    getSt(enumEstadosPercha.Duda), numLane, getSt(enumEstadosPercha.Colocada));

                if (dt != null && dt.Rows.Count > 0)
                {
                    resp = (int)dt.Rows[0]["num"];
                }
            }
            catch (Exception ex)
            {
                Work.qLog.Enqueue(new messagetoLog("Error finding carrier: " + ex.Message,
                    "Carrier", "NumCarrByLane", messagetoLog.messageType.error));
            }

            return resp;
        }

        public static string getSt(enumEstadosPercha state)
        {
            return ((int)state).ToString().PadLeft(3, '0');
        }

        public static Carrier Find(short tag)
        {
            Carrier c = null;
            try
            {
                DataTable dt = BD.ExecuteReader("SELECT * FROM dbo.DG_InvAereo WHERE CarrierId = @p0", tag);
                if (dt != null && dt.Rows.Count > 0)
                {
                    c = Inst(dt.Rows[0]);
                }

                if (c.CarrierId == 0)
                    c.CarrierId = tag;
            }
            catch (Exception ex)
            {
                Work.qLog.Enqueue(new messagetoLog("Error finding carrier: " + ex.Message,
                    "Carrier", "Find", messagetoLog.messageType.error));
                c = null;
            }

            return c;
        }

        /// <summary>
        /// Cambia el estado de una percha en la BD según las respuestas del PLC
        /// Si la percha no existe en la BD, la crea
        /// </summary>
        /// <param name="isNew">Indica si es una percha nueva o ya existía</param>
        public void Save(bool isNew)
        {
            try
            {
                string query = "";
                
                if (isNew)
                {
                    query = "INSERT INTO dbo.DG_InvAereo (Estado, Calle, TS, Referencia, Lote, CarrierId, Orden) VALUES (@p0, @p1, GETDATE(), @p2, @p3, @p4, @p5)";
                    
                }
                else
                {
                    query = "UPDATE dbo.DG_InvAereo set Estado = @p0, Calle = @p1, TS = GETDATE(), Referencia=@p2, Lote=@p3, Orden =@p5  WHERE CarrierId = @p4";
                }

                BD.ExecuteNonQuery(query,
                    getSt(Estado), this.calle, this.referencia, this.lote, this.CarrierId, this.orden);
            }
            catch (Exception ex)
            {
                Work.qLog.Enqueue(new messagetoLog("Error updating carrier: "   + ex.Message,
                    "Carrier", "Save", messagetoLog.messageType.error));
            }
        }

        internal static short PeticionPerchaCalle(enumPuestos puesto, string referencia)
        {
            short c = 0, c1 = 0, c2 = 0, c3 = 0;
            short resp = 0;
            string calle = "";
            int posibleCalle = 0;

            Dictionary<string, int> calles = new Dictionary<string, int>();

            switch (puesto)
            {
                case enumPuestos.DescargaL1:
                    {   
                        //c = 1000; c1 = 2000; c2 = 3000; c3 = 4000;
                        c = 1000; c1 = 1000; c2 = 1000; c3 = 1000;
                    }
                    break;
                case enumPuestos.DescargaL2:
                    {
                        //c = 2000; c1 = 4000; c2 = 3000; c3 = 1000;
                        c = 2000; c1 = 4000; c2 = 2000; c3 = 2000;
                    }
                    break;
                case enumPuestos.DescargaL3:
                    {
                        //c = 3000; c1 = 1000; c2 = 2000; c3 = 4000;
                        c = 3000; c1 = 3000; c2 = 3000; c3 = 3000;
                    }
                    break;
                case enumPuestos.Rep2:
                    {
                        //c = 2000; c1 = 4000; c2 = 3000; c3 = 1000;
                        c = 1019; c1 = 1019; c2 = 1019; c3 = 1019; 
                    }
                    break;
                case enumPuestos.Retoques:
                    {
                        //c = 1000; c1 = 1000; c2 = 1000; c3 = 1000;
                        c = 1000; c1 = 1000; c2 = 1000; c3 = 1000; 
                    }
                    break;
            }

            string query = "select count(carrierid) as suma, calle from dbo.DG_InvAereo where estado = '050' and calle = any ( " +
                " Select calle from dbo.ConfRef where referencia = @p0 and calle > @p1 and calle < @p2 " +
                " ) group by calle order by suma desc";

            DataTable dt = BD.ExecuteReader(query, referencia.Trim(), c, c + 999);
            if (dt == null || dt.Rows.Count == 0)
            {
                dt = BD.ExecuteReader(query, referencia.Trim(), c1, c1 + 999);
                if (dt == null || dt.Rows.Count == 0)
                {
                    dt = BD.ExecuteReader(query, referencia.Trim(), c2, c2 + 999);
                    if (dt == null || dt.Rows.Count == 0)
                    {
                        dt = BD.ExecuteReader(query, referencia.Trim(), c3, c3 + 999);
                        if (dt == null || dt.Rows.Count == 0)
                        {
                            Work.qLog.Enqueue(new messagetoLog("No encuentro la referencia en el almacén",
                                "Carrier", "PeticionPerchaCalle", messagetoLog.messageType.error));
                        }
                    }
                }
            }

            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    if ((int)dr["suma"] > 0)
                    {
                        if (int.TryParse(dr["calle"].ToString(), out posibleCalle))
                        {
                        //    if (PLCDaemon.dictStockReal.Keys.Contains(posibleCalle) &&
                        //        PLCDaemon.dictStockReal[posibleCalle])
                       //     {
                                calles.Add((string)dr["calle"], (int)dr["suma"]);
                       //     }
                        }
                    }
                }
            }

            if (calles.Keys.Count > 0)
            {
                calle = calles.FirstOrDefault(x => x.Value == calles.Values.Max()).Key;
            }

            if (short.TryParse(calle, out resp))
            {
                return resp;
            }
            else
            {
                Work.qLog.Enqueue(new messagetoLog("NO HAY STOCK", "Carrier", "PeticionPerchaCalle", messagetoLog.messageType.trace));
                return -1;
            }
        }

        /// <summary>
        /// Busca una percha para llevar a un puesto de trabajo
        /// </summary>
        /// <returns>Percha que se puede mover</returns>
        internal void BuscaCalle(enumPuestos puesto)
        {
            string query = "SELECT mt.* FROM dbo.DG_InvAereo mt INNER JOIN " +
                                   " ( " +
                                       " SELECT Calle, MIN(ts) MinDate " +
                                       " FROM dbo.DG_InvAereo " +
                                       " WHERE referencia = @p0 AND Estado = '050' " +
                                       " GROUP BY Calle " +
                                    " ) t ON mt.Calle = t.Calle AND mt.TS = t.MinDate " +
                                    " WHERE t.Calle BETWEEN @p1 AND @p2 " +
                                    " order by Calle";

            short c = 0, c1 = 0, c2=0, c3 = 0;
            if (puesto == enumPuestos.DescargaL1)
            {
                //c = 1000; c1 = 2000; c2 = 3000; c3 = 4000;
                c = 1000; c1 = 1000; c2 = 1000; c3 = 1000;
            }
            else if (puesto == enumPuestos.DescargaL2)
            {
                //c = 2000; c1 = 3000; c2 = 4000; c3 = 1000;
                c = 2000; c1 = 4000; c2 = 2000; c3 = 2000;
            }
            else if (puesto == enumPuestos.DescargaL3)
            {
                //c = 3000; c1 = 4000; c2 = 1000; c3 = 2000;
                c = 3000; c1 = 3000; c2 = 3000; c3 = 3000;
            }
            else if (puesto == enumPuestos.DescargaL4)
            {
                //c = 4000; c1 = 1000; c2 = 2000; c3 = 3000;
                c = 3000; c1 = 3000; c2 = 3000; c3 = 3000;
            }
        
            //Primero intento con la zona mas cercana
            DataTable dt = BD.ExecuteReader(query, this.referencia, c, c+999);
            if (dt == null || dt.Rows.Count == 0)
            {
                dt = BD.ExecuteReader(query, this.referencia, c1, c1 + 999);
                if (dt == null || dt.Rows.Count == 0)
                {
                    dt = BD.ExecuteReader(query, this.referencia, c2, c2 + 999);
                    if (dt == null || dt.Rows.Count == 0)
                    {
                        dt = BD.ExecuteReader(query, this.referencia, c3, c3 + 999);
                        if (dt == null || dt.Rows.Count == 0)
                        {
                            Work.qLog.Enqueue(new messagetoLog("No encuentro la referencia en el almacén", 
                                "Carrier", "FindLane", messagetoLog.messageType.error));
                            
                        } 
                    }
                }
            }

            if (dt != null && dt.Rows.Count > 0)
            {
                List<DataRow> lstdt = (dt.Rows.Cast<DataRow>().ToList()).OrderBy(ro=> (DateTime)ro["TS"]).ToList();
                DataRow r = lstdt[0];
                this.CarrierId = (short)r["CarrierId"];
                short cs = short.Parse((string)r["Estado"]);
                this.Estado = (enumEstadosPercha)Enum.Parse(typeof(enumEstadosPercha), Enum.GetName(typeof(enumEstadosPercha), cs));
                this.referencia = (string)r["Referencia"];
                this.lote = (string)r["Lote"];
                this.calle = short.Parse((string)r["Calle"]);
                this.orden = (string)r["Orden"];
                this.ts = (DateTime)r["TS"]; 
            }
        }

        /// <summary>
        /// Consulta cuantas perchas de la misma orden ya están en su sitio
        /// </summary>
        /// <param name="idOrdenJIT"></param>
        /// <returns></returns>
        public static int CalcNumCarriers(long idOrdenJIT)
        {
            int resp = 0;
            string query = "select COUNT(CarrierId) as contador from dbo.DG_InvAereo where IdOrdenJIT = @p0 and Estado = @p1";
            DataTable dt = BD.ExecuteReader(query, idOrdenJIT, getSt(enumEstadosPercha.Colocada));
            if (dt != null && dt.Rows.Count >0)
            {
                resp = (int)dt.Rows[0]["contador"];
            }
            return resp;
        }

        internal static Dictionary<short, short> CalcLanes(string reference)
        {
            string query = "Select * from dbo.ConfRef where Referencia = @p0 and calle > 1000 order by calle";
            DataTable dt = BD.ExecuteReader(query, reference);
            List<short> lanes = new List<short>();
            if(dt != null && dt.Rows.Count>0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    lanes.Add((short)dr["Calle"]);
                }
            }

            return null;
        }
    }
}
